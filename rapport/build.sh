#!/usr/bin/env bash
# Permet de compiler le rapport gardechou

name="gardechou"
rstfile="template/$name.rst"
texfile="template/rst_latex_article.tex"
outputfile="$name.pdf"
buildfolder="build"
latexfile="$name.tex"

rm $buildfolder/*
rst2latex --template=$texfile --documentoptions="10pt" $rstfile > $buildfolder/$name.tex
cd $buildfolder # apparemment il faut être dans le bon répertoire sinon metapost n'est pas content
pdflatex -shell-escape -interaction=nonstopmode $latexfile
lualatex -shell-escape -interaction=nonstopmode $latexfile # 2 fois pour les références, table de matières etc.
mv $outputfile ..
