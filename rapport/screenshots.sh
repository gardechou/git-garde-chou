#!/usr/bin/env bash
# Script pour automatiser la génération de captures d'écrans du site.
# Utile pour le rapport!
# Permet d'harmoniser la taille des captures d'écrans, entre autre chose.

width=1200

declare -A visiteur_pages
visiteur_pages["visiteur_home"]="/"
visiteur_pages["visiteur_recherche"]="/default/listbabysitters?code_postal=06000"
visiteur_pages["visiteur_inscription"]="/default/subscribe"

declare -A babysitter_pages
babysitter_pages["babysitter_home"]="/"
babysitter_pages["babysitter_gardes"]="/garde/lister"
babysitter_pages["babysitter_commentaires"]="/commentaires/lister"
babysitter_pages["babysitter_dispos"]="/dispo/lister"
babysitter_pages["babysitter_self_profil"]="/user/profile"
babysitter_pages["babysitter_view_famille"]="/user/profile?id=4"

declare -A famille_pages
famille_pages["famille_home"]="/"
famille_pages["famille_gardes"]="/garde/lister"
famille_pages["famille_commentaires"]="/commentaires/lister"
famille_pages["famille_self_profil"]="/user/profile"
famille_pages["famille_view_babysitter"]="/user/profile?id=1"

declare -A admin_pages
admin_pages["admin_home"]="/"
admin_pages["admin_users"]="/admin/users"
admin_pages["admin_comments"]="/admin/comments"
admin_pages["admin_gardes"]="/admin/gardes"
admin_pages["admin_stats"]="/admin/stats"
admin_pages["admin_params"]="/admin/parametres"

uri_prefix="https://www.nicoco.fr/gardechou"
images_folder="images/screenshots/"

echo "Screenshots des visiteurs..."
mkdir -p ${images_folder}
for page in "${!visiteur_pages[@]}"; do
    echo ${page}
    cutycapt --url=${uri_prefix}${visiteur_pages[$page]} --out=${images_folder}${page}.png --min-width=${width}
done

echo "Screenshots des baby-sitters..."
curl -X POST -F 'pseudo=jerem133' -F 'password=jerem133_password' ${uri_prefix}/user/login -c /tmp/cookie.txt
for page in "${!babysitter_pages[@]}"; do
    echo ${page}
    curl ${uri_prefix}${babysitter_pages[$page]} -b /tmp/cookie.txt > /tmp/gardechou.html
    sed -i 's/\/gardechou\//https:\/\/www.nicoco.fr\/gardechou\//g' /tmp/gardechou.html
    cutycapt --url=file:/tmp/gardechou.html --out=${images_folder}${page}.png --min-width=${width}
done

echo "Screenshots des familles..."
curl -X POST -F 'pseudo=groseille' -F 'password=groseille_password' ${uri_prefix}/user/login -c /tmp/cookie.txt
for page in "${!famille_pages[@]}"; do
    echo ${page}
    curl ${uri_prefix}${famille_pages[$page]} -b /tmp/cookie.txt > /tmp/gardechou.html
    sed -i 's/\/gardechou\//https:\/\/www.nicoco.fr\/gardechou\//g' /tmp/gardechou.html
    cutycapt --url=file:/tmp/gardechou.html --out=${images_folder}${page}.png --min-width=${width}
done

echo "Screenshots des admins..."
curl -X POST -F 'pseudo=nicoco' -F 'password=nicoco_password' ${uri_prefix}/user/login -c /tmp/cookie.txt
for page in "${!admin_pages[@]}"; do
    echo ${page}
    curl ${uri_prefix}${admin_pages[$page]} -b /tmp/cookie.txt > /tmp/gardechou.html
    sed -i 's/\/gardechou\//https:\/\/www.nicoco.fr\/gardechou\//g' /tmp/gardechou.html
    cutycapt --url=file:/tmp/gardechou.html --out=${images_folder}${page}.png --min-width=${width} --delay=2000
done
