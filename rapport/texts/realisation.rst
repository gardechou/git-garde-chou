Réalisation
===========

Page d’accueil
--------------

.. figure:: ../images/screenshots/visiteur_home.png
    :scale: 50 %

    Page d'accueil

Au centre de la page d'accueil, il y a la possibilité de rechercher des babysitters en indiquant un code postal pour les visiteurs.
En haut à gauche, le visiteur peu s’inscrire, il aura accès, via ce lien à un formulaire.
En haut à droite, l'utilisateur déjà inscrit, peut se connecter en entrant son pseudo et son mot de passe.

Formulaire d'inscription
------------------------

.. figure:: ../images/inscription_famille.png
    :scale: 50 %

    Formulaire d'inscription (version famille)

Tout d'abord, il faut indiquer si c'est une inscription en tant que babysitter ou en tant que famille. Cela permettra de faire apparaître le formulaire approprié.
Les informations à indiquer dans les deux cas sont: un pseudonyme, un mot de passe, un numéro de téléphone, une adresse e-mail, un prénom, un nom de famille, une adresse, un code postal et une ville. L'utilisateur peut aussi télécharger une photo et se présenter par un texte court, puis il peut valider son inscription en cliquant sur le bouton « valider ».
En revanche si l'utilisateur veut s'inscrire en tant que babysitter, il devra avant de valider son inscription préciser son âge en entrant sa date de naissance et son sexe.

Disponibilités
--------------

.. figure:: ../images/dispo_temporelle.png
    :scale: 50 %

    Modification des disponibilités d'un babysitter

Une fois l'inscription du babysitter validée par l'administrateur et après s'être connecté, il doit indiquer ses disponibilités s'il veut pouvoir recevoir des demandes de garde. Cela peut être une disponibilité ponctuelle ou une régulière. Si elle est ponctuelle, il doit préciser une date et une heure de début et de fin de créneau ainsi qu'un tarif correspondant à une heure de travail. En ce qui concerne la disponibilité régulière, il doit indiquer les jours de la semaine, un nombre de semaine, une heure de début et une heure de fin de créneau. Dans les deux cas il faudra indiquer une liste de multiplicateurs de tarif horaire pour ajuster le prix au nombre d’enfants gardés.
Il doit par ailleurs préciser les zones géographiques dans lesquelles il peut se rendre pour travailler en précisant une liste de codes postaux.

Paramètres du site
------------------

.. figure:: ../images/admin_param.png
    :scale: 50 %

    Modification des paramètres du site par l'administrateur


L'administrateur est le seul à pouvoir modifier les paramètres du site dont: le tarif maximal au delà duquel la garde doit être validée par l'administrateur avant de pouvoir être acceptée par un babysitter. Il doit préciser un taux de commission, qui sera ajouté au prix des gardes.

Il précise aussi trois délais; un premier d'annulation, au delà duquel (durée avant le début de la garde) il n'est plus possible d'annuler une garde que se soit par la famille ou par le babysitter.
De plus, une fois l'horaire de fin de garde passée, elle doit être validée ou invalidée par la famille; c'est ainsi qu'elle indique si la garde été effectuée ou non (cas où le babysitter ne se présente pas pour faire le travail). Le deuxième délai est donc un délai de validation au delà duquel (durée après la fin de la garde), elle est automatiquement validée.
Enfin, le dernier délai est celui correspondant à la possibilité de mettre ou modifier un commentaire après la fin de la garde.

A partir de cette page il est possible (de gauche à droite dans l'en-tête) d'accéder à l’accueil du site, d'avoir la liste des utilisateurs inscrits, de voir, modifier ou supprimer tous les commentaires associés à ces utilisateurs, d'autoriser ou interdire une garde (quand le tarif de celle-ci est supérieur au tarif maximal précisé en paramètre) et avoir accès à des statistiques concernant le site.


Profils
-------

.. figure:: ../images/screenshots/famille_view_babysitter.png
    :scale: 50 %

    Profil public d'un babysitter


La page de profil contient les informations que l'utilisateur a donné lors de l'inscription ; son adresse, son numéro de téléphone et son adresse mail ainsi que son âge et son sexe si c'est un babysitter. Le profil indique par ailleurs la date d'adhésion au site, le taux d'annulation; calculé en fonction du nombre de fois où l'utilisateur a annulé une garde après qu'elle ait été acceptée et la moyenne des notes ainsi que les commentaires qui lui ont été attribuées.

A partir de cette page, dans l'en-tête, l'utilisateur peut avoir accès à l’historique de ces gardes et à ses propres commentaires.

Accueil famille
---------------

.. figure:: ../images/home_famille.png
    :scale: 50 %

    Page d'accueil d'une famille


Sur cette page il est tout d'abord possible de rechercher les babysitters disponibles dans la région géographique correspondant au code postal de la famille et étant disponible pour les horaires de la garde demandée. L'utilisateur doit spécifier le nombre d'enfant qu'il désire faire garder.

La famille peut d'autre part, une fois l'horaire de fin de garde dépassée, valider ou invalider la garde. En faisant cela elle précise si la garde a été réellement effectuée ou non (cas où le babysitter ne se présente pas pour faire le travail).
