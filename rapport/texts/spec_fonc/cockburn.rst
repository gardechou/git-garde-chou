Scénarios de Cockburn
~~~~~~~~~~~~~~~~~~~~~

Recherche des baby-sitters par un visiteur
******************************************

- **Acteur:** Visiteur.

- **Pré-condition**: L'utilisateur n'est pas authentifié.

- **Scénario nominal**:

    #. L'utilisateur tape l'URL du site.

    #. Il renseigne le code postal de la région l'intéressant dans le champ correspondant sur la page d'accueil du site.

    #. Il soumet sa demande.

    #. Il consulte la liste des pseudonymes des baby-sitters disponibles dans la région ainsi que les photos de profils et les textes descriptifs qui y sont associés.

- **Extensions**:

- Le code postal entré n'est pas composé de cinq chiffre. Il doit recommencer sa recherche.

- Il n’y a pas de baby-sitter dans la région correspondante, le visiteur est invité à ré-essayer plus tard.

- **Post-condition**: Le visiteur consulte la liste des baby-sitters dans la région.

Inscription d’un baby-sitter
****************************

- **Acteurs**: Baby-sitter, Administrateur.

- **Pré-condition**: Le baby-sitter n’a pas de compte sur le site.

- **Scénario nominal**:

    #. Le baby-sitter tape l’URL du site.

    #. Il se rend sur “Inscription” et choisit une inscription de type baby-sitter.

    #. Il renseigne ses informations personnelles: prénom, nom, e-mail, numéro de téléphone, mot de passe, sexe, adresse, code postal, ville, multiplicateur permettant la hausse du prix en cas de garde de plusieurs enfants et date de naissance.

    #. Il soumet sa demande d’inscription.

    #. L’administrateur valide la demande.

- **Extensions**:

- L’administrateur refuse la demande de création de profil, l’utilisateur n’a pas de compte.

- Il fournit une image de profil.

- Il ajoute des informations à son profil par un texte de présentation.

- Tous les champs obligatoires ne sont pas remplis alors l’utilisateur doit recommencer son inscription.

- Le pseudonyme est déjà pré-existant dans la base de donnée, donc il doit recommencer l’inscription.

- Le mot de passe est trop court et ou ne comporte pas de chiffre et de caractère, alors il doit recommencer l'inscription.

- **Post-condition**: Le baby-sitter a un compte sur le site.

Inscription d’une famille
*************************

- **Acteurs**: Famille, Administrateur.

- **Pré-condition**: La famille n’a pas de compte sur le site.

- **Scénario nominal**:

    #. La famille tape l’URL du site.

    #. Elle se rend sur “Inscription” et choisit une inscription de type famille.

    #. Elle renseigne ses informations personnelles dans les champs correspondants: prénom, nom, adresse, code postal, mot de passe, e-mail, numéro de téléphone.

    #. Elle soumet sa demande d’inscription.

- **Extensions**:

- Elle fournit une image de profil.

- Elle ajoute des informations à son profil par un texte de présentation.

- Tous les champs obligatoires ne sont pas remplis alors elle doit recommencer son inscription.

- Le pseudonyme est déjà pré-existant dans la base de donnée, dont elle doit recommencer l’inscription.

- Le mot de passe est trop court et ou ne comporte pas de chiffre et de caractère, alors elle doit recommencer l'inscription.

- **Post-condition**: La famille a un de compte sur le site.

Authentification sur le site
****************************

- **Acteurs**: Tout les utilisateurs enregistrés sur le site (baby-sitters, familles, administrateurs)

- **Pré-condition**: L’utilisateur n’est pas authentifié.

- **Scénario nominal**:

    #. L’utilisateur se dirige sur la partie d’authentification.

    #. L’utilisateur donne son “Identifiant” correspondant à son pseudonyme et son “Mot de passe”.

    #. L’utilisateur valide les entrées et attend la validation des informations entrées.

- **Extension**: Si les informations entrées sont fausses, il est demandé d’essayer à nouveau.

- **Post-condition**: L’utilisateur est connecté et peut maintenant profiter de ses droits.

Le baby-sitter modifie ses disponibilités
*****************************************

- **Acteur**: Baby-sitter

- **Pré-condition**: Le baby-sitter est authentifié.

- **Scénario nominal**:

    #. Le baby-sitter se dirige vers la gestion des disponibilités, et se dirige ensuite sur la modification des disponibilités.

    #. Il indique alors sa disponibilité régulière à l’aide des instructions indiquées.

    #. Il indique les jours de la semaine où il est disponible, ainsi que le nombre de semaine où cela entre en vigueur et les horaires de début et fin de ses disponibilités.

    #. Il indique son prix horaire par heure.

    #. Une fois le formulaire de « modification des disponibilités » complètement terminé, il peut valider la modification de ses disponibilités.

- **Extensions**:

- Le baby-sitter abandonne la modification des disponibilités.

- Le baby-sitter valide la démarche de modification avant d’avoir complètement terminé les différentes étapes nécessaires, le site indiquera les étapes à compléter avant de pouvoir faire la modification.

- Le baby-sitter ajoute une disponibilité ponctuelle en renseignant le début et la fin de cette disponibilité ainsi que son taux horaire.

- Le baby-sitter peut ajouter ou supprimer une zone de travail indiquée par un code postal.

- **Post-condition**: Le baby-sitter a mis à jour ses disponibilités.

La famille fait une demande de garde
************************************

- **Acteur**: Famille

- **Pré-condition**: La famille est authentifiée.

- **Scénario nominal**:

    #. La famille authentifiée va sur sa page d’accueil.

    #. Elle renseigne dans les différents champs: le jour, l’heure de début et la durée de la garde. Elle valide alors ses choix de recherche.

    #. Elle consulte les différents baby-sitters disponibles selon les critères de recherche indiqués. Le système calcule pour chacun des baby-sitter le prix de la garde en fonction du tarif indiqué par le baby-sitter auquel se rajoute la commission du site.

    #. La famille envoie une demande de garde à un baby-sitter.

- **Extensions**:

- La famille n’est pas satisfaite des profils, elle n’envoie pas de demande.

- Il n’y a pas de baby-sitter correspondant à la demande, la famille est invitée à ré-essayer plus tard.

- La famille peut recommencer cette étape autant de fois qu'elle le souhaite, les demandes pour les familles sont illimitées.

- La famille consulte le profil complet d’un baby-sitter avant d’envoyer sa demande. Un nouvel onglet apparaît alors avec ses informations, dont les différents commentaires laissés par les familles, son taux d’annulation, son âge, son sexe, sa note moyenne et la date où ce dernier a rejoint Garde-chou.

- **Post-condition**: La famille a envoyé une demande de garde.

Une garde est avortée par la famille
************************************

- **Acteur**: Famille.

- **Pré-conditions**:

- La famille est authentifiée.

- La garde n'a pas été acceptée ou refusée par le baby-sitter.

- Le début de la garde est dans plus de 48h.

- **Scénario nominal**:

    #. Elle se rend sur les gardes actives.

    #. Elle choisit la garde qu’elle souhaite annuler.

    #. Elle confirme qu’elle souhaite bien annuler la garde.

- **Extension**: Si les 48h sont écoulées, la la garde demandée est ignorée.

- **Post-condition**: La garde est avortée.

Le baby-sitter accepte une garde

- **Acteur**: Baby-sitter

- **Pré-conditions**:

- Le baby-sitter est authentifié.

- Au moins une demande de garde a été adressée au baby-sitter.

- Dans le cas où le prix de la garde est supérieur au seuil fixé par l’administrateur, l’administrateur doit «autoriser» la garde.

- **Scénario nominal**:

    #. Le baby-sitter consulte les différentes demandes de garde envoyées par les familles.

    #. Le baby-sitter accepte la demande d’une famille.

- **Extension**: Le baby-sitter refuse la demande de la famille.

- **Post-condition**: La garde est acceptée par le baby-sitter. Le baby-sitter et la famille sont maintenant liés, leurs coordonnées sont alors mutuellement échangées.

Une famille annule une garde
****************************

- **Acteur**: Famille.

- **Pré-conditions**:

- La famille est authentifiée.

- La garde a été acceptée par le baby-sitter.

- Le début de la garde est dans plus de 48h.

- **Scénario nominal**:

    #. Elle se rend sur les gardes actives.

    #. Elle choisit la garde qu’elle souhaite annuler.

    #. Elle confirme qu’elle souhaite bien annuler la garde.

- **Extension**: Si les 48h sont écoulées, le compte de la famille est débité.

- **Post-condition**: La garde est annulée. Le système envoie un message au baby-sitter pour le lui signaler.

Un baby-sitter annule une garde
*******************************

- **Acteur**: Baby-sitter.

- **Pré-conditions**:

- L’utilisateur est authentifié.

- L’utilisateur a accepté la demande émise par la famille.

- Temps: 48 heures avant le jour “J” et la tranche horaire “H”.

- **Scénario nominal**:

    #. Le baby-sitter se dirige vers la gestion de garde.

    #. Le baby-sitter sélectionne la garde à annuler.

    #. Il confirme qu’il souhaite bien annuler la garde.

- **Post-condition**: La garde est annulée. Le système envoie un message à la famille pour le lui signaler.

Valider une garde par la famille
********************************

- **Acteurs**: Famille, Administrateur.

- **Pré-conditions**:

- Le baby-sitter choisit a accepté la demande émise par la famille.

- La famille n’a pas avorté ni annulé la garde.

- Dans le cas où le prix de la garde est supérieur au seuil fixé par l’administrateur, l’administrateur a «autorisé» la garde.

- Le baby-sitter n’a pas annulé la garde.

- La garde a été potentiellement effectuée (la date est dépassée) et son action et effectuée moins de 48h après que la garde ait lieu.

- La famille est authentifiée.

- **Scénario nominal**:

    #. La famille recherche la garde concernée.

    #. Elle clique pour cette garde sur ”validation”. Elle est redirigée sur une nouvelle page.

    #. Elle choisit, parmi une liste déroulante la qualité du déroulement de la garde.

- **Extensions**:

- La famille contacte le service clientèle en cas d’absence du baby-sitter. La garde est invalidée.

- Elle décide d’écrire un commentaire sur la garde effectuée. Ce commentaire apparaîtra sur le profil du baby-sitter après validation de l’administrateur. Elle pourra le modifier tant que l’administrateur ne l’aura pas validé.

- **Post-condition**: L’issue de la garde est indiquée.

Le baby-sitter met un commentaire
*********************************

- **Acteurs**: Baby-sitter, Administrateur

- **Pré-conditions**:

- Le baby-sitter est authentifié.

- La garde a été potentiellement effectuée et il réalise son action moins de 48h après que la garde ait lieu.

- **Scénario nominal**:

    #. Il recherche la garde effectuée. Il est redirigée sur une nouvelle page.

    #. Il choisit, parmi une liste déroulante la qualité du déroulement de la garde.

    #. Il écrit un commentaire sur le profil de la famille.

- **Extensions**:

- Le baby-sitter supprime ou modifie le commentaire qu’il a posté.

- Le commentaire est modéré par l’administrateur.

- **Post-condition**: Le baby-sitter a commenté la garde effectuée sur le profil de la famille.

Fixer la commission
*******************

- **Acteur**: Administrateur

- **Pré-condition**: L’administrateur est authentifié.

- **Scénario nominal**:

    #. Il choisit de se modifier la commission du site.

    #. Il entre une valeur pour la commission.

    #. Il valide le montant.

- **Extensions**:

- La commission est inférieure à zéro, le scénario est terminé.

- La commission n’a jamais été modifiée par l’administrateur et est donc par défaut égale à zéro.

- **Post-condition**: L’administrateur a fixé la commission

L'administrateur modifie un commentaire
***************************************

- **Acteur** : Administrateur.

- **Pré-conditions**:

- L’administrateur est authentifié.

- La garde a été validée par la famille.

- La famille et/ou le baby-sitter a commenté la garde.

- **Scénario nominal**:

    #. L'administrateur va dans l'onglet commentaires.

    #. Il sélectionne le commentaire.

    #. Il modifie le commentaire.

- **Extension**: L'administrateur supprime le commentaire.

- **Post-condition**:  L’administrateur a exercé son rôle de modérateur sur le commentaire.

Suspension d'un compte utilisateur
**********************************

- **Acteur**: Administrateur.

- **Pré-conditions**:

- L’administrateur est authentifié.

- Au moins un compte baby-sitter ou famille a été créé.

- Le compte n'est ni supprimé, ni suspendu.

- **Scénario nominal**:

    #. L'administrateur va dans l'onglet comptes.

    #. Il sélectionne le compte.

    #. Il clique sur le bouton suspendre.

- **Extensions** :

- L'administrateur supprime le compte de l'utilisateur.

- L'administrateur réactive le compte suspendu.

- **Post-condition** : L’administrateur a suspendu un compte utilisateur.

Autorisation d'une garde
************************

- **Acteur** : Administrateur.

- **Pré-conditions** :

- L’administrateur est authentifié.

- La famille a demandé une garde à un baby-sitter.

- Le tarif de la garde est supérieur au seuil fixé par l'administrateur.

- Le temps où la garde doit être effectuée n'est pas dépassé.

- **Scénario nominal**:

    #. L'administrateur va dans l'onglet gardes.

    #. Il clique sur « autorisation ».

- **Extensions** :

- L'administrateur interdit la garde.

- L'administrateur ne choisit rien. Il peut décider de l'état de la garde ultérieurement tant que date de la garde à laquelle elle doit être effectuée n'est pas dépassée.

- Le temps est où la garde doit être effectuée est dépassé, alors la garde est ignorée.

- **Post-condition** : L’administrateur a décidé du devenir de la garde.
