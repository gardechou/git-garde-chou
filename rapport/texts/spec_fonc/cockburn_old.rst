Scénarios de Cockburn
~~~~~~~~~~~~~~~~~~~~~

Inscription d’un baby-sitter
****************************

- Acteurs: baby-sitter, administrateur.

- Pré-condition: le baby-sitter n’a pas de compte sur le site.

- Scénario nominal:

   #. Le baby-sitter tape l’URL du site.

   #. Il se rend sur “Inscription” et choisit une inscription de type
      babysitter.

   #. Il renseigne ses informations personnelles: prénom, nom, e-mail,
      numéro de téléphone, mot de passe, sexe, adresse, code postal, ville,
      multiplicateur permettant la hausse du prix en cas de garde de plusieurs enfants et date de naissance.

   #. Il soumet sa demande d’inscription.

   #. L’administrateur valide la demande.

- Extensions:
    - L’administrateur refuse la demande de création de profil
      :math:`\rightarrow` l’utilisateur n’a pas de compte.
    - Il fournit une image de profil.
    - Il ajoute des informations à son profil par un texte de présentation.
    - Tous les champs obligatoires ne sont remplis alors l'utilisateur doit recommencer son inscription.
    - Le pseudonyme est déjà pré-existant dans la base de donnée, dont il doit recommencer l'inscription.
    - Le mot de passe est trop court et ou ne comporte pas de chiffre et de caractère, alors il doit recommencer.

- Post-condition: le baby-sitter a un compte sur le site.

Inscription d’une famille
*************************

- Acteurs: famille, administrateur.

- Pré-condition: la famille n’a pas de compte sur le site.

- Scénario nominal:

   #. La famille tape l’URL du site.

   #. Elle se rend sur “Inscription” et choisit une inscription de type
      famille.

   #. Elle renseigne ses informations personnelles dans les champs correspondants: prénom, nom,
      adresse, code postal, mot de passe, e-mail, numéro de téléphone.

   #. Elle soumet sa demande d’inscription.

- Extensions:
    - Elle fournit une image de profil.
    - Elle ajoute des informations à son profil par un texte de présentation.
    - Tous les champs obligatoires ne sont remplis alors elle  doit recommencer son inscription.
    - Le pseudonyme est déjà pré-existant dans la base de donnée, dont elle doit recommencer l'inscription.
    - Le mot de passe est trop court et ou ne comporte pas de chiffre et de caractère, alors elle doit recommencer.

- Post-conditions: la famille a un de compte sur le site.

Authentification sur le site
****************************

- Acteurs: Tout les utilisateurs enregistrés sur le site (baby-sitters,
   familles, administrateurs)

- Pré-condition: L’utilisateur n’est pas authentifié.

- Scénario nominal:

   #. L’utilisateur se dirige sur la partie d’authentification.

   #. L’utilisateur donne son “Identifiant” correspondant à son pseudonyme et son “Mot de passe”.

   #. L’utilisateur valide les entrées et attend la validation des
      informations entrées.

- Extension: Si les informations entrées sont fausses, il est demandé
   d’essayer à nouveau.

- Post-condition: L’utilisateur est connecté et peut maintenant
   profiter de ses droits.

Le baby-sitter modifie ses disponibilités
*****************************************

- Acteurs: baby-sitter

- Pré-condition: le baby-sitter est authentifié (voir ).

- Scénario nominal:

   #. Le baby-sitter se dirige vers la gestion des disponibilités, et se
      dirige ensuite sur la modification des disponibilités.

   #. Il indique alors sa disponibilté régulière à
      l’aide des instructions indiquées.

   #. Il indique les jours de la semaine où il est disponible, ainsi que le nombre de
   semaine où cela entre en vigueur et les horaires de début et fin de ses disponibilités.

   #. Il indique son prix horaire par heure.

   #. Une fois le formulaire de << modification des disponibilités >>
      complètement terminé, il peut valider la modification de ses
      disponibilités.

- Extensions:
    - le baby-sitter abandonne la modification des disponibilités.
    - le baby-sitter valide la démarche de modification avant d’avoir complètement terminé les différentes étapes nécessaires, le site
      indiquera les étapes à compléter avant de pouvoir faire la
      modification.
    - le baby-sitter ajoute une disponibilité ponctuelle en renseignant
      le début et la fin de cette disponibilité ainsi que son taux horaire.
    - le baby-sitter peut ajouter ou supprimer une zone de travail indiquée par un code postal.

- Post-condition: le baby-sitter a mis à jour ses disponibilités.

Faire une demande de garde
**************************

- Acteurs: Famille

- Pré-condition: La famille est authentifié (voir ).

- Scénario nominal:

   #. La famille authentifiée va sur sa page d'acceuil.

   #. Elle renseigne dans les différents champs: le jour, l'heure de début et la durée de la garde.
      Elle valide alors ses choix de recherche.

   #. Elle consulte les différents baby-sitters disponibles
      selon les critères de recherches indiqués. Le système calcule pour
      chacun des baby-sitter le prix de la garde en fonction du tarif
      indiqué par le babysitter auquel se rajoute la commission du site.

   #. L’utilisateur envoie une demande de garde à un baby-sitter.

- Extensions :
   - La famille n’est pas satisfait des profils, elle n’envoie pas de
     demande.
   - Il n’y a pas de baby-sitter correspondant à la demande, la famille
     est invitée à ré-essayer plus tard.
   - La famille peut recommencer cette étape autant de fois que
     souhaitée, les demandes pour les familles sont illimitées.
   - La famille consulte le profil complet d'un baby-siter
     avant d'envoyer sa demande. Un nouvel onglet apparait alors avec
     ses informations, dont les différents commentaires laissés par les familles,
     son taux d'annulation, son âge, son sexe, sa note moyenne et la date où ce
     dernier a rejoint Garde-chou.

- Post-condition: La famille a envoyé une demande de garde.

Accepter une garde
******************

- Acteurs: Baby-sitter

- Pré-conditions:
    - Le baby-sitter est authentifié (voir ).
    - Au moins une demande de garde a été addressée au baby-sitter.
    - Dans le cas où le prix de la garde est supérieur au seuil fixé
      par l'administrateur, l'administrateur doit "accepter"la garde.

- Scénario nominal:

   #. L’utilisateur consulte les différentes demandes de garde envoyées
      par les familles.

   #. L’utilisateur accepte la demande d’une famille.

- Extension : Le baby-sitter refuse la demande de la famille.

- Post-condition: La garde est acceptée par le baby-sitter. Le baby-sitter et la famille sont maintenant liés,
   leurs coordonnées sont alors mutuellement échangées.

Une famille annule une garde
****************************

- Acteurs: famille.

- Pré-conditions:
   - La famille est authentifiée (voir ).
   - La garde a été acceptée par le baby-sitter (voir ).
   - Le début de la garde est dans plus de 48h.

- Scénario nominal:

   #. Elle se rend sur les gardes actives.

   #. Elle choisit la garde qu’elle souhaite annuler.

   #. Elle confirme qu’elle souhaite bien annuler la garde.

- Extension: Si les 48h sont écoulées, le compte de la famille est débité.

- Post-condition: le système envoie un message au babysiter pour le lui signaler.

Un baby-sitter annule une garde
*******************************

- Acteurs: baby-sitter.

- Pré-conditions:
   - L’utilisateur est authentifié.
   - L’utilisateur a accepté la demande émise par la famille.
   - Temps : 48 heures avant le jour “J” et la tranche horraire “H”.

- Scénario nominal:

   #. Le baby-sitter se dirige vers la gestion de garde.

   #. Le baby-sitter selectionne la garde à annuler.

   #. Il confirme qu’il souhaite bien annuler la garde.

- Post-condition: le système envoie un message à la famille pour le lui
   signaler.

Valider une garde
*****************

- Acteurs: Famille, Administrateur.

- Pré-conditions:
   - Le baby-sitter choisit a accepté la demande émise par la famille.
   - La famille n’a pas annulé la garde.
   - Le baby-sitter n’a pas annulé la garde.
   - La garde a été potentiellement effectuée (la date est dépassée) et son action et effectuée moins de 48h après que la garde ait
     lieu.
   - La famille est authentifiée.

- Scénario nominal:

   #. La famille recherche la garde concernée.

   #. Elle clique pour cette garde sur "validation". Elle est redirigée sur une nouvelle page.

   #. Elle choisit, parmis une liste déroulante la qualité du déroulement de la garde.

- Extensions:
   - La famille contacte le service clientèle en cas d’absence du
     babysitter.
   - Elle décide d'écrire un commentaire sur la garde effectuée. Ce commentaire aparaîtra sur le profil du baby-sitter après
     validation de l'administrateur. Elle pourra le modifier tant que l'administrateur ne l'aura pas validé.

   - Post-condition: L’issue de la garde est indiquée.

Le babysitter met un commentaire
********************************

- Acteurs: Babysitter, Administrateur

- Pré-conditions:
   - Le babysitter est authentifié.
   - La garde a été potentiellement effectuée et il réalise son action moins de 48h après que la garde ait
     lieu.

- Scénario nominal:

       #. Il recherche la garde effectuée. Il est redirigée sur une nouvelle page.

       #. Il choisit, parmis une liste déroulante la qualité du déroulement de la garde.

       #. Il écrit un commentaire sur le profil de la famille.

- Extensions:
    - Le babysitter supprime ou modifie le commentaire qu’il a posté.
    - Le commentaire est modéré par l’administrateur.

- Post-condition: Le babysitter a commenté la garde effectuée sur le profil de la famille.



Fixer la commission
*******************

- Acteurs: Administrateur

- Pré-conditions:

   - L’administrateur est authentifié.

- Scénario nominal:

   #. Il choisit de se modifier la commission du site.

   #. Il entre une valeur pour la commission.

   #. Il valide le montant.

- Extensions:
    - La commission est inférieure à zéro, le scénario est terminé.
    - La commission n'a jamais été modifiée par l'administrateur et est donc par défault égale à zéro.

- Post-condition : L'administrateur a fixé la commission.