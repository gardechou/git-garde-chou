Présentation générale
---------------------

.. raw:: latex
    :file: ../../diagrams/use_cases.tex

Garde-chou est un service en ligne qui permet de mettre en relation des
baby-sitters et des familles qui ont besoin de faire garder leurs
enfants.

La :autoref:`use-cases` présente les principaux cas d’utilisation. Pour améliorer la
lisibilité, on considère ici les acteurs comme déjà authentifiés sur le
site. Les modalités de paiement n’apparaissent pas non plus.

Présentation détaillée
----------------------

Les clients
~~~~~~~~~~~

Les baby-sitters
****************

Les baby-sitters doivent renseigner:

-  Leurs noms et prénoms.

-  Leur pseudonyme.

-  Leur sexe et leur date de naissance.

-  Leurs adresses e-mail.

-  Leurs numéros de téléphone.

-  Leurs *disponibilités*, c’est-à-dire:

   -  Les zones géographiques dans lesquelles ils peuvent se rendre pour
      travailler en précisant une liste de codes postaux.

   -  Une liste d’horaires auxquels ils sont disponibles avec le tarif
      horaire correspondant.

- Un multiplicateur de tarif horaire pour ajuster le prix au nombre d’enfants gardés.

Les baby-sitter peuvent fournir:

-  Un texte de présentation.

-  Une image de profil.

Avant d’être actif, un compte baby-sitter doit être validé par un
administrateur.

Un visiteur du site peut consulter la liste des baby-sitters
disponibles pour un code postal. Le visiteur a accès au pseudo, à la description et une image de profil que le baby-
sitter aura potentiellement renseignée.

Une famille authentifiée peut consulter toutes les informations sur
chaque baby-sitter, y compris le taux d’annulation de garde et les
commentaires laissés par d’autres familles.

Les familles
************

Les familles doivent renseigner:

-  Leurs noms et prénoms.

-  Leur pseudonyme.

-  Leurs coordonnées physiques: adresse complète, la ville et code postal.

-  Leur adresse e-mail.

-  Leur numéro de téléphone.

-  Leur mot de passe valide.

Les familles peuvent fournir:

-  Un texte de présentation.

-  Une image de profil.

Ces informations ne seront visibles que par les baby-sitters auxquels
elles auront fait une demande de garde. Seront également visibles les
commentaires laissés par les baby-sitters dont les gardes avec cette
famille ont été *validées*.

Les gardes
~~~~~~~~~~

Une famille enregistrée et authentifiée peut trouver un baby-sitter de
la manière suivante:

-  Elle entre des horaires de début et de fin de garde, et un nombre
   d’enfants à garder.

-  Garde-chou lui propose une liste de baby-sitters disponibles aux
   horaires demandés et dans la zone géographique précisée par la
   famille lors de son inscription (qui pourra être modifiée par la
   suite). Pour chacun des baby-sitters, le prix de la garde est
   calculé par garde-chou selon les critères définis par les
   baby-sitters.

-  Elle peut envoyer plusieurs *demandes*, si son solde garde-chou le lui permet (elle peut le recharger le cas échéant).

-  L’\ *acceptation* par un baby-sitter annule les autres *demandes* de
   garde et soustrait le prix de la garde du solde de la famille. Le
   baby-sitter qui a *accepté* peut alors voir l’adresse complète de la
   famille.

-  Une fois que la garde a été effectuée, la famille dispose d’un
   certain délai pour la *valider* et l’argent est alors crédité sur le
   solde du baby-sitter. Au-delà de ce délai, la garde est
   automatiquement *validée* et l’argent est également crédité.

-  Chaque partie peut alors laisser un commentaire sur l’autre pendant
   un certain temps, mais ceux-ci ne peuvent être lus que lorsque les
   deux ont soumis leurs commentaires.

Validation
**********

Lorsque l’horaire de fin d’une garde est dépassé, la famille dispose
d’un délai pendant lequel elle peut l’\ *invalider*. Cela peut arriver
si le baby-sitter ne s’est pas présenté au rendez-vous ou pour une autre
raison à préciser. Le baby-sitter a la possibilité de contester cette
invalidation, ce qui ouvrira un dossier de litige à traiter au cas par
cas par un des administrateurs.

Annulation
**********

Une garde peut être *annulée* par un baby-sitter ou une famille jusqu’à
48 heures avant son déroulement. L’argent est alors recrédité sur le
compte de la famille.

Avortement
**********

Une garde peut être *avortée* par une famille jusqu'à 48 heures avant son 
déroulement tant que le baby-sitter ne l'a pas *validée* ou *refusée*.
L’argent est alors recrédité sur le compte de la famille.

Interdiction ou Autorisation
****************************

Les gardes dont le prix est supérieur au seuil fixé par l'administrateur 
sont *en attente d'autrisation*. L'administrateur peut alors choisir entre
*autoriser* ou *interdire* la garde. Mais, si il ne choisit rien, elle est
alors *ignorée*. Une garde dont le prix ne dépasse pas ce seuil est par 
défaut *autorisée* mais l'administrateur peut toujours *l'interdire*. Une
garde peut être *interdite* par l'administrateur jusqu'à 48 heures 
avant son déroulement. Si une garde est *interdite* ou *ignorée*, alors 
l’argent est recrédité sur le compte de la famille. 

Une garde ignorée
*****************

Une garde *demandée* est *ignorée* lorsqu'après son déroulement la famille ne l'a 
pas *avortée* et le baby-sitter ni *acceptée* ou *refusée*. Une garde *en attente d'autrisation*
est *ignorée* après son déroulement si l'administrateur ne l'a pas *autorisée* ou *interdite*.
Quand une garde est *ignorée* l'argent est recrédité sur le compte famille.

L’argent
~~~~~~~~

.. raw:: latex
    :file: ../../diagrams/argent.tex

Les familles et les baby-sitters disposeront d’un compte garde-chou
associé à leurs profils, de manière à simuler l’échange d’argent. Il y
aura la possibilité de recharger ou de solder ces comptes. Le compte de
la famille ne peut être négatif. Si une famille veut envoyer une
demande, le solde de son compte doit être supérieur ou égal au tarif du
baby-sitter choisit.