Défauts
=======

Défauts fonctionnels
--------------------

Si c'était à refaire nous rédigerions probablement des spécifications plus détaillées.
En effet, en développant nous avons dû régler certains détails sur le fonctionnement précis d'une garde et du système de commentaire, des éléments que nous n'avions pas anticipé.

Ci-suit une critique des principaux défauts restant du site.

Inscription
~~~~~~~~~~~

L'adresse email est le seul lien de communication officiel entre l'entreprise et l'utilisateur, il est donc primordial de s'assurer que l'internaute fournit une adresse email qui lui appartient. Or, rien n'est mis en place pour vérifier que l'adresse e-mail est valide.

Annulation de garde
~~~~~~~~~~~~~~~~~~~
Lorsque l'on dépasse le délai d'annulation d'une garde, rien n'est possible tant qu'elle n'est pas écoulée.
Pourtant, on peut imaginer que dans certains cas, la famille ou le baby-sitter savent pertinemment qu'elle n'aura pas lieu.
Actuellement, rien n'est prévu pour ce cas

En plus de cela, si un babysitter accepte un garde qui commence sous peu, il ne peut plus faire marche arrière sur sa décision.

Modération de commentaires
~~~~~~~~~~~~~~~~~~~~~~~~~~
Si un commentaire est posté quelques minutes avant la fin de la date limite de commentaire imposée par l'administrateur, il est quasiment tout de suite publié sans vérification de son contenu.

Authentification persistante
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Une option "se souvenir de moi" à la connection rendrait l'expérience utilisateur plus agréable.

Défauts techniques
------------------

Base de données
~~~~~~~~~~~~~~~

Il aurait été plus pratique de créer plus de tables comme par exemple :
- une table à part pour les commentaires.
- une table à part pour le solde dans le but de stocker les transactions.

En ce qui concerne les attributs des tables, nous aurions probablement dû stocker quelques informations de manière redondantes, pour éviter des jointures systématiques. L'exemple le plus frappant est celui de la table garde qui gagnerait à stocker les pseudos des client impliqués. Une double jointure sur la table ``user`` est effectuée quasiment à chaque fois que l'on s'intéresse à une garde.

php
~~~

Il serait intéressant d'utiliser les *namespaces* qui fournissent un moyen de regrouper des classes, interfaces, fonctions ou constantes. Les espaces de noms sont conçus pour résoudre deux problèmes, le premier étant les collisions de noms entre le code créé, les classes, les fonctions ou les constantes internes de PHP. Le deuxième problème est la capacité de faire des alias ou de raccourci des noms extrêmement long pour aider à la résolution du premier problème, et améliorer la lisibilité du code. Nous avons découvert malheureusement découvert cette option un peu tard pour l'intégrer à notre projet.

Un moteur de *template* permettrait de rassembler le code de présentation (tout ce qui est HTML et CSS) et le code d'application (requête en PHP et autres). L'utilisation d'un moteur de template aiderait dans la lisibilité et la logique du projet en général et de son code en particulier. Couplé à une architecture MVC, ce système donne d'excellentes performances. 
Il permettrait en outre une composition des *vues* plus riches. Actuellement, on peut seulement empiler *linéairement* plusieurs vues.

Design (HTML/CSS)
~~~~~~~~~~~~~~~~~

L'inscription n'est pas assez mise en valeur sur le site, le fait qu'elle soit attirante pourrait encourager les internautes à devenir membres.

Certaines fonctionnalités comme les calendrier s'affichant pour choisir les dates ne sont actuellement disponibles que dans Google Chrome (et Chromium).
L'étape incontournable pour une vraie mise en production du site serait d'assurer la compatibilité avec les principaux navigateurs du marché.


Javascript
~~~~~~~~~~

Utiliser plus de Javascript permettrait:

- de ne pas générer une nouvelle page à chaque fois que l'on exécute une *action*. Cela réduirait la quantité de données qui passent par le réseau ainsi que la charge processeur et mémoire sur le serveur.

- d'améliorer l'expérience utilisateur en permettant une navigation plus fluide.


