Les classes métier
------------------

.. raw:: latex
    :file: ../../diagrams/classes.tex

La :autoref:`classes` schématise les relations entre les classes métier de *garde-chou*.
Les héritages concernent uniquement les différents utilisateurs, qui selon leur type disposent d'attributs et de méthodes différentes.
Les commentaires et les gardes sont systématiquement liés à un baby-sitter et une famille.
À chaque client peut correspondre une infinité de commentaires et gardes.
À chaque baby-sitter peut correspondre une infinité de disponibilités.

.. raw:: latex
    :file: ../../diagrams/state.tex

Les états de la classe garde (:autoref:`state`) dépendent à la fois d'actions des utilisateurs et du temps.
En effet, à chaque garde sont associés des temps limites aux delà desquels elle:

- ne peut plus être acceptée/refusée par un babysitter ou autorisée/interdite par un administrateur: son horaire de début. À ce moment-là, son état passe à ignorée.
- ne peut plus être annulée.
- ne peut plus être validée/invalidée par une famille: elle est alors validée automatiquement.

Base de données
---------------

.. raw:: latex
    :file: ../../diagrams/db.tex

La schéma de base de données utilisé est représenté sur la :autoref:`db`. Il a été conçu en trouvant un équilibre entre deux objectifs opposés:

- ne pas stocker d'informations redondantes
- réduire la complexité des requêtes qui seront le plus souvent faites.

Table user
~~~~~~~~~~

Cette table stocke les informations relatives à tous les utilisateurs du site.

Pour certaines catégories comme les administrateurs, une grande partie des attributs peuvent être nuls.
Cependant il nous a semblé préférable de regrouper toutes les informations sur les utilisateurs dans une seule table, notamment pour unifier la mise en place du système d'authentification sur le site.

Le mot de passe est stocké de manière chiffrée dans la base de données selon les recommandations de la documentation php [#]_.
C'est actuellement l'algorithme *bcrypt* qui est utilisé.

.. [#] http://php.net/manual/fr/faq.passwords.php

Le sexe des baby-sitters est représenté sous forme d'un entier selon la norme ISO/IEC 5218 [#]_

.. [#] http://www.iso.org/iso/iso_catalogue/catalogue_tc/catalogue_detail.htm?csnumber=36266

Table garde
~~~~~~~~~~~

Cette table stocke les informations relatives aux transactions (les gardes) sur le site.

Chaque entrée contient deux clés étrangères correspondant à un babysitter et une famille.

Les commentaires et les notes étant nécessairement liés à une garde, ils sont également stockés dans cette table.

Nous aurions pu éviter de stocker les dates limites de validation, d'annulation et de *commentabilité* d'une garde au sein de cette table,
puisqu'elles peuvent être retrouvées en se référant à la table paramètres.
Pour des raisons pratiques, et parce que ces dates limites seront très souvent sollicités lorsque nous agirons sur les gardes,
il nous a semblé préférable de stocker ces informations de manière redondante.

Table dispo
~~~~~~~~~~~

Cette table stocke les disponibilités temporelles des babysitters.
Il s'agit d'une tranche horaire (deux *timestamps*) associée à un prix et un babysitter


Table dispo_geo
~~~~~~~~~~~~~~~

Pour stocker les disponibilités géographiques (liste de codes postaux) associés à chaque babysitter, une table à part est utilisée.
Elle n'a que deux attributs, un code postal et un babysitter.
C'est ainsi qu'il sera possible pour les baby-sitters d'être associés à une liste de différents codes postaux.

Table parametres
~~~~~~~~~~~~~~~~

Cette table stocke les paramètres définis par un administrateur à une date donnée.
Les paramètres ont une influence sur les dates limites et le status des gardes demandées.