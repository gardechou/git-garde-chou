Les classes php
---------------

Une documentation technique très détaillée du code source est générée à partir des *docblocks*
(commentaires formatés selon des conventions bien précises)
grâce à *phpDocumentor* [#]_.
Elle est disponible sur https://www.nicoco.fr/gardechoudoc, c'est pourquoi nous détaillerons
ici seulement certains aspects fondamentaux de l'organisation du code.

Nous avons choisi de développer le site en respectant le patron de conception
modèle-vue-contrôleur(-service) en raison des avantages qu'il confère:

- pour faciliter la division du travail.
- pour la lisibilité et par conséquent l'évolutivité du code.

.. [#] https://www.phpdoc.org/

.. raw:: latex
    :file: ../../diagrams/mvc.tex

Les modèles
~~~~~~~~~~~

Ils correspondent aux classes métier à proprement parler.

De manière générale, ils comprennent des méthodes statiques *usines à instances* qui permettent d'en générer des instances selon
certains critères à partir de la base de données. Exemple: la méthode ``Garde::genForActor($user_id)`` retourne des instances de ``garde`` dans lequel l'utilisateur ``$user_id`` est impliqué.

Toujours dans la classe ``Garde``, certaines méthodes permettent de savoir si son status peut être modifié, ce qui sera utilisé pour l'affichage de
l'interface utilisateur (faire apparaître ou non l'option), mais aussi pour vérifier les droits avant le changement effectif d'état.
Exemple: ``$garde->isAnnulableBy($user_id)`` renvoie un booléen qui répond à la question: « La garde est-elle être annulée par tel utilisateur? ».

À titre d'exemple, voici la méthode de la classe ``Dispo`` qui permet de s'assurer qu'une disponibilité temporelle ajoutée par un baby-sitter
n'est pas en conflit avec une de ses autres disponibilités:

.. raw:: latex

    \begin{minted}[fontsize=\footnotesize]{php}
    <?php
    class Dispo {
    (...)
    /**
     * L'instance de dispo est est-elle en conflit temporel avec une liste de dispos?
     *
     * @param Dispo[] La liste de dispos vs laquelle tester la dispo
     * @return bool
     */
    public function hasConflictWith($dispos)
    {
        $conflict = false;
        $i = 0;
        while (!$conflict and $i < count($dispos)) {
            $dispo = $dispos[$i];
            if ($this->overlap($this->debut,
                               $this->fin,
                               $dispo->getDebut(),
                               $dispo->getFin())) {
                $conflict = true;
            }
            $i++;
        }
        return $conflict;
    }
    \end{minted}


Les services
~~~~~~~~~~~~

Tous les appels à la base de données sont centralisés dans ce *package*.
Cela permet de ne devoir changer que les méthodes présentes dans ces classes en
cas d'évolution du schéma de la base de données.

Chaque classe métier dispose d'un service associé, ses méthodes jouant le rôle de mapping objet-relationnel.
En exemple, voici une méthode de ``UserService`` permettant de retourner le taux d'annulation moyen pour un utilisateur.
Elle ne sera appelée a priori que par des méthodes des modèles Babysitter et Famille:

.. raw:: latex

    \begin{minted}[fontsize=\footnotesize]{php}
    <?php
    class UserService {
    (...)
    /**
     * Calcule le taux d'annulation d'un client ainsi que le nombre de gardes
     * permettant le calcul de ce taux.
     *
     * @param int $id L'id du client
     * @param int $type facultatif mais requête plus performante si précisé.
     * @return array ["taux_annulation"=>float, "n_toget_taux_ann"=>int]
     */
     */
    public static function getTauxAnnulationForUser($id, $type = null)
    {
        $valide = Garde::DB_VALIDEE;
        $annulee = Garde::DB_ANNULEE;

        if (is_null($type)) {
            $query = Database::getPdo()->prepare($sql1);
        } else {
            $user_type = ($type == User::TYPE_BABYSITTER) ? "babysitter" : "famille";
            $query = Database::getPdo()->prepare($sql2);
            $query->bindParam(":type", $type, PDO::PARAM_INT);
        }
        $query->bindParam(":id", $id, PDO::PARAM_INT);

        $query->execute();
        $data = $query->fetch();
        $n_annul = $data['n_annul'];
        $n_accept = $data['n_gardes'];
        $data_res = array(
            "taux_annulation" => ($n_accept != 0) ? $n_annul / $n_accept : 0,
            "n_toget_taux_ann" => $n_accept
        );

        return $data_res;
    }
    \end{minted}

Pour améliorer la lisibilité, les requêtes SQL sont ici présentées à part:

.. raw:: latex

    \begin{minted}[fontsize=\footnotesize]{mysql}
    # valide et annulee sont des entiers et sont définis comme constantes dans la classe Garde
    # user_type est un entier défini comme constante dans la classe User
    # $sql1
    SELECT
      SUM(annuleur = :id) AS n_annul,
      SUM(status IN (valide, annulee)
          AND :id IN (babysitter, famille)) AS n_gardes
    FROM garde
    WHERE
      annuleur = :id
      OR (status IN (valide, annulee) AND :id IN (babysitter, famille));
    # $sql2
    SELECT
      SUM(annuleur = :id) AS n_annul,
      SUM(status IN (valide, annulee)
          AND user_type = :id) AS n_gardes
    FROM garde
       WHERE annuleur = :id OR
            (status IN (valide, annulee) AND user_type = :id)
    \end{minted}




Certains services *spéciaux* regroupent des fonctionnalités utilisées par toutes les pages (liste non exhaustive):

- La classe ``Database`` gérant la connexion à la base de données. Elle utilise le patron de conception singleton afin de ne créer qu'une seule connexion à *MariaDB* par exécution d'une action [#]_.
- La classe ``Page`` permet aux contrôleurs de choisir les vues utilisées, le titre de la page web affichée, et implémente un système unifié d'affichage des messages d'erreur, d'information ou d'avertissement à l'utilisateur.
- La classe ``FormChecker`` fournit une manière standardisée de traiter les entrées de l'utilisateur.

.. [#] Ceci n'est vrai que dans le cas où une action n'entraine pas de redirection.

Les contrôleurs
~~~~~~~~~~~~~~~

Les différentes actions des utilisateurs sont implémentées dans des contrôleurs sous forme de méthodes statiques.

Grâce à de la ré-écriture d'URL via un fichier ``.htaccess``, elles peuvent être appelées ainsi:

.. math::
    https://\underbrace{www.nicoco.fr}_{\text{domaine}}/\underbrace{gardechou}_{URI\_PREFIX}/\underbrace{garde}_{\text{contrôleur}}/\underbrace{ajouter}_{\text{action}}/

Toutes les requêtes sont ainsi redirigées vers le fichier ``src/web/index.php``, qui se charge d'appeler l'action demandée.

Le déroulement d'une action est typiquement le suivant:

- L'action du contrôleur reçoit ses *arguments* via POST et/ou GET (input)
- Elle fait appel à des *services* et des *modèles* pour consulter et/ou modifier la base de données
- Elle utilise la service ``Page`` pour afficher des messages à l'utilisateur et/ou lister un ensemble de données. Pour ceci, le contrôleur doit également définir quelle(s) vue(s) doi(ven)t être utilisée(s).

Le code suivant illustre comment supprimer une disponibilité pour un babysitter.
``FormChecker`` est utilisé pour contrôler le contenu de POST.
Les méthodes ``addError`` et ``addInfo`` de ``Page`` permettent d'informer l'utilisateur du bon déroulement de l'opération ou non.
On re-dirige enfin vers un autre contrôleur en utilisant ``Page::go``.

.. raw:: latex

    \begin{minted}[fontsize=\footnotesize]{php}
    <?php
    class DispoController {
    (...)

    /**
     * Supprimer une disponibilité temporelle
     *
     * POST doit contenir "dispo_id". La dispo est supprimé si elle appartient
     * bien à l'utilisateur actuellement connecté.
     * "dispo_id" peut aussi être "all".
     */
    public static function supprimerAction()
    {
        $form = new FormChecker($_POST);
        $form->addField("dispo_id", "ID de la disponibilité", "/\d*|all/");

        if (!$form->parse()) {
            // Uniquement si l'utilisateur bidouille ce qui passe par POST
            Page::addError(
                "ID invalide ou non précisé. Comment vous êtes-vous retrouvé ici&nbsp;?");
            Page::goBack();
        }

        $dispo_id = $form->getStructuredArray()["dispo_id"];

        if ($dispo_id == "all") {
            DispoService::removeAllForBabysitter(Session::getCurrentUserId());
            Page::addInfo("Toutes vos disponibilités ont été supprimées.");
            Page::go("/dispo/lister");
        }

        if (DispoService::removeIfProprio($dispo_id, Session::getCurrentUserId())) {
            Page::addInfo("Disponibilité supprimée.");
        } else {
            Page::addError("Impossible de supprimer la disponibilité.");
        }

        Page::goBack();
    }
    \end{minted}

Note: il n'y a pas besoin de vérifier que l'utilisateur qui connecté est bien un babysitter grâce à une utilisation atypique de l'*autoloader* de classes [#]_.

.. [#] voir https://www.nicoco.fr/gardechoudoc/files/service.AutoLoader.html pour plus de détails

Les vues
~~~~~~~~

On ne trouve dans ces fichiers que très peu de php et c'est la seule partie du code où l'on doit trouver du HTML.
Voici comment par exemple les commentaires qu'un utilisateur a posté lui sont affichés:

.. raw:: latex

    \begin{minted}[fontsize=\footnotesize]{php}
    <legend>Les commentaires que j'ai laissés</legend>
    <?php foreach ($comments as $comment) { ?>
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    sur
                    <a href="<?= URI_PREFIX . "/user/profile?id=" . $comment->getOnId() ?>">
                        <strong><?= $comment->getOnPseudo() ?></strong></a>
                        <span class="text-muted"> le <?= strftime(DATE_FORMAT, $comment->getDate()) ?
                        </span>
                    <?php if (!$comment->isPublished()) { ?>
                        <a href="<?= URI_PREFIX . "/garde/commenter?id=" . $comment->getGardeId() ?>">
                            Modifier
                        </a>
                    <?php } ?>
                </div>
                <div class="panel-body">
                    <?= $comment->hasContent() ? $comment->getContent() : "<em>Pas de texte</em>" ?>
                    <br/>Note&nbsp;: <?= $comment->getNote() ?>/5
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if (empty($comments)) { ?>
        Aucun commentaire
    <?php } ?>
    \end{minted}
