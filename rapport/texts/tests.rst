Tests
=====

Dans le but de voir si les spécifications fonctionnelles ont bien été respectées, nous avons créé une matrice de test.

Le principe est simple: il consiste premièrement à déterminer pour un utilisateur l'accès aux différentes fonctionnalités.
Le premier cas est que l'utilisateur n'a pas accès à cette fonctionnalité, la case est alors remplie par le symbole "X".
Le second cas, est que l'utilisateur y a accès. L'étape suivante consiste à vérifier la comportement de notre logiciel, nous souhaitons qu'il respecte les spécifications.
Si la consigne du document est respectée, la case contiendra le chiffre "1" et dans le cas contraire un "0".

Nous avons donc testé tout les cas afin de remplir la matrice et se situer dans l'exactitude de notre projet, c'est à dire vérifier que les demandes du client sont correctement satisfaites.
Elle nous a également permis de déceler certains bugs.
Nous avons pris l'initiative d'utiliser le *Bugtracker* proposé par *BitBucket* afin de hiérarchiser (par ordre d'importance) les différents bugs découverts.
Lors de la découverte d'un bug, il est possible de créer une nouvelle *Issue*: il est possible de lui donner un nom; comme par exemple "Calcul du prix incorrect",
d'assigner un niveau d'importance allant de "Mineur" à "Majeur", et enfin on peut indiquer le membre qui devra contribuer à la réalisation de cette tâche.

On obtient alors une liste ordonnée de tâches à effectuer dans l'optique de faire correspondre au maximum les fonctionnalités globales du sites et les spécifications rédigées selon les demandes du client.