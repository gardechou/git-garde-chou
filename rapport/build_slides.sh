#!/usr/bin/env bash

name="slides"
outputfile="$name.pdf"
buildfolder="build"
latexfile="$name.tex"

rm $buildfolder/*
cp soutenance/* $buildfolder
cat diagrams/db_mp.tex | sed 's/topToBottom(20)(garde, user);//' | sed 's/leftToRight(40)(dispo_geo, user);/leftToRight(40)(dispo_geo, user, garde);/' > $buildfolder/db_mp_horizontal.tex

cd $buildfolder # apparemment il faut être dans le bon répertoire sinon metapost n'est pas content
pdflatex -shell-escape -interaction=nonstopmode $latexfile
lualatex -shell-escape -interaction=nonstopmode $latexfile # 2 fois pour les références, table de matières etc.
mv $outputfile ..
