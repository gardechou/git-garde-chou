.. role:: ref
.. role:: autoref
.. role:: label

.. include:: ../texts/intro.rst
.. include:: ../texts/spec_fonc.rst
.. include:: ../texts/spec_techniques.rst
.. include:: ../texts/realisation.rst
.. include:: ../texts/tests.rst
.. include:: ../texts/critique.rst
.. include:: ../texts/conclusion.rst