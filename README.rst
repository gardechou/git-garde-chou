Garde-chou
==========

Site web en php destiné à mettre en relation des babysitters et leurs clients ("familles"), réalisé dans le cadre
d'un projet de master 2 "biologie, informatique et mathématiques" à l'université de Nice Sophia.

/src
----
Le code est organisé selon l'architecture "modèle-vue-contrôleur-service" dans le sous-répertoire src/ de la manière suivante:

src/web/
~~~~~~~~
Seul le sous-répertoire **web/** est accessible directement via l'extérieur (via le web).
Toutes les requêtes HTTP concernant les URI commençant par le préfixe du site sont redirigées vers src/web/index.php grâce au fichier **src/web/.htaccess** dans lequel doit être défini l'URI racine du site.

src/controller/
~~~~~~~~~~~~~~~
Ces URI doivent être formées de la manière suivante: URI_PREFIX/*contrôleur*/*action*.
index.php appelle alors la méthode *action* de la classe *contrôleur* définie dans le répertoire src/controller/*contrôleur*Controller.
Cette méthode se charge d'appeler les méthodes nécessaires à la réalisation de l'action demandée, en utilisant éventuellement des données spécifiées via *HTTP POST* ou *GET*. Cela consiste à préparer les données qui devront être affichés pour l'utilisateur sur son navigateur web. Elle peut appeler d'autres actions, éventuellement définies dans d'autre contrôleurs si nécessaire, en fonction de l'état de la session ou des données fournies par *HTTP POST* ou *GET*.
Elle se charge également de définir les caractéristiques de la page qui sera affiché dans le navigateur de l'utilisateur, comme son titre, les feuilles de style utilisées et le contenu de la page à proprement parler.

src/service/
~~~~~~~~~~~~
Les classes définies dans ce répertoire servent principalement à gérer les interactions avec la BDD. Tout le code SQL se trouve ici.
On y trouve également les classes *Page* et *Session* destinées à gérer l'état de la session et le rendu final de la page dans le navigateur de l'utilisateur.

src/model/
~~~~~~~~~~
Les classes métiers sont définies ici.
Elles disposent éventuellement de méthodes destinées à les instancier à partir de données issues de la BDD, en appelant des méthodes des classes *services* appropriées.

src/view/
~~~~~~~~~
Tout le code *HTML* de gardechou est situé dans ce sous-répertoire.
Le php est limité autant que faire se peut à des *echo*, des *for* et des blocs *if .. else* très simples.

src/cronjobs/
~~~~~~~~~~~~~
Des scripts à exécuter régulièrement sur le serveur pour nettoyer la base de données et valider automatiquement les gardes au bout d'un certain délai.

/db
---
Scripts SQL destinés à initialiser la BDD