# suppression du schema précédent
DROP DATABASE gardechou;

# creation de la base de donnes
CREATE DATABASE gardechou;

# creation de l'utilisateur gardechou.
CREATE USER 'gardechou'@'localhost'
  IDENTIFIED BY 'gardechou';

# autorisation d'ecrire dans la nouvelle database pour le nouvel utilisateur.
GRANT ALL PRIVILEGES ON gardechou.* TO 'gardechou'@'localhost';

USE gardechou;

# suppression des tables si elles existaient (utile si on fait des modifs...)
# creation des tables
CREATE TABLE user
(
  # Numero ID utilisateur, automatiquement incremente.
  id             INT PRIMARY KEY AUTO_INCREMENT,
  pseudo         VARCHAR(16)  NOT NULL,
  nom            VARCHAR(20),
  prenom         VARCHAR(20),
  # 1 = masculin , 2 = feminin - Stockage pour le genre : https://en.wikipedia.org/wiki/ISO/IEC_5218
  genre_humain   TINYINT(1),
  date_naissance DATE,
  # 255 = recommandé sur http://php.net/manual/en/function.password-hash.php
  password       VARCHAR(255) NOT NULL,
  # Nature utilisateur : 0 Administrateur , 1 Famille , 2 Babysitter.
  type           TINYINT(1),
  email          VARCHAR(50)  NOT NULL,
  adresse        VARCHAR(100),
  code_postal    VARCHAR(5),
  ville          VARCHAR(50),
  multiplicateur FLOAT,
  presentation   VARCHAR(500),
  date_creation  TIMESTAMP    NOT NULL,
  solde          FLOAT,
  valide         BOOLEAN,
  telephone      VARCHAR(10),
  pic            VARCHAR(255)
);

# rendre "uniques" pseudos et emails
CREATE UNIQUE INDEX user_pseudo_uindex
  ON gardechou.user (pseudo);

CREATE UNIQUE INDEX user_pseudo_uemail
  ON gardechou.user (email);

CREATE TABLE dispo_geo
(
  # Numero ID dispo-geo, automatiquement incremente.
  babysitter  INT        NOT NULL,
  code_postal VARCHAR(5) NOT NULL,
  FOREIGN KEY (babysitter) REFERENCES user (id)
);

CREATE TABLE dispo
(
  # Numero ID dispo, automatiquement incremente.
  id           INT PRIMARY KEY AUTO_INCREMENT,
  babysitter   INT       NOT NULL,
  #"Debut" de la garde au format heure:date.
  debut        TIMESTAMP NOT NULL,
  #"Fin" de la garde au format heure:date.
  fin          TIMESTAMP NOT NULL,
  #Prix horaire heure
  taux_horaire FLOAT     NOT NULL,
  FOREIGN KEY (babysitter) REFERENCES user (id)
);

CREATE TABLE garde
(
  # Numéro ID garde , automatiquement incremente.
  id             INT PRIMARY KEY AUTO_INCREMENT,
  # id user pour un type = 1 (famille). 
  famille        INT        NOT NULL,
  # id user pour un type = 2 (babysitter).
  babysitter     INT        NOT NULL,
  note_f         TINYINT(1),
  note_bs        TINYINT(1),
  # commentaire de la famille
  comment_f      VARCHAR(255),
  # commentaire du babysitter
  comment_bs     VARCHAR(255),
  # Date de demande de la garde pour appliquer les paramètres en vigueur à ce moment-là
  date_demande   TIMESTAMP  NOT NULL,
  # "Début" de la garde au format heure:date.
  debut          TIMESTAMP  NOT NULL,
  # "Fin" de la garde au format heure:date.
  fin            TIMESTAMP  NOT NULL,
  # Etat de la garde : 0 Attente (admin), 1 Demande , 2 Acceptée , 3 Validée , 4 Invalidée, 5 Refusée, 6 Annulée
  status         TINYINT(1) NOT NULL,
  # Prix de la garde.
  prix           FLOAT      NOT NULL,
  # Prix de la commission sur cette garde.
  commission     FLOAT      NOT NULL,
  # nombre d'enfants sur cette garde
  nb_enfants     TINYINT(1) NOT NULL,
  # date limite d'annulation de la garde
  lim_annulation TIMESTAMP  NOT NULL,
  # date limite de commentaire
  lim_comment    TIMESTAMP  NOT NULL,
  # date limite d'invalidation
  lim_invalid    TIMESTAMP  NOT NULL,
  # L'annuleur
  annuleur       INT,
  FOREIGN KEY (annuleur) REFERENCES user (id),
  FOREIGN KEY (famille) REFERENCES user (id),
  FOREIGN KEY (babysitter) REFERENCES user (id)
);

# CREATE TABLE commentaire
# (
#   # Numéro ID commentaire, automatiquement incrémenté.
#   id          INT PRIMARY KEY AUTO_INCREMENT,
#   # id user auteur du commentaire.
#   author      INT          NOT NULL,
#   # id user qui est concerne par le commentaire.
#   concerned   INT          NOT NULL,
#   # Date du commentaire.
#   date        TIMESTAMP    NOT NULL,
#   # Le commentaire est limite par le nombre de caractère.
#   commentaire VARCHAR(250) NOT NULL,
#   # La garde à laquelle le commentaire se réfère
#   garde       INT          NOT NULL,
#   # Le status de modération
#   valide      BOOLEAN,
#   FOREIGN KEY (author) REFERENCES user (id),
#   FOREIGN KEY (concerned) REFERENCES user (id),
#   FOREIGN KEY (garde) REFERENCES garde (id)
# );

CREATE TABLE parametres
(
  # La data à laquelle les paramètres ont été changés
  date              TIMESTAMP NOT NULL,
  # L'id de l'admin qui a changé la date
  set_by            INT       NOT NULL,
  # Le taux de commission appliquée par le site
  commission        FLOAT     NOT NULL,
  # Le délai possible avant le début d'une garde pour l'annuler, en heures
  delai_annulation  INT,
  # Le délai possible après la fin d'une garde pour laisser un commentaire la concernant, en heures
  delai_commentaire INT,
  # Le délai avant qu'une garde ne soit validée automatiquement, en heures
  delai_validation  INT,
  # Le prix maximum pour qu'une garde soit validée automatiquement
  prix_max          INT,
  FOREIGN KEY (set_by) REFERENCES user (id)
);