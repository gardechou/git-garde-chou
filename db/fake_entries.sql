# valeurs factices pour la bdd

USE gardechou;

# mots de passe de la forme pseudo_password
INSERT INTO
  user (pseudo, nom, prenom, genre_humain, date_naissance, password, type, email, adresse, code_postal, ville, multiplicateur, presentation,
        date_creation, solde, valide, telephone, pic) VALUES
  (
    'jerem133',
    'Durand',
    'Jeremy',1,
    "1984-01-15",
    '$2y$10$IHv4DAWmGATmxgjhsulA5OFC/Jszh4vgzKbJKeV9S91eJKR5ICtW6',
    2, 'truenicoco@gmail.com', '8 av Tartampion', '75000', 'Paris', 1.2,
    "Etudiant en communication, j'adore les animaux et les enfants.", DATE_SUB(NOW(), INTERVAL 3 MONTH), 144.15, TRUE, "0555445545",
  "1.png"),
  (
    'bogossdu06', 'Baron', 'Cantin',1,"1994-12-15", '$2y$10$i4GbaB4QM7SRRRJXRJ/Jn.NLdFbAVCrAXrcQGRCYQns2nhxyVXBLK',
                  2, 'norab@rab.com', '55 rue de la street', '06400', 'Cannes', 1.5,
                  "J'ai un très bon contact avec les enfants.", DATE_SUB(NOW(), INTERVAL 3 MONTH), 25000, TRUE,
    "0123456789", "2.jpg"),
  ('hadmi', 'Nistrateur', 'Hadmi',1,"1984-09-15", '$2y$10$2PFQudPz8tzgMJK9RZzwI.P2lKFT7uvjESLbFx55In0MoRy3SqLJq',
             0, 'truenicoco@free.fr', NULL, NULL, NULL, NULL, NULL, DATE_SUB(NOW(), INTERVAL 4
                                                                             MONTH), NULL, TRUE, "0123456789", "3.jpg"),
  ('groseille', 'Groseille', 'Marie',NULL,NULL, '$2y$10$9AR55VYKvlsSYQXQk2BDYOqiH6jTbLQhhkSFH9xB/PNk1a0OO6J..',
                1, 'nicoco@nicoco.fr', '84 av Jean Medecin', '06000', 'Nice', NULL,
                "Nous sommes une gentille petite famille, nous cherchons des babysitters sérieux pour faire des gardes ponctuelles.", DATE_SUB(NOW(), INTERVAL 3 MONTH), 1000, TRUE, "0123456789", "4.png"),
  ('Nori', 'Sadouni', 'Nori',NULL,NULL, '$2y$10$Zg05KgZM/AY/HWBWZxQ0JuxKwTzouWgM4/lLAk3h.pkf8Z3cvPvhe',
               1, 'NonoSadouni@laposte.fr', '55 rue du Faubourd Saint-Honoré', '75008', 'Paris', NULL,
               "Nous sommes une famille chaleureuse, nous cherchons un babysitter pour notre fille.",
   DATE_SUB(NOW(), INTERVAL 2 MONTH), 100000, TRUE, "0123456789", "5.jpg"),
  ('Sarah', 'Guez', 'Sarah',NULL,NULL, '$2y$10$RHr7hYAkRsGQE5w8hqx2RudDZW9eADQHEo4tyQAubMCdnaKqLd7Tm',
           1, 'guez@hotmail.dz', '12 boulevard de LADC', '06150', 'Cannes la Bocca', NULL,
           "Famille de cuisinier, nous travaillons le soir et nous cherchons des babysitter sérieux sur le long terme.", DATE_SUB(
       NOW(),
       INTERVAL
       2
       MONTH), 4000, TRUE, "0123456789", "6.png"),
  ('Jacques', 'Ben Soussan', 'Jacques',NULL,NULL, '$2y$10$OY/ZOsen3Uh/BtF3fzEwUePL06bsg6R3C2P.KqJLRsqXRblrPQYzG',
            1, 'JBS@hotmail.fr', '15 rue du Sappes', '06000', 'Nice', NULL,
            "Nous sommes une famille accueillante à la recherche d'un ou d'une babysitter qui soit sérieux et atttentif aux besoins de jeunes enfants.", DATE_SUB(NOW(), INTERVAL 2
                                                                                MONTH), 652, TRUE, "0123456789", "7.png"),
  ('Claire', 'Caroli', 'Claire',NULL,NULL, '$2y$10$es0yTIFsChmUumN7vj.mEOXHti4LZd75ay7x22BPwBvYbpAoRq1xG',
                 1, 'Claire.caroli@hotmail.fr', '36 chemin de la Croute', '06000', 'Nice', NULL,
                 "Je cherche des babysitter pour s'occuper des enfants après l'école", DATE_SUB(NOW(), INTERVAL 2
                                                                      MONTH), 450, TRUE, "0123456789", "7.png"),
  ('Frank', 'Odot', 'Frank',NULL,NULL,'$2y$10$aJvLz59xGliVJxpNB/lTQuViqniaiL5U8FrFDvSBabA6qHCFf2cre',
            1, 'FF@hotmail.fr', '12 boulevard Michelet', '13008', 'Marseille', NULL,
            "Nous sommes une gentille famille, nous pouvons vous ramener chez vous si vous n'avez pas de véhicule", DATE_SUB(NOW(), INTERVAL 2
                                                                                        MONTH), 800, TRUE, "0123456789", "7.png"),
  ('Paoli', 'Paoli', 'Sophie',NULL,NULL,'$2y$10$ieOfwJ/eramfuhwpWAmjMOTBrIarutMIBoyR8TjrqnP.MKKiw/Yeu',
            1, 'Paolisophie@hotmail.fr', '65 avenue de LabelleIle', '06000', 'Nice', NULL,
            "Nous cherchons des babysitters, nous esperons que tout se passera bien pour notre enfant. Sinon, le site perdra un membre...", DATE_SUB(
       NOW(), INTERVAL 1 MONTH), 10000, TRUE, "0123456789", "4.png"),
  ('Sofia', 'Zaidi', 'Sofia',1,"1986-09-15", '$2y$10$ckm.QFEcYffixjPHn5C3U.eInJEAZmpucGkSqQu9WahqzhluJL17q',
            2, 'sofia.zaidi@mail.fr', '85 avenue Atlanta', '06150', 'Cannes la Bocca', 1.9,
            "Jeune étudiante, j'aime beaucoup les enfants, je ne vous décevrez pas !", DATE_SUB(NOW(), INTERVAL 1 MONTH), 50000, TRUE, "0123456789", "4.png"),
  ('Joris', 'Jugnet', 'Joris',1,"1986-09-15",'$2y$10$06vGg4gV4nVJXEwe99trF.70KUBirVzj.1ZfY75e6ZyBrQnKByRrC',
           2, 'JJ@mail.fr', '65 avenue Brooklyn', '06400', 'Cannes', 1.9,
           "Je suis en 1ère à la fac de Sciences de Nice ! Je peux aussi aider vos enfants à faire leurs devoirs !", DATE_SUB(NOW(),
                                                                                                             INTERVAL 1
                                                                                                             MONTH), 50000, TRUE, "0123456789", "1.png"),
  ('Matthieu', 'Andrieu', 'Matthieu',1,"1986-09-15", '$2y$10$S1GIdchvlnSN/hhw87O8nuzjDrjJOXk4oT1hBAVXRUNDMFC5yWFhG',
              2, 'FrenchMontana@gmail.com', '35 avenue du Bronx', '06370', 'Mouans Sartoux', 1.4,
              "Etudiant en droit, j'adore jouer de la guitare, je peux donner des cours à vos enfants :).", NOW(), 1000, TRUE, "0123456789", "1.png"),
  ('Mika', 'James', 'Michaël',1,"1986-09-15", '$2y$10$BRXtHLZ1nWtZJiSHG/r5ZuTpKV/vntq5PXhnFLdF5MTw.Zn0nZZbW',
            2, 'MichaelJames@hotmail.fr', '15 avenue du Port ', '94310', 'Orly', 1.3,
            "Je suis très patient avec les enfants et très attentif à leurs besoins", DATE_SUB(NOW(),
                                                                                                                INTERVAL
                                                                                                                1
                                                                                                                MONTH), 100, TRUE, "0123456789", "1.png"),
  ('YvesA', 'Aldie', 'Yves',1,"1986-09-15", '$2y$10$hCB8XgmWskfRlcya.c1GMeXw4U3QR22J9g7qeM8cGXGhgqyJvZSDO',
            2, 'Yvesaldie@hotmail.fr', '1 avenue des Quatres saisons ', '06000', 'Nice', 1.2,
            "Je dispose du BAFA, et de plusieurs expériences en centres aérés.", DATE_SUB(NOW(), INTERVAL
                                                                                                         1
                                                                                                         MONTH), 325, TRUE, "0123456789", "4.png"),
  ('DidierD', 'Dupont', 'Didier',1,"1986-09-15", '$2y$10$ZebccwSzj0OZ3ZXhpih0Oe1PUptqP1vTpX0kuHTj1HvQAlOEQ8PzW',
              2, 'didier_homme_fort@hotmail.fr', '1 avenue du Bendo', '06000', 'Nice', 1.1,
              "J'adore les enfants, j'en ai moi même eu 5 ! Niveau expérience je suis au niveau !", NOW(), 6000, TRUE, "0123456789", "2.jpg"),
  ('Patrice', 'Guissard', 'Patrice',1,"1986-09-15", '$2y$10$76yGjJzUN5mvQLVwrtOnb.ceoWfkJrSY5/pLfNFJTr7gl4wcf1H2O',
               2, 'Patrick_Lamoule@unice.fr', '12 boulevard Cimiez', '06000', 'Nice', 1.1,
               "Jeune étudiant, je cherche des gardes à faire dans des petites familles", DATE_SUB(
       NOW(), INTERVAL 1 MONTH), 325, TRUE, "0123456789", "5.jpg"),
  ('Jérémy', 'Cassol', 'Jérémy',1,"1986-09-15", '$2y$10$Dc9S4mtsOZz.pHQY6ctVIe9VQqkaerB6XjfP21x7ISCR/fYjYN.V.',
           2, 'Jcassol@hotmail.fr', '13 avenue du Combat', '13000', 'Marseille', 1.3,
           "J'aime la peche et le théâtre.", DATE_SUB(NOW(), INTERVAL 1
                                                                                                 MONTH), 450, TRUE, "0123456789", "2.jpg"),
  ('EmilieL', 'Lapu', 'Emilie',2,"1986-09-15", '$2y$10$cZj0lnRfdYb/3MZbSp6bNOixQkijjADffQUpfT5Y87MqhN5EpIY2O',
              2, 'ELP@gmail.fr', '25 rue du Chemin', '06220', 'Mougins', 1.2,
              "Jeune étudiante pulpeuse disponible pour garder votre enfant et s'occuper de la maison",
       NOW(), 20, FALSE, "0123456789", "2.jpg"),
  ('SophieL', 'Lezak', 'Sophie',2,"1986-09-15", '$2y$10$8Ws6IYEnrAj5SVQezDranO3ppeaJgdfa/oVUtN7od/RPT1GiZg.9W',
              2, 'sophiel@hotmail.fr', '19 avenue du Marechal Joffre', '06000', 'Nice', 1.2,
              "J'adore m'occuper des enfants et jouer avec eux.", DATE_SUB(NOW(), INTERVAL 3
                                                                       MONTH), 100, TRUE, "0123456789", "2.jpg"),
  ('Melanie', 'Durant', 'Melanie',2,"1986-09-15", '$2y$10$D8jhDMwkAEMvQUgI5rpM4OsEh/RBw8pGGksvw0doAXW8gugOrmA8a',
            2, 'melaniedurant@hotmail.dz', '45 avenue Borriglione', '06600', 'Nice', 1.05,
            "Jeune Femme titulaire d'un CAP petite enfance.", DATE_SUB(NOW(), INTERVAL 2
                                                                                     MONTH), 100, TRUE, "0123456789", "2.jpg"),
  ('Adri', 'Laurent', 'Adriana',2,"1986-09-15", '$2y$10$fdPodEQIuiLowkOtvCD0IOckq4WgGJyWERCfGGO2llU4.ubUiFxzK',
              2, 'AdrianaLaurent@hotmail.bz', '13 boulevard de Rio', '06600', 'Nice', 1.18,
              "Jeune étudiante en Master 1 Sciences et cultures, je souhaite devenir institutrice, j'ai toujours aimé le contact avec les enfants.",
   DATE_SUB(NOW(), INTERVAL 2 MONTH), 100, TRUE, "0123456789", "2.jpg"),
  ('Jules', 'Baron', 'Jules',1,"1936-01-01", '$2y$10$MShUZluNKFgPzj6LaLnWgO5t2/rPURGp7kOxEvfbTJV7O6o2hxKIq',
                    2, 'jules.baron@mail.fr', '7 Rue Lamartine', '06660', 'Nice', 1,
                    "Je suis disponible les soirs et les week-end, je possède le BAFA avec plusieurs expériences en centre aéré", NOW(), 0, FALSE, "0123456789", "2.jpg");


INSERT INTO dispo_geo VALUES
  (1, "75000"),
  (1, "75111"),
  (1, "06000"),
  (2, "06000"),
  (2, "06144"),
  (11, "06150"),
  (12, "06400"),
  (12, "06150"),
  (13, "06370"),
  (14, "94310"),
  (15, "06000"),
  (16, "06000"),
  (17, "06000"),
  (18, "13000"),
  (19, "06220"),
  (20, "06000"),
  (21, "06000"),
  (22, "06000"),
  (23, "06000");

INSERT INTO dispo (babysitter, debut, fin, taux_horaire) VALUES
  (1, NOW(), DATE_ADD(NOW(), INTERVAL 6 MONTH), 15),
  (2, NOW(), DATE_ADD(NOW(), INTERVAL 6 MONTH), 8),
  (11, NOW(), DATE_ADD(NOW(), INTERVAL 6 MONTH), 8),
  (12, NOW(), DATE_ADD(NOW(), INTERVAL 6 MONTH), 10),
  (13, NOW(), DATE_ADD(NOW(), INTERVAL 6 MONTH), 12),
  (14, NOW(), DATE_ADD(NOW(), INTERVAL 6 MONTH), 11),
  (15, NOW(), DATE_ADD(NOW(), INTERVAL 6 MONTH), 7.5),
  (16, NOW(), DATE_ADD(NOW(), INTERVAL 6 MONTH), 9.25),
  (17, NOW(), DATE_ADD(NOW(), INTERVAL 6 MONTH), 8.85),
  (18, NOW(), DATE_ADD(NOW(), INTERVAL 6 MONTH), 10.35),
  (19, NOW(), DATE_ADD(NOW(), INTERVAL 6 MONTH), 9.35),
  (20, NOW(), DATE_ADD(NOW(), INTERVAL 6 MONTH), 7.25),
  (21, NOW(), DATE_ADD(NOW(), INTERVAL 6 MONTH), 3.25),
  (22, NOW(), DATE_ADD(NOW(), INTERVAL 6 MONTH), 9.80),
  (23, NOW(), DATE_ADD(NOW(), INTERVAL 6 MONTH), 20);

INSERT INTO parametres (set_by, commission, delai_annulation, delai_commentaire, delai_validation, prix_max) VALUES
  (3, 0.05, '24', '192', '48', '100');

# const DB_ATTENTE = 0;
# const DB_DEMANDEE = 1;
# const DB_ACCEPTEE = 2;
# const DB_VALIDEE = 3;
# const DB_INVALIDEE = 4;
# const DB_REFUSEE = 5;
# const DB_ANNULEE = 6;
# const DB_AVORTEE = 7;

INSERT INTO
  garde (famille, babysitter, date_demande, debut, fin, status, prix,
         commission, nb_enfants, lim_annulation, lim_comment, lim_invalid)
VALUES
  (
    4,
    1,
    DATE_ADD(NOW(), INTERVAL 7 DAY),
    DATE_ADD(NOW(), INTERVAL 8 * 24 - 2 HOUR),
    DATE_ADD(NOW(), INTERVAL 8 DAY),
    0, 250, 4, 2,
    DATE_ADD(NOW(), INTERVAL 8 DAY),
    DATE_ADD(NOW(), INTERVAL 8 DAY),
    DATE_ADD(NOW(), INTERVAL 8 DAY)
  ),
  (
    4,
    1,
    DATE_SUB(NOW(), INTERVAL 7 DAY),
    DATE_SUB(NOW(), INTERVAL 8 * 24 + 2 HOUR),
    DATE_SUB(NOW(), INTERVAL 8 DAY),
    1, 35, 4, 2,
    DATE_SUB(NOW(), INTERVAL 8 DAY),
    DATE_SUB(NOW(), INTERVAL 8 DAY),
    DATE_SUB(NOW(), INTERVAL 8 DAY)
  ),
  (
    4,
    1,
    DATE_SUB(NOW(), INTERVAL 3 DAY),
    DATE_SUB(NOW(), INTERVAL 24 + 2 HOUR),
    DATE_SUB(NOW(), INTERVAL 1 DAY),
    5, 20, 2.25, 2,
    DATE_SUB(NOW(), INTERVAL 1 DAY),
    DATE_SUB(NOW(), INTERVAL 1 DAY),
    DATE_SUB(NOW(), INTERVAL 1 DAY)
  ),
  (
    4,
    18,
    DATE_SUB(NOW(), INTERVAL 5 DAY),
    DATE_SUB(NOW(), INTERVAL 24 * 5 + 2 HOUR),
    DATE_SUB(NOW(), INTERVAL 5 DAY),
    4, 15, 3.25, 2,
    DATE_SUB(NOW(), INTERVAL 1 DAY),
    DATE_SUB(NOW(), INTERVAL 8 DAY),
    DATE_SUB(NOW(), INTERVAL 8 DAY)
  ),
  (
    4,
    1,
    DATE_SUB(NOW(), INTERVAL 6 DAY),
    DATE_SUB(NOW(), INTERVAL 24 * 6 + 2 HOUR),
    DATE_SUB(NOW(), INTERVAL 6 DAY),
    7, 17, 2.25, 2, DATE_SUB(NOW(), INTERVAL 1 DAY),
    DATE_SUB(NOW(), INTERVAL 8 DAY),
    DATE_SUB(NOW(), INTERVAL 8 DAY)
  ),
  (
    4,
    18,
    DATE_SUB(NOW(), INTERVAL 10 DAY),
    DATE_SUB(NOW(), INTERVAL 24 * 10 + 2 HOUR),
    DATE_SUB(NOW(), INTERVAL 5 DAY),
    3, 13, 0.75, 2,
    DATE_SUB(NOW(), INTERVAL 1 DAY),
    DATE_SUB(NOW(), INTERVAL 8 DAY),
    DATE_SUB(NOW(), INTERVAL 8 DAY)
  ),
  (
    4,
    1,
    NOW(),
    DATE_ADD(NOW(), INTERVAL 2 DAY),
    DATE_ADD(NOW(), INTERVAL 24 * 2 + 4 HOUR),
    1, 42, 5, 1,
    DATE_ADD(NOW(), INTERVAL 1 DAY),
    DATE_ADD(NOW(), INTERVAL 7 DAY),
    DATE_ADD(NOW(), INTERVAL 4 DAY)
  ),
  (
    4,
    16,
    NOW(),
    DATE_SUB(NOW(), INTERVAL 28 HOUR),
    DATE_SUB(NOW(), INTERVAL 24 HOUR),
    2, 42, 5, 1,
    DATE_SUB(NOW(), INTERVAL 28 HOUR),
    DATE_ADD(NOW(), INTERVAL 7 DAY),
    DATE_ADD(NOW(), INTERVAL 4 DAY)
  )
  ,
  (
    4,
    1,
    NOW(),
    DATE_SUB(NOW(), INTERVAL 72 HOUR),
    DATE_SUB(NOW(), INTERVAL 70 HOUR),
    2, 42, 5, 1,
    DATE_SUB(NOW(), INTERVAL 28 HOUR),
    DATE_ADD(NOW(), INTERVAL 7 DAY),
    DATE_ADD(NOW(), INTERVAL 4 DAY)
  );

INSERT INTO
  garde (famille, babysitter, date_demande, debut, fin, status, prix,
         commission, nb_enfants, lim_annulation, lim_comment, lim_invalid,
         comment_bs, comment_f, note_f, note_bs)
VALUES (
  4,
  1,
  DATE_SUB(NOW(), INTERVAL 10 DAY),
  DATE_SUB(NOW(), INTERVAL 24 * 10 + 2 HOUR),
  DATE_SUB(NOW(), INTERVAL 5 DAY),
  3, 13, 0.75, 2,
  DATE_SUB(NOW(), INTERVAL 1 DAY),
  DATE_SUB(NOW(), INTERVAL 8 DAY),
  DATE_SUB(NOW(), INTERVAL 8 DAY),
  "Enfant mal élevé et irrespectueux, une des pires expériences de ma vie.",
  "Très sérieux, merci à vous.",
  5,
  2
),
  (
    4,
    1,
    DATE_SUB(NOW(), INTERVAL 20 DAY),
    DATE_SUB(NOW(), INTERVAL 24 * 20 + 2 HOUR),
    DATE_SUB(NOW(), INTERVAL 20 DAY),
    3, 13, 0.75, 2,
    DATE_SUB(NOW(), INTERVAL 20 DAY),
    DATE_SUB(NOW(), INTERVAL 20 DAY),
    DATE_SUB(NOW(), INTERVAL 20 DAY),
    "Famille très accueillante et chaleureuse, je les recommande.",
    "Il a vidé les placards, mais il s'est très bien occupé des enfants.",
    4,
    5
  )
;