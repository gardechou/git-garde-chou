<?php
/**
 * Définition de la classe CronJobs pour les actions à executer régulièrement sur le serveur
 *
 * @package Services
 */

$path = realpath(dirname(__FILE__));
include $path . "/../config.php";
include $path . "/../service/Database.php";
include $path . "/../service/SoldeService.php";

/**
 * Les différentes actions qui ont besoin d'être effectuées régulièrement
 */
class CronJobs
{
    /**
     * Effacer les disponibilités dont la date de fin est dépassée
     */
    public static function cleanDispos()
    {
        // Nettoyage des disponibilités...;
        $query = Database::getPdo()->exec("DELETE FROM dispo WHERE fin < NOW()");
    }

    /**
     * Automatiquement valider les gardes qui ne peuvent plus être validées "manuellement"
     */
    public static function autoValidateGarde()
    {
        // Auto-validation des gardes...;
        $query = Database::getPdo()->exec("UPDATE garde SET status = 3 WHERE status = 2 AND lim_invalid < NOW()");
    }

    /**
     * Recréditer l'argent des gardes ignorées aux familles
     * À exécuter toutes les 24 heures.
     */
    public static function recreditIgnoredGardes()
    {
        $query = Database::getPdo()->prepare(
            "SELECT prix, commission, famille FROM garde
             WHERE status IN (0,1) AND DATE_ADD(NOW(), INTERVAL 1 DAY) < debut < NOW()");
        $query->execute();
        $data = $query->fetchAll();
        foreach ($data as $row) {
            SoldeService::modify($row["prix"] + $row["commission"], $row["famille"]);
        }
    }

    /**
     * Envoyer un mail de rappel aux clients x minutes avant le début d'une garde acceptée.
     */
    // TODO: gérer (tester?) l'envoi de mails pour rappels de gardes
    public static function rappelGarde()
    {
        $rappel_subject = "Garde-chou: garde imminente";
        $rappel_body = "Vous avez une garde qui commence dans 1 heure";
        // Envoi des mails de rappels de garde
        $query = Database::getPdo()->prepare(
            "SELECT
               babysitter.*,
               famille.*,
               garde.*
             FROM
               garde
               JOIN user babysitter ON garde.babysitter = babysitter.id 
               JOIN user famille ON garde.famille = famille.id 
             WHERE garde.debut BETWEEN DATE_SUB(NOW(), INTERVAL 60 MINUTE) AND DATE_SUB(NOW(), INTERVAL 70 MINUTE)"
        );
        $query->execute();
        $data = $query->fetchAll();

        foreach ($data as $row) {
            if (mail($row['babysitter.email'], $rappel_subject, $rappel_body)) {
                echo "Mail envoyé.";
            } else {
                echo "Problème dans l'envoi du mail";
            }
            if (mail($row['famille.email'], $rappel_subject, $rappel_body)) {
                echo "Mail envoyé.";
            } else {
                echo "Problème dans l'envoi du mail à " . $row["babysitter.email"];
            }
        }
    }
}