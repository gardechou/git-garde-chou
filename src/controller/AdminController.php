<?php
/**
 * Définition de la classe AdminController
 *
 * @package Contrôleurs
 * @tag Administrateur
 */

/**
 * Les actions des administrateurs.
 *
 * Grâce à l'utilisation de l'autoloader, il n'est pas nécessaire de vérifier que l'on est connecté en
 * tant qu'admin pour chacune de ces actions.
 */
class AdminController
{
    /**
     * Page d'accueil des administrateurs
     */
    public static function homeAction()
    {
        Page::addToTitle("Bienvenue " . Session::getCurrentUserPseudo() . " !");

        // Préparer les gardes à autoriser
        $gardes = Garde::genAttenteAdmin();
        if (!empty($gardes)) {
            Page::add("gardes_title", "Gardes à autoriser");
            Page::add("gardes", $gardes);
            Page::addView("gardes.php");
        }

        // Préparer les instances babysitters invalides pour êtres affichés
        $users = Babysitter::genInvalides();
        if (!empty($users)) {
            Page::add("users", $users);
            Page::add("users_title", "Utilisateurs à valider");
            Page::addView("admin/users.php");
        }

        // Préparer les derniers commentaires
        $comments_per_page = 10;
        $comments = Comment::genAll($comments_per_page, 1, "date");
        if (!empty($comments)) {
            Page::add("comments", $comments);
            Page::addView("admin/comments.php");
        }

        // Pour que les views multipage fonctionnent sans peine
        Page::add("page", 1);
        Page::add("nextpage", false);
    }

    /**
     * Modification des paramètres du site (commissions, délais, prix maximum sans validation admin)
     */
    public static function parametresAction()
    {
        $parametres = Settings::getCurrent();
        if (!empty($_POST)) {
            $form = new FormChecker($_POST);
            $form->addField("commission", "commission du site", "/[0-9]*\.?[0-9]*/");
            $form->addField("annulation", "délai d'annulation d'une garde", "/\d*/");
            $form->addField("commentaire", "délai pour laisser un commentaire", "/\d*/");
            $form->addField("prix_max", "délai pour laisser un commentaire", "/\d*/");
            $form->addField("validation", "délai pour laisser un commentaire", "/\d*/");
            if ($form->parse()) {
                $new_settings = $form->getStructuredArray();
                if ($parametres == $new_settings) {
                    Page::addError("Vous n'avez rien changé&nbsp;!");
                } else {
                    Settings::set(
                        $_POST['commission'] / 100,
                        $_POST['prix_max'],
                        $_POST['annulation'],
                        $_POST['commentaire'],
                        $_POST['validation']);
                    Page::addInfo("Les paramètres ont bien été modifiés.");
                    $parametres = $new_settings;
                    Page::goBack();
                }
            }
        }
        Page::addArray($parametres);
        Page::setNavbar("settings");
        Page::addView("admin/parametres.php");
    }

    /**
     * Liste des utilisateurs inscrits.
     */
    public static function usersAction()
    {
        if (empty($_POST)) {
            $nb_per_page = 10;
            $page = (isset($_GET["page"])) ? $_GET["page"] : 1;
            $sort_by = "date_creation";
            $users = User::genAll($nb_per_page, $page, $sort_by);
            Page::add("users", array_slice($users, 0, $nb_per_page));
            Page::add("page", $page);
            Page::add("nextpage", !empty(User::genAll($nb_per_page, $page + 1, $sort_by)));
            Page::setNavbar("users");
            Page::add("users_title", "Liste des utilisateurs");
            Page::addView("admin/users.php");
        }
    }

    /**
     * Statistiques pour l'admin
     */
    public static function statsAction()
    {
        $n_users = UserService::count();
        $colors = [];
        // on prépare les datasets pour chartjs
        foreach ($n_users as $type => $n) {
            switch ($type) {
                case "admins suspendus":
                    $colors[] = "#rgba(105,76,234,0.5)";
                    break;
                case "admins":
                    $colors[] = "rgba(105,76,234,1)";
                    break;
                case "familles suspendues":
                    $colors[] = "rgba(223,179,72,0.5)";
                    break;
                case "familles":
                    $colors[] = "rgba(223,179,72,1)";
                    break;
                case "baby-sitters suspendus":
                    $colors[] = "rgba(201,47,148,0.5)";
                    break;
                case "baby-sitters":
                    $colors[] = "rgba(201,47,148,1)";
                    break;
            }
        }
        Page::add("colors_types", json_encode($colors));
        Page::add("labels_types", json_encode(array_keys($n_users)));
        Page::add("data_types", json_encode(array_values($n_users)));

        $benefs = GardeService::getBenefs();

        $users_by_month = UserService::countByMonth();
        list($dates, $babysitters, $familles, $admins) = $users_by_month;

//        $dates = array_unique($dates);

        $babysitters_data = [];
        $familles_data = [];
        $admin_data = [];
        $benefs_data = [];

        foreach ($dates as $date) {
            $babysitters_data[] = (isset($babysitters[$date])) ? (int)$babysitters[$date] : 0;
            $familles_data[] = (isset($familles[$date])) ? (int)$familles[$date] : 0;
            $admin_data[] = (isset($admins[$date])) ? (int)$admins[$date] : 0;
            $benefs_data[] = (isset($benefs[$date])) ? (float)$benefs[$date] : 0;
        }

        $babysitters_data = self::array_cumulative($babysitters_data);
        $familles_data = self::array_cumulative($familles_data);
        $admin_data = self::array_cumulative($admin_data);
        $benefs_data = self::array_cumulative($benefs_data);

        $dates_data = [
            [
                "label" => "baby-sitters",
                "data" => $babysitters_data,
                "backgroundColor" => "rgba(201,47,148,1)",
                "fill" => false,
                "borderColor" => "rgba(201,47,148,1)"
            ],
            [
                "label" => "familles",
                "data" => $familles_data,
                "backgroundColor" => "rgba(223,179,72,1)",
                "borderColor" => "rgba(223,179,72,1)",
                "fill" => false
            ],
            [
                "label" => "administrateurs",
                "data" => $admin_data,
                "backgroundColor" => "rgba(105,76,234,1)",
                "borderColor" => "rgba(105,76,234,1)",
                "fill" => false
            ],
            [
                "label" => "chiffre d'affaire",
                "data" => $benefs_data,
                "backgroundColor" => "rgba(255,0,0,1)",
                "borderColor" => "rgba(255,0,0,1)",
                "fill" => false,
                "yAxisID" => "y-axis-2"
            ]
        ];

        Page::add("dates", json_encode($dates));
        Page::add("dates_data", json_encode($dates_data));
        Page::setNavbar("stats");
        Page::addView("admin/stats.php");
    }

    /**
     * Afficher toutes les gardes
     */
    public static function gardesAction()
    {
        $nb_per_page = 10;
        $page = (isset($_GET["page"])) ? $_GET["page"] : 1;
        $gardes = Garde::genAll($nb_per_page, $page);
        Page::add("gardes_title", "Toutes les gardes");
        Page::add("gardes", $gardes);
        Page::add("page", $page);
        Page::add("nextpage", !empty(Garde::genAll($nb_per_page, $page + 1)));
        Page::add("nextpage_uri", "/admin/gardes");
        Page::setNavbar("garde");
        Page::addView("gardes.php");
    }

    /**
     * Afficher tous les commentaires
     */
    public static function commentsAction()
    {
        $comments_per_page = 10;
        $page = (isset($_GET["page"])) ? $_GET["page"] : 1;
        $comments = Comment::genAll($comments_per_page, $page, "date");
        if (empty($comments) and $page > 1) {
            Page::go("/admin/comments");
        }
        Page::add("comments", array_slice($comments, 0, $comments_per_page));
        Page::add("page", $page);
        Page::add("nextpage", !empty(Comment::genAll($comments_per_page, $page + 1, "date")));
        Page::setNavbar("comment");
        Page::addView("admin/comments.php");
    }

    /**
     * Renvoie un array, du même type et de la même taille que celui en entrée,
     * et composé de la somme des n - i cases à la case n (utilisé pour les statistiques
     * mensuelles sur les inscriptions.
     *
     * @param array $array
     * @return array
     */
    private static function array_cumulative($array)
    {
        $n = 0;
        $res = [];
        foreach ($array as $i => $value) {
            $n += $value;
            $res[$i] = $n;
        }
        return $res;
    }

    /**
     * Autoriser une garde dont le montant dépasse celui défini par l'administrateur
     *
     * L'id de la garde est passé par GET
     */
    public static function autorisergardeAction()
    {
        if (!isset($_GET["id"])) {
            Page::goHome();
        }

        $id = $_GET["id"];

        $garde = Garde::genFromId($id);

        if ($garde->isAutorisable()) {
            GardeService::autoriserById($id);
        } else {
            Page::addError("Vous ne pouvez plus autoriser cette garde.");
        }

        Page::goBack();
    }

    /**
     * Interdire une garde dont le montant dépasse celui défini par l'administrateur
     *
     * L'id de la garde est passé par GET
     */
    public static function interdiregardeAction()
    {
        if (!isset($_GET["id"])) {
            Page::goHome();
        }

        $id = $_GET["id"];

        $garde = Garde::genFromId($id);

        if ($garde->isAutorisable()) {
            GardeService::modifStatus($id, Garde::DB_INTERDITE);
            SoldeService::modify($garde->getTotalPrice(), $garde->getFamilleId());
        } else {
            Page::addError("Vous ne pouvez plus interdire cette garde.");
        }

        Page::goBack();
    }

    /**
     * Supprimer le commentaire d'une garde.
     */
    public static function suppcommAction()
    {
        CommentService::updateCommentFromGardeIdAndUserType($_GET["garde_id"], null, $_GET["type_client"]);
        Page::goBack();
    }

    /**
     * Editer le commentaire d'une garde.
     */
    public static function editercommAction()
    {
        $garde_id = $_GET['garde_id'];
        $type_client = $_GET['type_client'];
        $garde = Garde::genFromId($garde_id);
        $garde->setPseudos();
        $pseudo = ($type_client == User::TYPE_BABYSITTER) ? $garde->getBabysitterPseudo() : $garde->getFamillePseudo() . ".";
        Page::add("type_client", $type_client);
        Page::add("control", "admin");
        Page::add("action", "validerediter");
        Page::add("garde", $garde);
        Page::add("note", ($type_client == User::TYPE_BABYSITTER) ? $garde->getNoteBabysitter() : $garde->getNoteFamille());
        Page::add("legende", "Éditer le commentaire et/ou la note soumise par " . $pseudo);
        Page::add("help_comment", "Modifier le commentaire.");
        Page::add("contenu", ($type_client == User::TYPE_BABYSITTER) ? $garde->getCommentaireBabysitter() : $garde->getCommentaireFamille());
        Page::setNavbar("comment");
        Page::addView("comment_form.php");
    }

    /**
     * Valider la modification du commentaire d'une garde.
     */
    public static function validerediterAction()
    {
        $garde_id = $_POST['garde_id'];
        $type_client = $_POST['type_client'];
        $new_comment = $_POST['commentaire'];
        $new_note = $_POST['note'];
        CommentService::updateCommentAndNoteFromGardeIdAndUserType($garde_id, $new_comment, $new_note, $type_client);
        Page::go("/admin/comments");
    }
}