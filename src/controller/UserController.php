<?php
/**
 * Définition de la classe UserController
 *
 * @package Contrôleurs
 * @tag Famille
 * @tag Administrateur
 * @tag Visiteur
 * @tag Baby-sitter
 */

/** Les actions concernant tous les utilisateurs. */
class UserController
{
    /**
     * Page d'accueil des utilisateurs connectés.
     *
     * Redirige vers l'accueil spécifique selon le type de compte, et vers /default/home si pas connecté.
     */
    public static function homeAction()
    {
        if (Session::isLoggedIn()) {
            // Redirige vers l'accueil spécifique selon le type de compte
            $controllerName = ucfirst(Session::getType()) . "Controller";
            $controllerName::homeAction();
        } else {
            // Si pas loggé, home par défaut.
            DefaultController::homeAction();
        }
    }

    /**
     * Action de se connecter.
     *
     * Reçoit pseudo et password par POST.
     */
    public static function loginAction()
    {
        if (Session::isLoggedIn()) {
            // Pas de login si déjà connecté
            self::homeAction();
        }

        // TODO: Utiliser FormChecker ici
        // On vérifie que les arguments ont bien été passés et qu'ils sont valides
        if (isset($_POST['pseudo']) and isset($_POST['password']) and
            UserService::authenticate($_POST['pseudo'], $_POST['password'])
        ) {
            Session::setCurrentUserFromPseudo($_POST['pseudo']);
            Page::addInfo("Vous êtes maintenant connecté.");
            Page::goHome();
        } else {
            Page::addError("Mauvaises informations de connexion.");
            DefaultController::homeAction();
        }
    }

    /** Déconnecte l'utilisateur actuellement connecté. */
    public static function logoutAction()
    {
        Session::destroy();
        Page::addInfo("Vous êtes maintenant déconnecté de garde-chou.");
        DefaultController::homeAction();
    }

    // TODO: utiliser POST ici
    // TODO: passer ça dans AdminController

    /**
     * En tant qu'administrateur, valider un utilisateur.
     */
    public static function validerAction()
    {
        if (Session::isAdmin() and isset($_GET["id"])) {
            UserService::validate($_GET['id']);
            Page::addInfo("L'utilisateur a bien été validé.");
        }
        Page::goBack();
    }

    // TODO: passer ça dans AdminController

    /**
     * En tant qu'administrateur, invalider un utilisateur.
     */

    public static function invaliderAction()
    {
        if (Session::isAdmin() and isset($_GET["id"])) {
            UserService::invalidate($_GET['id']);
            Page::addInfo("L'utilisateur a bien été invalidé.");
        }
        Page::goBack();
    }

    /**
     * Voir le profil d'un utilisateur
     */
    public static function profileAction()
    {
        if (!Session::isLoggedIn()) {
            Page::goHome();
        }

        $id = (isset($_GET['id'])) ? $_GET['id'] : $_SESSION['id'];
        $user = User::genFromId($id);
        if ($user->getType() == User::TYPE_ADMIN and !Session::isAdmin()) {
            Page::goHome();
        }

        Page::add("user", $user);
        // Cas dans lesquels l'utilisateur loggé peut voir les commentaires
        Page::add("view_comments",
            (
                !$user->getType() == User::TYPE_ADMIN and Session::isAdmin() or (
                    $user->getType() == User::TYPE_BABYSITTER or
                    $user->getType() == User::TYPE_FAMILLE and Session::isBabysitter()
                )
            )
        );

        Page::addToTitle("Profil de " . $user->getPseudo());
        Page::addView("profile.php");
    }

    /**
     * Modifier les infos de profils utilisateur
     */
    public static function modifAction()
    {
        if (!Session::isLoggedIn()) {
            Page::goHome();
        }


        if (!empty($_POST)) {
            $form = new FormChecker($_POST);
            $form->addField("password", "mot de passe", "/((?=.*\d)(?=.*[a-z]).)/", $min = 6, $max = 20,
                $errormsg = "Le mot de passe doit être composé d'au moins un chiffre et d'au moins une lettre",
                $pre_fill = false, $optional = true);
            $form->addField("confirm_password", "confirmation du mot de passe", "confirm", $min = 6, $max = 20,
                $errormsg = "Veuillez taper deux fois le même mot de passe.", $pre_fill = false, $optional = true);
            $form->addField("telephone", "telephone", "/^\d*$/",
                $min = 10,
                $max = 10,
                $errormsg = "Veuillez taper un numéro de téléphone à 10 chiffres",
                $pre_fill = true);
            $form->addField("email", "courriel", "email", $min = 5, $max = 50);
            $form->addField("nom", "nom", null, $min = 2, $max = 20);
            $form->addField("prenom", "prénom", null, $min = 2, $max = 20);
            $form->addField("adresse", "adresse", null, $min = 5, $max = 100);
            $form->addField("code_postal", "code postal", "/^\d{5}$/", $min = 5, $max = 5);
            $form->addField("ville", "ville", null, $min = 1, $max = 50);
            $form->addField("presentation", "présentation", null, $min = 0, $max = 500, null, $pre_fill = true,
                $optional = true);
            if (Session::isBabysitter()) {
                $form->addField("multiplicateur", "Multiplicateur", "float");
                $form->addField("genre_humain", "Genre (Sexe)", "/1|2/");
                $form->addField("date_naissance", "Date de naissance", "date", $min = 10, $max = 10, $pre_fill = true,
                    $errormsg =
                        "Veuillez indiquer votre date de naissance au format AAAA-MM-JJ");
            }
            if (!$form->parse()) {
                Page::addErrors($form->getErrors());
            } else {
                extract($form->getStructuredArray());
                $password = (empty($password)) ? null : $password;
                if (Session::isBabysitter()) {
                    UserService::modify(Session::getCurrentUserId(), Session::getIntType(), $nom, $prenom, $email, $adresse,
                        $code_postal, $ville, $presentation, $telephone, $password, $multiplicateur, $date_naissance, $genre_humain);
                } else if (Session::isFamille()) {
                    UserService::modify(Session::getCurrentUserId(), Session::getIntType(), $nom, $prenom, $email, $adresse,
                        $code_postal, $ville, $presentation, $telephone, $password);
                }
                Page::addInfo("Modifications enregistrées");
            }
        }
        $user = User::genFromId(Session::getCurrentUserId());
        Page::add("user", $user);
        Page::addView("modif_profil.php");
    }
}
