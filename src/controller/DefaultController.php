<?php
/**
 * Définition de la classe DefaultController
 *
 * @package Contrôleurs
 * @tag Visiteur
 */

/** Ce contrôleur gère les actions du visiteur. */
class DefaultController
{
    /** Accueil du site pour utilisateur non connecté. */
    public static function homeAction()
    {
        // Rediriger vers l'accueil "utilisateur enregistré" si utilisateur connecté.
        if (Session::isLoggedIn()) {
            UserController::homeAction();
        } else {
            Page::addToTitle("Bienvenue !");
            Page::addView("home.php");
        }
    }

    /** Lister les babysitters travaillant dans un code postal donné par HTTP GET. */
    public static function listbabysittersAction()
    {
        if (Session::isLoggedIn()) {
            UserController::homeAction();
        } else {
            if (isset($_GET['code_postal'])) {
                // vérifier que le code postal est bien de la forme 5 chiffres et rien d'autre
                if (preg_match("/^\d{5}$/", $_GET['code_postal'])) {
                    $babysitters = Babysitter::genFromDispoGeo($_GET['code_postal']);
                    Page::add("babysitters", $babysitters);
                    Page::add("code_postal", $_GET['code_postal']);
                    Page::addView("list_babysitters.php");
                } else {
                    Page::addError("Le code postal " . $_GET['code_postal'] . " n'est pas valide");
                    Page::goBack();
                }
            } else {
                Page::goBack();
            }
        }
    }

    /**
     * S'inscrire en tant que famille ou babysitter.
     *
     * Un type d'inscription et/ou un code postal peuvent êtres passés par HTTP GET et ainsi être pré-remplis.
     *
     * Les informations d'inscription sont passées par HTTP POST.
     * Leur contenu est vérifié avant d'effectivement ajouter l'utilisateur à la base de données.
     *
     * @see ../view.inscription.html ../view/inscription.php
     * @see ../service.UserService.html UserService::addClient()
     */
    public static function subscribeAction()
    {
        // Pas d'inscription si déjà connecté
        if (Session::isLoggedIn()) {
            Page::goHome();
        }

        // Si des données arrivent par POST, on essaye d'inscrire
        if (!empty($_POST) and isset($_POST["type"])) {
            $valid_values = false;
            // Vérification de la conformité des champs.
            $form = new FormChecker($_POST);
            $form->addField("type", "Type d'inscription", "/babysitter|famille/");
            $form->addField("pseudonyme", "pseudonyme", "/[A-Za-z][A-Za-z0-9._]/", $min = 5, $max = 16,
                $errormsg =
                    "Le pseudonyme doit être composé de chiffres, de lettres non accentuées uniquement, de . et de _");
            $form->addField("password", "mot de passe", "/((?=.*\d)(?=.*[a-z]).)/", $min = 6, $max = 20,
                $errormsg = "Le mot de passe doit être composé d'au moins un chiffre et d'au moins une lettre",
                $pre_fill = false);
            $form->addField("confirm_password", "confirmation du mot de passe", "confirm", $min = 6, $max = 20,
                $errormsg = "Veuillez taper deux fois le même mot de passe.", $pre_fill = false);
            $form->addField("telephone", "telephone", "/^\d*$/",
                $min = 10,
                $max = 10,
                $errormsg = "Veuillez taper un numéro de téléphone à 10 chiffres",
                $pre_fill = true);
            $form->addField("email", "courriel", "email", $min = 5, $max = 50);
            $form->addField("nom", "nom", null, $min = 2, $max = 20);
            $form->addField("prenom", "prénom", null, $min = 2, $max = 20);
            $form->addField("adresse", "adresse", null, $min = 5, $max = 100);
            $form->addField("code_postal", "code postal", "/^\d{5}$/", $min = 5, $max = 5);
            $form->addField("ville", "ville", null, $min = 1, $max = 50);
            $form->addField("presentation", "présentation", null, $min = 0, $max = 500, null, $pre_fill = true,
                $optional = true);
            if ($_POST["type"] == "babysitter") {
                $form->addField("multiplicateur", "Multiplicateur", "float");
                $form->addField("genre_humain", "Genre (Sexe)", "/1|2/");
                $form->addField("date_naissance", "Date de naissance", "date", $min = 10, $max = 10, $pre_fill = true,
                    $errormsg =
                        "Veuillez indiquer votre date de naissance au format AAAA-MM-JJ");
            }
            if ($form->parse()) {
                // Vérification que pseudo et email ne sont pas déjà utilisés
                // TODO: interdire les duplicats de pseudo case-insensitive
                if (UserService::isPseudoAvailable($_POST['pseudonyme'])) {
                    if (UserService::isEmailAvailable($_POST['email'])) {
                        $valid_values = true;
                    } else {
                        Page::addError(
                            "L'email " . $_POST['email'] . " est déjà utilisé. Seriez-vous déjà inscrit&nbsp;?");
                        Page::addArray($form->getStructuredArray());
                        Page::add("email", "");
                    }
                } else {
                    Page::addError("Le pseudo " . $_POST['pseudonyme'] . " est déjà utilisé");
                    Page::addArray($form->getStructuredArray());
                    Page::add("pseudonyme", "");
                }
            } else { // Si les champs ne sont pas du tous "conformes", on ajoute les msg d'erreurs de FormChecker
                Page::addErrors($form->getErrors());
                Page::addArray($form->getStructuredArray());
            }
            if ($valid_values) {
                // Compte famille valide dès inscription, mais pas baby-sitter
                $valid = ($_POST['type'] == "famille") ? true : false;
                $solde = 0;
                if ($_FILES["profile_picture"]["size"] != 0) {
                    $pic = Upload::picture("profile_picture");
                } else {
                    $pic = "default.png";
                }
                UserService::addClient($_POST["pseudonyme"], $_POST["nom"], $_POST["prenom"], $_POST["genre_humain"], $_POST["date_naissance"], $_POST["password"],
                    $_POST["type"], $_POST["telephone"], $_POST["email"], $_POST["adresse"], $_POST["code_postal"], $_POST["ville"],
                    $_POST["presentation"], $solde, $valid, $pic);
                // TODO: utiliser une autre méthode de Session pour relire ce qu'on vient d'ajouter dans la BDD
                // plutôt que de refaire une requête SQL
                Session::setCurrentUserFromPseudo($_POST["pseudonyme"]);
                Page::addInfo("Votre compte a bien été créé!");
                Page::goHome();
            }
        } else { // Possibilité de passer des champs pré-remplis par GET, si pas de données passées par POST
            foreach (array("type", "code_postal") as $field) {
                if (isset($_GET[$field])) {
                    Page::add($field, $_GET[$field]);
                }
            }
        }

        $fields = [
            "pseudonyme", "telephone", "email", "nom", "prenom", "adresse", "code_postal",
            "ville", "presentation", "multiplicateur", "genre_humain", "date_naissance"
        ];

        foreach ($fields as $field)
        {
            if (!Page::isDefined($field)) {
                Page::add($field, "");
            }
        }

        if (!Page::isDefined("type"))
        {
            Page::add("type", "famille");
        }

        Page::setNavbar("subscribe");
        Page::addToTitle("Inscription");
        Page::addView("inscription.php");
    }
}