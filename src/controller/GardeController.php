<?php
/**
 * Définition de GardeController
 *
 * @tag Famille, Baby-sitter
 * @package Contrôleurs
 */

/**
 * Action sur les gardes. Réservées aux familles et baby-sitters via l'autoloader.
 */
class GardeController
{
    /**
     * Lister toutes les gardes de l'utilisateur connecté
     */
    public static function listerAction()
    {
        $nb_per_page = 5;
        $page = (isset($_GET["page"])) ? $_GET["page"] : 1;
        $gardes = Garde::genForActor($_SESSION["id"], $nb_per_page, $page);
        Page::add("gardes", array_slice($gardes, 0, $nb_per_page));
        Page::add("page", $page);
        Page::add("nextpage", (!empty(Garde::genForActor($_SESSION["id"], $nb_per_page, $page + 1))));
        Page::setNavbar("gardes");
        Page::add("gardes_title", "Toutes vos gardes");
        Page::add("nextpage_uri", "/garde/lister");
        Page::addView("gardes.php");
    }


    /**
     * Annuler une garde
     */
    public static function annulerAction()
    {
        $garde_id = $_GET['id'];
        $garde = Garde::genFromId($garde_id);
        if ($garde->isAnnulableBy($_SESSION['id'])) {
            GardeService::annulerById($garde_id, $_SESSION['id']);
            SoldeService::modify($garde->getTotalPrice(), $garde->getFamilleId());
            // TODO: Être plus verbeux sur le message d'annulation.
            Page::addInfo("La garde a bien été annulée.");
        } else {
            Page::addError("Vous n'avez pas l'autorisation d'annuler cette garde.");
        }
        Page::goBack();
    }

    /**
     * Refuser une garde
     */ //TODO: passer les actions réservées au babysitter dans babysittercontroller
    public static function refuserAction()
    {
        $garde_id = $_GET['id'];
        $garde = Garde::genFromId($garde_id);
        if ($garde->isRefusableBy($_SESSION['id'])) {
            GardeService::refuserById($garde_id);
            SoldeService::modify($garde->getTotalPrice(), $garde->getFamilleId());
            Page::addInfo("La garde " . $garde->summary() . " a bien été refusée.");
        } else {
            Page::addError("Vous n'avez pas l'autorisation de refuser cette garde.");
        }
        Page::goBack();
    }

    public static function avorterAction()
    {
        $garde_id = $_GET['id'];
        $garde = Garde::genFromId($garde_id);
        if ($garde->isAvortableBy($_SESSION['id'])) {
            GardeService::avorterById($garde_id);
            SoldeService::modify($garde->getTotalPrice(), $garde->getFamilleId());
            Page::addInfo("La garde " . $garde->summary() . " a bien été avortée.");
        } else {
            Page::addError("Vous n'avez pas l'autorisation d'avorter cette garde.");
        }
        Page::goBack();
    }

    /**
     * Accepter une garde
     */
    public static function accepterAction()
    {
        $garde_id = $_GET['id'];
        $garde = Garde::genFromId($garde_id);
        if ($garde->isAcceptableBy($_SESSION['id'])) {
            GardeService::accepterById($garde_id);
            DispoService::cutDispo($_SESSION['id'], $garde->getDebut(), $garde->getFin());
            $gardes = Garde::genActiveFromBabysitterId($_SESSION['id']);
            foreach ($gardes as $other_garde) {
                if ($other_garde->getId() == $garde->getId()) {
                    continue;
                }
                if (Dispo::overlap(
                    $garde->getDebut(), $garde->getFin(),
                    $other_garde->getDebut(), $other_garde->getFin())) {
                    GardeService::refuserById($other_garde->getId());
                }
            }
            // TODO: Être plus verbeux sur le message
            Page::addInfo("La garde a bien été acceptée.");
        } else {
            Page::addError("Vous n'avez pas l'autorisation d'accepter cette garde.");
        }
        Page::goBack();
    }

    /**
     * Valider une garde
     */
    public static function validerAction()
    {
        isset($_GET['id']) ? $garde_id = $_GET['id'] : Page::goBack();
        $garde = Garde::genFromId($garde_id);
        if ($garde->isValidableBy(Session::getCurrentUserId())) {
            GardeService::validerById($garde_id);
            SoldeService::modify($garde->getPrix(), $garde->getBabysitterId());
            // TODO: Être plus verbeux sur le message
            Page::addInfo("La garde " . $garde->summary() . " a bien été validée.");
            Page::go("/garde/commenter?id=" . $garde_id);
        } else {
            Page::addError("Vous n'avez pas l'autorisation de valider cette garde.");
            Page::goBack();
        }
    }

    /**
     * Invalider une garde
     */
    public static function invaliderAction()
    {
        $garde_id = $_GET['id'];
        $garde = Garde::genFromId($garde_id);
        if ($garde->isValidableBy($_SESSION['id'])) {
            GardeService::invaliderById($garde_id);
            // TODO: Être plus verbeux sur le message
            SoldeService::modify($garde->getPrix(), Session::getCurrentUserId());
            Page::addInfo("La garde a bien été invalidée.");
        } else {
            Page::addError("Vous n'avez pas l'autorisation d'invalider cette garde.");
        }
        Page::goBack();
    }

    /**
     * Formulaire pour ajouter un commentaire sur une garde.
     */
    public static function commenterAction()
    {
        $garde_id = $_GET['id'];
        $garde = Garde::genFromId($garde_id);
        $garde->setPseudos();

        if ($garde->isCommentableBy($_SESSION["id"])) {
            Page::add("type_client", ""); // pour que la vue marche aussi bien pour les clients que les admins...
            Page::add("control", "commentaires");
            Page::add("action", "ajouter");
            $note = (Session::isBabysitter()) ? $garde->getNoteBabysitter() : $garde->getNoteFamille();
            Page::add("note", is_null($note) ? 3 : $note);
            Page::add("help_comment","Écrivez ce que vous avez pensé de la garde.");
            Page::add("garde", $garde);
            Page::add("legende", "Laisser un commentaire sur la garde " . $garde->summary());
            Page::add("contenu", $garde->getCurUserComment());
            Page::addView("comment_form.php");
        } else {
            Page::addError("Vous ne pouvez pas commenter cette garde.");
            Page::goBack();
        }
    }
}