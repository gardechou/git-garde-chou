<?php
/**
 * Définition de la classe FamilleController
 *
 * @package Contrôleurs
 * @tag Famille
 */

/**
 * Le contrôleur correspondant aux actions des utilisateurs enregistrés en tant que Famille.
 *
 * Grâce à l'utilisation de l'autoloader, il n'est pas nécessaire de vérifier que l'on est connecté en
 * tant que famille pour chacune de ces actions.
 */
class FamilleController
{
    /** Page d'accueil des familles. */
    public static function homeAction()
    {
        Page::addToTitle("Bienvenue " . Session::getCurrentUserPseudo() . " !");
        // Préparation du mini-formulaire "Trouver un baby-sitter"
        $default_date = new DateTime('now');
        $default_date->modify('+3 day');
        Page::add("date_debut", $default_date->format("Y-m-d"));
        Page::add("heure_debut", $default_date->format("H:i"));
        Page::add("duree", "03:00");
        Page::add("nb_enfants", 1);
        Page::addView("famille/trouver_un_babysitter.php");
        // Préparation de la liste des gardes actives
        Page::add("gardes_title", "Vos gardes actives");
        Page::add("gardes", Garde::genActiveFromFamilleId($_SESSION['id']));
        Page::add("nextpage", false);
        Page::add("page", 1);
        Page::addView("gardes.php");
    }

    /** Envoyer une demande à un babysitter. */
    public static function envoyerdemandeAction()
    {
        // On passe l'id du babysitter par get et il est indispensable
        if (empty($_POST) or !isset($_GET["babysitter"]) or !preg_match("/\d*/", $_GET["babysitter"])) {
            Page::goBack();
        }

        $form = new FormChecker($_POST);
        $form->addField("prix", "prix", "float");
        $form->addField("commission", "commission", "float");
        $form->addField("debut", "durée", "/\d*/");
        $form->addField("fin", "nombre d'enfants", "/\d*/");
        $form->addField("date_demande", "date de demande", "/\d*/");
        $form->addField("nb_enfants", "nombre d'enfants", "/\d*/");
        $form->addField("limit_annulation", "limite d'annulation", "/\d*/");
        $form->addField("limit_comment", "limite de commentaire", "/\d*/");
        $form->addField("limit_invalidation", "limite d'invalidation", "/\d*/");
        $form->addField("autovalid", "prix inférieur à prix_max?", "/\d*/");
        $parse_success = $form->parse();

        if (!$parse_success) {
            Page::addError("Problème avec les valeurs passées par POST!");
            Page::goBack();
        }

        $values = $form->getStructuredArray();
        $total = $values['prix'] + $values['commission'];

        if (!($total <= $_SESSION['solde'])) {
            Page::addError("Vous n'avez pas assez d'argent sur votre compte pour demander cette garde.");
            Page::goBack();
        }

        $debut = Database::timestampToSQLString($values['debut']);

        if ($debut > time()) {
            Page::addError("Vous ne pouvez pas faire de demande pour le passé!");
            Page::goBack();
        }

        GardeService::newDemande(
            $_SESSION['id'],
            $_GET['babysitter'],
            Database::timestampToSQLString($values['date_demande']),
            $debut,
            Database::timestampToSQLString($values['fin']),
            $values['prix'],
            $values['commission'],
            $values['nb_enfants'],
            Database::timestampToSQLString($values['limit_annulation']),
            Database::timestampToSQLString($values['limit_comment']),
            Database::timestampToSQLString($values['limit_invalidation']),
            $values['autovalid']
        );
        $babysitter_data = UserService::getDataFromId($_GET['babysitter']);
        EmailService::demande(
            $babysitter_data['pseudo'],
            $babysitter_data['email'],
            $_SESSION['id'], $values['debut'], $values['prix'], $_SESSION['pseudo']);
        SoldeService::modify(-$total);
        Page::addInfo("La demande de garde a bien été envoyée. L'argent a été déduit de votre solde, " .
            "il vous sera restitué si la garde est annulée ou invalidée.");
        Page::goHome();
    }

    /**
     * Lister les babysitters disponible pour un créneau horaire précis.
     *
     * Formulaire correspondant sur ../view/famille/trouver_un_babysitter.php
     */
    public static function demandeAction()
    {
        $form = new FormChecker($_GET);
        $form->addField("date_debut", "date de début", "date");
        $form->addField("heure_debut", "heure de début", "time");
        $form->addField("duree", "durée", "time");
        $form->addField("nb_enfants", "nombre d'enfants", "/[0-4]/");
        if (!$form->parse()) {
            Page::goHome();
        }
        $date_demande_timestamp = time();

        $settings = Settings::getCurrent();

        $debut_string = $_GET['date_debut'] . " " . $_GET['heure_debut'];
        $debut = strtotime($debut_string);

        if ($debut < $date_demande_timestamp) {
            Page::addError("Vous ne pouvez pas faire de demande pour le passé!");
            Page::goBack();
        }

        list($heures, $min) = explode(":", $_GET['duree']);
        $fin = $debut + $heures * 3600 + $min * 60;
        $fin_string = date("Y-m-d H:i", $fin);

        $babysitters = Babysitter::genFromDispo($_SESSION['code_postal'], $debut_string, $fin_string);

        $n = count($babysitters);
        $prices = new SplFixedArray($n);
        $commissions = new SplFixedArray($n);
        $total_prices = new SplFixedArray($n);
        $autovalids = new SplFixedArray($n);

        for ($i = 0; $i < $n; $i++) {
            $babysitter = $babysitters[$i];
            $price = $babysitter->getTauxHoraire() / 3600 * ($fin - $debut) * pow(
                    $babysitter->getMultiplicateur(), $_GET['nb_enfants'] - 1);
            $prices[$i] = round($price, 2);
            $commissions[$i] = round($price * $settings['commission'], 2);
            $total_prices[$i] = round($price + $commissions[$i], 2);
            $autovalids[$i] = ($total_prices[$i] < $settings['prix_max']) ? 1 : 0;
        }

        // Utile pour envoyer la demande
        Page::add("limit_annulation", $debut - $settings['annulation'] * 3600);
        Page::add("limit_comment", $fin + $settings['commentaire'] * 3600);
        Page::add("limit_invalidation", $fin + $settings['validation'] * 3600);
        Page::add('date_demande_timestamp', $date_demande_timestamp);
        Page::add('debut_timestamp', $debut);
        Page::add('fin_timestamp', $fin);
        // Utile aussi pour l'affichage
        Page::add('babysitters', $babysitters);
        Page::add('prices', $prices);
        Page::add('total_prices', $total_prices);
        Page::add('commissions', $commissions);
        Page::add('autovalids', $autovalids);
        Page::add('debut', strftime("%A %e %B %Y %H:%M", $debut));
        Page::add('fin', strftime("%A %e %B %Y %H:%M", $fin));
        Page::add('nb_enfants', $_GET['nb_enfants'] . " enfant" . ($_GET['nb_enfants'] > 1 ? "s" : ""));
        Page::addArray($settings);
        Page::addView("famille/demande.php");
    }
}