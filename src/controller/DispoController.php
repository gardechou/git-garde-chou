<?php
/**
 * Définition de la classe DispoController
 *
 * @package Contrôleurs
 * @tag Baby-sitter
 */

/**
 * Les actions en rapport avec les commentaires
 *
 * Grâce à l'utilisation de l'autoloader, il n'est pas nécessaire de vérifier que l'on est connecté en
 * tant que babysitter pour chacune de ces actions.
 */
class DispoController
{


    /**
     * Lister les dispos de l'utilisateur courant
     *
     */
    public static function listerAction()
    {   //TODO; Verifier que le lister, genere pas si page = vide pour d'autres variables (commentaires ou autres)
        //TODO: écrire une fonction générique pour l'affichage sur plusieurs pages
        $dispos_per_page = 10;
        $page = (isset($_GET["page"])) ? $_GET["page"] : 1;
        $dispos = Dispo::genFromBabysitterId($_SESSION['id'], $dispos_per_page +1, $page);
        if (empty($dispos) and $page > 1) {
            Page::go("/dispo/lister");
        }
        Page::add("dispos",array_slice($dispos,0,$dispos_per_page));
        Page::add("page",$page);
        Page::add("nextpage",(count($dispos) > $dispos_per_page));
        Page::addView("babysitter/dispo.php");
        Page::add("dispos_geo", DispoGeoService::getFromBabysitterId($_SESSION['id']));
        Page::addToTitle("Mes disponibilités");
        Page::setNavbar("dispo");
        Page::addView("babysitter/dispogeo.php");
    }

    /**
     * Ajouter un code postal
     *
     * Reçoit "code_postal" par POST et l'ajoute à la BDD pour le babysitter connecté
     */
    public static function ajouter_geoAction()
    {
        $form = new FormChecker($_POST);
        $form->addField("code_postal", "code postal", "zipcode");
        if ($form->parse()) {
            if (DispoGeoService::isPresent($_SESSION['id'], $_POST['code_postal'])) {
                Page::addError("Le code postal est déjà présent.");
            } else {
                DispoGeoService::add($_SESSION['id'], $_POST['code_postal']);
                Page::addInfo("Code postal ajouté avec succès");
            }
        } else {
            Page::addError("Code postal invalide.");
        }
        Page::goBack();
    }

    /**
     * Supprimer un code postal
     *
     * Reçoit "code_postal" par POST et le retire de la BDD pour le babysitter connecté
     */
    public static function supprimer_geoAction()
    {
        $form = new FormChecker($_POST);
        $form->addField("code_postal", "code postal", "zipcode");
        if ($form->parse()) {
            if (DispoGeoService::isPresent($_SESSION['id'], $_POST['code_postal'])) {
                DispoGeoService::remove($_SESSION['id'], $_POST['code_postal']);
            } else {
                Page::addError("Le code postal n'est pas dans la BDD!");
            }
        } else {
            Page::addError("Le code postal n'est pas valide!");
        }
        Page::goBack();
    }

    // TODO: ajouter vérification que la date est bien dans le futur et que fin > debut

    /**
     * Ajoute une des disponibilités temporelles pour le baby sitter connecté.
     *
     * POST doit contenir:
     * - "type_dispo" = (reguliere|ponctuelle) et "taux_horaire"
     * - pour les dispos régulières : "duree", "debut", "fin"
     * - pour une dispo ponctuelle "date_debut" "date_fin" "time_debut" "time_fin"
     *
     */
    public static function ajouterAction()
    {
        $form = new FormChecker($_POST);
        $form->addField("type_dispo", "type de disponibilité", "/reguliere|ponctuelle/");
        $form->addField("taux_horaire", "taux horaire", "float");
        $success = $form->parse();
        if (!$success) {
            Page::addErrors($form->getErrors());
            Page::goBack();
        }

        $dispos_existantes = Dispo::genFromBabysitterId($_SESSION["id"]);

        if ($_POST["type_dispo"] == "reguliere") {
            $form = new FormChecker($_POST);
            $form->addField("duree", "durée", "/\d*/");
            $form->addField("debut", "heure de début", "time");
            $form->addField("fin", "heure de fin", "time");
            $success = $form->parse();
            // TODO: empêcher de passer plus d'un an de dispo
            if (!$success) {
                Page::addErrors($form->getErrors());
                Page::goBack();
            }

            list($heure_debut, $min_debut) = explode(":", $_POST["debut"]);
            list($heure_fin, $min_fin) = explode(":", $_POST["fin"]);
            $jours = [];
            for ($i = 1; $i <= 7; $i++) {
                // TODO: ajouter vérification qu'on passe pas nawak comme valeur de jourX par POST, ie, populer $jours en dur
                if (isset($_POST["jour" . $i])) {
                    $jours[] = $_POST["jour" . $i];
                }
            }
            $debuts = [];
            $fins = [];
//            $dispos = [];
            foreach ($jours as $jour) {
                $minuit = strtotime("next " . $jour);
                for ($i = 0; $i < $_POST["duree"]; $i++) {
                    // 1semaine = 604800s
                    $dispo = new Dispo();
                    $debut = $minuit + $heure_debut * 3600 + $min_debut * 60 + 604800 * $i;
                    $fin = $minuit + $heure_fin * 3600 + $min_fin * 60 + 604800 * $i;
                    $dispo->setDebut($debut);
                    $dispo->setFin($fin);
                    if (!$dispo->hasConflictWith($dispos_existantes)) {
                        $debuts[] = $debut;
                        $fins[] = $fin;
                    }
                }
            }

            if (empty($debuts)) {
                Page::addError("Conflit(s) avec d'autres disponibilités");
            } else {
                DispoService::addFromArraysWithSameBabysitterAndTauxHoraire($_SESSION["id"], $debuts, $fins, $_POST['taux_horaire']);
            }
            Page::addInfo(count($debuts) . " disponibilité(s) régulière(s) ajoutée(s).");
        } elseif ($_POST["type_dispo"] == "ponctuelle") {
            $form = new FormChecker($_POST);
            $form->addField("date_debut", "date de début", "date");
            $form->addField("date_fin", "date de fin", "date");
            $form->addField("time_debut", "heure de début", "time");
            $form->addField("time_fin", "heure de fin", "time");
            $success = $form->parse();
            if (!$success) {
                Page::addErrors($form->getErrors());
                Page::goBack();
            }
            $dispo = new Dispo();
            $debut = strtotime($_POST['date_debut'] . " " . $_POST["time_debut"]);
            $fin = strtotime($_POST['date_fin'] . " " . $_POST["time_fin"]);

            $dispo->setDebut($debut);
            $dispo->setFin($fin);

            if ($dispo->hasConflictWith($dispos_existantes)) {
                Page::addError("Cette disponibilité entre en conflit avec une autre.");
            } elseif ($debut > $fin) {
                Page::addError("La date de début et la date de fin sont incohérentes!.");
            } else {
                $dispo->setBabysitter($_SESSION['id']);
                $dispo->setTauxHoraire($_POST['taux_horaire']);
                DispoService::addFromObject($dispo);
                Page::addInfo("Disponibilité ajoutée.");
            }
        }

        Page::goBack();
    }

    /**
     * Supprimer une disponibilité temporelle
     *
     * POST doit contenir "dispo_id". La dispo est supprimé si elle appartient bien à l'utilisateur
     * actuellement connecté.
     * "dispo_id" peut aussi être "all".
     */
    public static function supprimerAction()
    {
        $form = new FormChecker($_POST);
        $form->addField("dispo_id", "ID de la disponibilité", "/\d*|all/");

        if (!$form->parse()) {
            Page::addError("Pas d'ID précisé. Comment vous êtes-vous retrouvé ici&nbsp;?");
            Page::goBack();
        }

        $dispo_id = $form->getStructuredArray()["dispo_id"];

        if ($dispo_id == "all") {
            DispoService::removeAllForBabysitter(Session::getCurrentUserId());
            Page::addInfo("Toutes vos disponibilités ont été supprimées.");
            Page::go("/dispo/lister");
        }

        if (DispoService::removeIfProprio($dispo_id, Session::getCurrentUserId())) {
            Page::addInfo("Disponibilité supprimée.");
        } else {
            Page::addError("Impossible de supprimer la disponibilité.");
        }

        Page::goBack();
    }
}