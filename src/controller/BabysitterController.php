<?php
/**
 * Définition de la classe BabysitterController
 *
 * @package Contrôleurs
 * @tag Baby-sitter
 */

/**
 * Les actions des baby-sitters
 *
 * Grâce à l'utilisation de l'autoloader, il n'est pas nécessaire de vérifier que l'on est connecté en
 * tant que babysitter pour chacune de ces actions.
 */
class BabysitterController
{
    /**
     * Le message d'avertissement à afficher au cas où le baby-sitter n'a aucune disponibilité géographique
     */
    const NO_DISPO_GEO = 'Vous n\'avez pas encore ajouté de codes postaux où vous souhaitez faire du babysitting !' .
    '<a href="' . URI_PREFIX . '/dispo/lister"> Cliquez ici pour ajouter des disponibilités géographiques.</a>';

    /**
     * Le message d'avertissement à afficher au cas où le baby-sitter n'a aucune disponibilité temporelle
     */
    const NO_DISPO = "Vous n'avez pas encore ajouté de disponibilités temporelles ! " .
    '<a href="' . URI_PREFIX . '/dispo/lister">Cliquez ici pour ajouter des disponibilités temporelles.</a>';

    /**
     * Le message d'avertissement à afficher lorsqu'un compte n'est pas encore validé ou a été suspendu par l'administrateur
     */
    const INVALID = "Votre compte n'est pas encore validé ou a été suspendu. Vous n'apparaîtrez pas dans les recherches.";

    /**
     * Page d'accueil des baby-sitters.
     */
    public static function homeAction()
    {
        Page::addToTitle("Bienvenue " . Session::getCurrentUserPseudo() . " !");
        if (!BabysitterService::isValide(Session::getCurrentUserPseudo())) {
            Page::addInfo(self::INVALID);
        }
        if (empty(DispoGeoService::getFromBabysitterId(Session::getCurrentUserId()))) {
            Page::addInfo(self::NO_DISPO_GEO);
        }
        if (empty(DispoService::getFromBabysitterId(Session::getCurrentUserId()))) {
            Page::addInfo(self::NO_DISPO);
        }
        $gardes = Garde::genActiveFromBabysitterId($_SESSION['id']);
        Page::add("nextpage", false);
        Page::add("page", 1);
        Page::add("gardes", $gardes);
        Page::add("gardes_title", "Vos gardes actives");
        $n_demandes = 0;
        foreach ($gardes as $garde) {
            if ($garde->isAcceptable()) {
                $n_demandes++;
            }
        }
        if ($n_demandes) {
            list($s, $l, $la) = ($n_demandes > 1) ? ["s", "les ", "les"] : ["", "l'", "la"];
            Page::addWarning("Vous avez $n_demandes demande$s de garde$s. " .
                "Vous devriez ${l}accepter ou $la refuser avant qu'il ne soit trop tard.");
        }
        Page::addView("gardes.php");
    }
}