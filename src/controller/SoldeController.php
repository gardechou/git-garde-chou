<?php
/**
 * Définition de la class SoldeController
 *
 * @package Contrôleurs
 * @tag Famille
 * @tag Babysitter
 */

/**
 * Gestion du solde des clients.
 * Met à jour la base de données et la variable de session.
 *
 */
class SoldeController {
    /**
     * Ajouter un montant au solde du client connecté
     *
     * Le montant est passé via GET.
     */
    static public function addAction() {
        if (Session::isLoggedIn()) {
            SoldeService::modify($_GET['montant']); // TODO: Utiliser POST ici
            Page::addInfo("Votre compte a bien été crédité.");
            Page::goBack();
        } else {
            DefaultController::homeAction();
        }
    }

    /** Remet le solde du client connecté à zéro. */
    static public function cashoutAction() {
        if (Session::isLoggedIn()) {
            SoldeService::cashout();
            Page::goBack();
        } else {
            DefaultController::homeAction();
        }
    }
}