<?php
/**
 * Définition de la classe CommentairesController
 *
 * @package Contrôleurs
 * @tag Baby-sitter, Famille
 */

/**
 * Les actions en rapport avec les commentaires
 *
 * Grâce à l'utilisation de l'autoloader, il n'est pas nécessaire de vérifier que l'on est connecté en
 * tant que babysitter ou famille pour chacune de ces actions.
 */
class CommentairesController
{
    /**
     * Ajouter un commentaire.
     *
     * Reçoit les données par POST, qui doivent être "garde_id", "note", et "commentaire" (optionnel)
     * Ajoute le commentaire à la BDD si l'utilisateur a les permissions nécessaires et la garde est encore
     * commentable.
     */
    public static function ajouterAction()
    {
        if (empty($_POST)) {
            Page::goBack();
        }

        $form = new FormChecker($_POST);
        $form->addField("garde_id", "ID de la garde", "/\d*/");
        $form->addField("note", "Note de la garde", "/\d*/");
        $form->addField("commentaire", "Commentaire", "/.*/", 0, INF, null, null, $optional = true);
        if ($form->parse()) {
            $garde = Garde::genFromId($_POST['garde_id']);
            if ($garde->isCommentableBy($_SESSION['id'])) {
                CommentService::updateCommentAndNoteFromGardeIdAndUserType($_POST['garde_id'], $_POST['commentaire'], $_POST['note'], $_SESSION['type']);
                Page::addInfo("Note et commentaire ajoutés avec succès.");
            } else {
                Page::addError("Vous n'avez pas le droit de commenter cette garde.");
            }
        } else {
            Page::addErrors($form->getErrors());
        }
        Page::goHome();
    }


    /**
     * Listes les commentaires postés par l'utilisateur courant.
     */
    public static function listerAction()
    {
        Page::addToTitle("Liste des commentaires");
        Page::setNavbar("comment");
        $cur_user = new Client();
        $cur_user->setId(Session::getCurrentUserId());
        $cur_user->populateAllComments();
        Page::add("comments", $cur_user->getMesCommentaires());
        Page::addView("mes_commentaires.php");
    }
}