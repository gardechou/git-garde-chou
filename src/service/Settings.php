<?php
/**
 * @package Services
 * @tag Administrateur
 */

/** Gestion des paramètres généraux du site (BDD) */
class Settings
{
    /**
     * Récupérer tous les paramètres du site actuellement en application
     *
     * @return array Un array structuré de la forme ["param1"=>"val1", "param2"=>"val2", ...]
     */
    public static function getCurrent()
    {
        $query = Database::getPdo()->prepare("
                    SELECT commission,
                           delai_annulation AS annulation,
                           delai_commentaire AS commentaire,
                           delai_validation AS validation,
                           prix_max
                    FROM parametres ORDER BY DATE DESC LIMIT 1");
        $query->execute();
        return $query->fetch();
    }

    /**
     * Mettre à jour les paramètres du sites
     *
     * @param float $commission
     * @param int $prix_max
     * @param int $annulation
     * @param int $comment
     * @param int $valid
     */
    public static function set($commission, $prix_max, $annulation, $comment, $valid)
    {
        // MySQL remplit la date avec "maintenant"
        $query = Database::getPdo()->prepare("
                    INSERT INTO parametres (commission,
                                            delai_annulation,
                                            delai_commentaire,
                                            delai_validation,
                                            prix_max,
                                            set_by)
                    VALUES (:commission, :annulation, :comment, :valid, :prix_max, :set_by)");
        $query->bindParam(":commission", $commission);
        $query->bindParam(":prix_max", $prix_max, PDO::PARAM_INT);
        $query->bindParam(":annulation", $annulation, PDO::PARAM_INT);
        $query->bindParam(":comment", $comment, PDO::PARAM_INT);
        $query->bindParam(":valid", $valid, PDO::PARAM_INT);
        $query->bindParam("set_by", $_SESSION["id"], PDO::PARAM_INT);
        $query->execute();
    }
}