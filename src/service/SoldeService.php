<?php
/**
 * Définition de la classe SoldeService
 *
 * @package Services
 * @tag Baby-sitter
 * @tag Famille
 */

/**
 * Le service gérant la mise à jour du solde dans la BDD et la session si nécessaire.
 */
class SoldeService
{
    /**
     * @param float $amount Le montant à ajouter.
     * @param int $user_id L'utilisateur dont on veut modifier le solde.
     * Si pas précisé, il s'agit de l'utilisateur connecté.
     */
    public static function modify($amount, $user_id = null)
    {
        if (is_null($user_id)) {
            $user_id = $_SESSION['id'];
        }
        if ($user_id == $_SESSION['id']) {
            $_SESSION['solde'] += $amount;
        }
        $query = Database::getPdo()->prepare("UPDATE user SET solde = solde + ?, date_creation = date_creation WHERE id = ?");
        $query->bindParam(1, $amount);
        $query->bindParam(2, $user_id, PDO::PARAM_INT);
        $query->execute();
    }

    /** Remet le solde de l'utilisateur connecté à zéro. */
    public static function cashout()
    {
        $new_solde = 0;
        $query = Database::getPdo()->prepare("UPDATE user SET solde = ?, date_creation = date_creation WHERE id = ?");
        $query->bindParam(1, $new_solde);
        $query->bindParam(2, $_SESSION['id'], PDO::PARAM_INT);
        $query->execute();
        $_SESSION['solde'] = $new_solde;
    }

//    /** Enlève de l'argent du solde de l'utilisateur */
//    public static function remove($amount, $user_id = null)
//    {
//        if (is_null($user_id)) {
//            $user_id = $_SESSION['id'];
//            $new_solde = $_SESSION['solde'] + $amount;
//            $_SESSION['solde'] = $new_solde;
//        }
//        $new_solde = $_SESSION['solde'] - $amount;
//        $query = Database::getPdo()->prepare("UPDATE user SET solde = ? WHERE id = ?");
//        $query->bindParam(1, $new_solde);
//        $query->bindParam(2, $_SESSION['id'], PDO::PARAM_INT);
//        $query->execute();
//        $_SESSION['solde'] = $new_solde;
//    }
}