<?php

/**
 * Définition de la classe CommentService
 *
 * @package Services
 */

/**
 * Le service qui gère les interactions avec la base de donnée en relation avec les commentaires.
 */
class CommentService
{
    /**
     * Retourne la note moyenne pour un client.
     *
     * Ne concerne que les commentaires publiés.
     *
     * @param int $id L'id de l'utilisateur.
     * @param int $type Le type de compte utilisateur (User::TYPE_BABYSITTER ou User::TYPE_FAMILLE)
     * @return array ["note_moyenne"=>float, "nombre_notes"=>int (le nombre de notes sur lesquelles la moyenne a été calculée)]
     */
    public static function getNoteMoyenneFromId($id, $type)
    {
        if ($type == User::TYPE_FAMILLE) {
            $suffix = "_bs";
            $type_str = "famille";
        } else {
            $type_str = "babysitter";
            $suffix = "_f";
        }
        $query = Database::getPdo()->prepare(
            "SELECT
              AVG(note$suffix) AS note_moyenne, 
              COUNT(note$suffix) AS nombre_notes
            FROM
              garde
            WHERE $type_str = :id AND lim_comment < NOW()");
        $query->bindParam("id", $id);
        $query->execute();
        $data = $query->fetch();
        return $data;
    }

    /**
     * Met à jour les attributs commentaires et notes pour un utilisateur sur une garde.
     *
     * @param int $garde_id L'id de la garde concernée.
     * @param string $comment Le texte du commentaire.
     * @param int $note La note de 1 à 5.
     * @param int $type Le type de compte de l'auteur du commentaire (User::TYPE_BABYSITTER ou User::TYPE_FAMILLE)
     */
    public static function updateCommentAndNoteFromGardeIdAndUserType($garde_id, $comment, $note, $type)
    {
        $suffix = ($type == "famille" | $type == User::TYPE_FAMILLE) ? "f" : "bs";
        $query = Database::getPdo()->prepare("UPDATE garde SET comment_$suffix = :comment, note_$suffix = :note WHERE id = :garde");
        $query->bindParam(":garde", $garde_id, PDO::PARAM_INT);
        $query->bindParam(":comment", $comment);
        $query->bindParam(":note", $note, PDO::PARAM_INT);
        $query->execute();
    }

    /**
     * Met à jour l'attribut commentaire pour un utilisateur sur une garde.
     * @param int $garde_id L'id de la garde concernée.
     * @param string $comment Le string du commentaire.
     * @param int $type Le type de client de l'auteur du commentaire (User::TYPE_BABYSITTER ou User::TYPE_FAMILLE).
     */

    public static function updateCommentFromGardeIdAndUserType($garde_id, $comment, $type)
    {
        $suffix = ($type == User::TYPE_BABYSITTER) ? "bs" : "f";
        $query = Database::getPdo()->prepare("UPDATE garde SET comment_$suffix = :comment WHERE id = :garde");
        $query->bindParam(":garde", $garde_id, PDO::PARAM_INT);
        $query->bindParam(":comment", $comment);
        $query->execute();
    }

    /**
     * Retourne tous les commentaires postés et reçus par un utilisateur.
     *
     * @param int $id L'id de l'utilisateur.
     * @return array Un tableau associatif de type ["par_moi"=>..., "sur_moi"=>...] (voir CommentService::genParMoiSurMoi)
     */
    public static function getAllCommentsForUser($id)
    {
        $query = Database::getPdo()->prepare(
            "SELECT
              garde.note_f, garde.note_bs, garde.comment_f, garde.comment_bs, garde.fin, garde.lim_comment,
              garde.id AS garde,
              famille.pic AS pic_famille,
              babysitter.pic AS pic_babysitter,
              garde.babysitter AS id_bs,
              garde.famille AS id_f,
              babysitter.pseudo AS babysitter,
              famille.pseudo AS famille
            FROM
              garde
              JOIN user babysitter ON garde.babysitter = babysitter.id
              JOIN user famille ON garde.famille = famille.id
            WHERE (babysitter = :id OR famille = :id)");
        $query->execute(["id" => $id]);
        $data = $query->fetchAll();
        return self::genParMoiSurMoi($data, $id);
    }

    /**
     * Retourne les commentaires publiés postés et reçus par un utilisateur.
     *
     * @param int $id L'id de l'utilisateur.
     * @return array Un tableau associatif de type ["par_moi"=>..., "sur_moi"=>...] (voir CommentService::genParMoiSurMoi)
     */
    public static function getPublishedCommentsForUser($id)
    {
        $query = Database::getPdo()->prepare(
            "SELECT
              garde.note_f, garde.note_bs, garde.comment_f, garde.comment_bs, garde.fin, garde.lim_comment,
              garde.id AS garde,
              famille.pic AS pic_famille,
              babysitter.pic AS pic_babysitter,
              garde.babysitter AS id_bs,
              garde.famille AS id_f,
              babysitter.pseudo AS babysitter,
              famille.pseudo AS famille
            FROM
              garde
              JOIN user babysitter ON garde.babysitter = babysitter.id
              JOIN user famille ON garde.famille = famille.id
            WHERE (babysitter = :id OR famille = :id) AND lim_comment < NOW()");
        $query->execute(["id" => $id]);
        $data = $query->fetchAll();
        return self::genParMoiSurMoi($data, $id);
    }


    /**
     * À partir de "lignes" SQL de garde, génère un tableau associatif liés à ses commentaires.
     *
     * Le résultats est de la forme ["par_moi"=>..., "sur_moi"=>...] où ... comprend les champs:
     * "sur" => Pseudo de l'utilisateur concerné par le commentaire.
     * "sur_id" => id de l'utilisateur conecerné par le commentaire.
     * "contenu" => Texte du commentaire.
     * "note" => Note entre 1 et 5.
     * "garde" => id de la garde associée au commentaire.
     * "limite" => Date limite de modification (=date de publication) du commentaire.
     * "par" => Pseudo de l'auteur du commentaire.
     * "par_id" => Id de l'auteur du commentaire.
     * NB: les 2 derniers champs concernent uniquement "sur_moi"
     *
     * @param array $data Une ligne SQL telle que retournée par getAllCommentsForUser()
     * @param int $id L'id de l'utilisateur concerné.
     * @return array La liste des commentaires (voir plus haut)
     */
    private static function genParMoiSurMoi($data, $id)
    {
        $sur_moi = [];
        $par_moi = [];
        if (!empty($data)) {
            $moi_court = ($data[0]["id_bs"] == $id) ? "bs" : "f";
            $autre = ($data[0]["id_bs"] == $id) ? "famille" : "babysitter";
            $autre_court = ($data[0]["id_bs"] == $id) ? "f" : "bs";
            foreach ($data as $row) {
                if (!empty($row["note_$moi_court"])) {
                    $par_moi[] = [
                        "sur" => $row[$autre],
                        "sur_id" => $row["id_$autre_court"],
                        "contenu" => $row["comment_$moi_court"],
                        "note" => $row["note_$moi_court"],
                        "date" => strtotime($row["fin"]),
                        "garde" => $row["garde"],
                        "limite" => strtotime($row["lim_comment"])
                    ];
                }
                if (!empty($row["note_$autre_court"])) {
                    $sur_moi[] = [
                        "par" => $row[$autre],
                        "par_id" => $row["id_$autre_court"],
                        "contenu" => $row["comment_$autre_court"],
                        "note" => $row["note_$autre_court"],
                        "date" => strtotime($row["fin"]),
                        "pic" => $row["pic_$autre"],
                        "garde" => $row["garde"],
                        "limite" => strtotime($row["lim_comment"])
                    ];
                }
            }
        }
        return ["par_moi" => $par_moi, "sur_moi" => $sur_moi];
    }

    /**
     * @param int $n Le nombre de commentaires à renvoyer
     * @param int $page La "page" de commentaires à renvoyer
     * @param string $sort_by L'attribut par lequel trier les commentaires
     * @return array
     */
    public static function getAll($n, $page, $sort_by)
    {
        $query = Database::getPdo()->prepare(
            "SELECT
                garde.id AS garde_id,
                garde.comment_f AS comment_famille,
                garde.note_f AS note_famille,
                garde.comment_bs AS comment_babysitter,
                garde.note_bs AS note_babysitter,
                garde.babysitter AS babysitter_id,
                garde.famille AS famille_id,
                garde.lim_comment AS date,
                babysitter.pseudo AS babysitter_pseudo,
                famille.pseudo AS famille_pseudo
            FROM garde
            JOIN user babysitter ON garde.babysitter = babysitter.id
            JOIN user famille ON garde.famille = famille.id
            WHERE garde.note_bs IS NOT NULL OR garde.note_f IS NOT NULL
            ORDER BY $sort_by DESC LIMIT ? OFFSET ?");
        $query->bindParam(1, $n, PDO::PARAM_INT);
        $offset = ($page - 1) * $n;
        $query->bindParam(2, $offset, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll();
        $res = [];
        foreach ($data as $row) {
            if (!empty($row['note_famille'])){
                $comm = [
                    "by" => $row["famille_pseudo"],
                    "on" => $row["babysitter_pseudo"],
                    "by_id" => $row["famille_id"],
                    "on_id" => $row["babysitter_id"],
                    "date" => strtotime($row["date"]),
                    "contenu" => $row["comment_famille"],
                    "note" => $row["note_famille"],
                    "garde" => $row["garde_id"],
                    "by_type" => User::TYPE_FAMILLE
                ];
                $res[] = $comm;
            }
            if (!empty($row['note_babysitter'])){
                $comm = [
                    "by" => $row["babysitter_pseudo"],
                    "on" => $row["famille_pseudo"],
                    "by_id" => $row["babysitter_id"],
                    "on_id" => $row["famille_id"],
                    "date" => strtotime($row["date"]),
                    "contenu" => $row["comment_babysitter"],
                    "note" => $row["note_babysitter"],
                    "garde" => $row["garde_id"],
                    "by_type" => User::TYPE_BABYSITTER
                ];
                $res[] = $comm;
            }
        }
        return $res;
    }
}