<?php
/**
 * Définition de la classe DispoService
 *
 * @package Services
 * @tag Baby-sitter
 */

/**
 * Ce service gère les interactions avec la BDD en rapport avec les dispos temporelles babysitters
 */
class DispoService //TODO: documenter les docblocks
{
    /**
     * @param $id
     * @param $n
     * @param int $page
     * @return array
     */
    public static function getFromBabysitterId($id, $n = INF, $page = 0)
    {
        if ($n == INF) {
            $query = Database::getPdo()->prepare("SELECT * FROM dispo WHERE babysitter = ? ORDER BY debut");
        } else {
            $query = Database::getPdo()->prepare("SELECT * FROM dispo WHERE babysitter = ? ORDER BY debut LIMIT ? OFFSET ?");
            $offset = ($page - 1) * $n;
            $query->bindParam(2, $n, PDO::PARAM_INT);
            $query->bindParam(3, $offset, PDO::PARAM_INT);
        }
        $query->bindParam(1, $id, PDO::PARAM_INT);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * @param $id
     * @param $babysitter
     * @return int
     */
    public static function removeIfProprio($id, $babysitter)
    {
        $query = Database::getPdo()->prepare("DELETE FROM dispo WHERE id = ? AND babysitter = ?");
        $query->bindParam(1, $id, PDO::PARAM_INT);
        $query->bindParam(2, $babysitter, PDO::PARAM_INT);
        $query->execute();
        return $query->rowCount();
    }

    public static function cutDispo($babysitter, $garde_debut, $garde_fin)
    {
        // On récupère la dispo concernée
        $query = Database::getPdo()->prepare(
            "SELECT 
              dispo.id, dispo.debut, dispo.fin, dispo.taux_horaire
            FROM dispo
            WHERE babysitter = :bs AND :garde_debut > dispo.debut AND :garde_fin < dispo.fin");
        $query->bindParam("bs", $babysitter, PDO::PARAM_INT);
        $query->bindParam("garde_debut", Database::timestampToSQLString($garde_debut));
        $query->bindParam("garde_fin", Database::timestampToSQLString($garde_fin));
        $query->execute();
        $old_dispo = $query->fetch();
        // On la supprime de la BDD
        $query = Database::getPdo()->prepare("DELETE FROM dispo WHERE id = ?");
        $query->bindParam(1, $old_dispo['id'], PDO::PARAM_INT);
        $query->execute();
        // On calcule les 2 nouvelles dispos
        $dispo1_debut = strtotime($old_dispo["debut"]);
        $dispo1_fin = $garde_debut;
        $dispo2_debut = $garde_fin;
        $dispo2_fin = strtotime($old_dispo["fin"]);
        if ($dispo1_debut < $dispo1_fin) {
            DispoService::add($babysitter, $dispo1_debut, $dispo1_fin, $old_dispo['taux_horaire']);
        }
        if ($dispo2_debut < $dispo2_fin) {
            DispoService::add($babysitter, $dispo2_debut, $dispo2_fin, $old_dispo['taux_horaire']);
        }
    }

    /**
     * @param $babysitter
     */
    public static function removeAllForBabysitter($babysitter)
    {
        $query = Database::getPdo()->prepare("DELETE FROM dispo WHERE babysitter = ?");
        $query->bindParam(1, $babysitter, PDO::PARAM_INT);
        $query->execute();
    }

    /**
     * @param $babysitter
     * @param $debut
     * @param $fin
     * @param $taux_horaire
     */
    public static function add($babysitter, $debut, $fin, $taux_horaire)
    {
        $query = Database::getPdo()->prepare("INSERT INTO dispo (babysitter, debut, fin, taux_horaire) VALUES (?,?,?,?)");
        $query->bindParam(1, $babysitter, PDO::PARAM_INT);
        $query->bindParam(2, Database::timestampToSQLString($debut));
        $query->bindParam(3, Database::timestampToSQLString($fin));
        $query->bindParam(4, $taux_horaire);
        $query->execute();
    }

    /**
     * @param $dispo
     */
    public static function addFromObject($dispo)
    {
        self::add(
            $dispo->getBabysitter(),
            $dispo->getDebut(),
            $dispo->getFin(),
            $dispo->getTauxHoraire()
        );
    }

    /**
     * @param $babysitter
     * @param $debuts
     * @param $fins
     * @param $taux_horaire
     */
    public static function addFromArraysWithSameBabysitterAndTauxHoraire($babysitter, $debuts, $fins, $taux_horaire)
    {
        // pour se protéger des injections sql puisqu'on va pas faire pdo->prepare pour ces deux valeurs
        $babysitter = (int)$babysitter;
        $taux_horaire = (float)$taux_horaire;
        foreach ($debuts as $i => $debut) {
            $values[] = "($babysitter, ?, ?, $taux_horaire)";
        }
        $statement = "INSERT INTO dispo (babysitter, debut, fin, taux_horaire) VALUES " . join(",", $values);
        $query = Database::getPdo()->prepare($statement);
        foreach ($debuts as $i => $debut) {
            $j = $i * 2 + 1;
            $k = $i * 2 + 2;
            $query->bindParam($j, Database::timestampToSQLString($debut));
            $query->bindParam($k, Database::timestampToSQLString($fins[$i]));
        }
        $query->execute();
    }
}