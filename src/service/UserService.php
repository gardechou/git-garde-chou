<?php

/**
 * Gère les interactions avec la table user communs à tous les utilisateurs.
 *
 * @package Services
 */
class UserService
{
    /**
     * Valide un couple (pseudo, mot de passe)
     *
     * @param string $pseudo
     * @param string $pass
     *
     * @return bool
     */
    public static function authenticate($pseudo, $pass)
    {
        // On récupère la version "hashée" du mot de passe
        $query = Database::getPdo()->prepare("SELECT password FROM user WHERE pseudo = ?");
        $query->execute(array($pseudo));
        $data = $query->fetch();
        $real_pass = $data['password'];
        // On compare avec le mot de passe passé en argument
        return password_verify($pass, $real_pass);
    }

    /**
     * Récupérer toutes les infos sur un utilisateur à partir de son pseudo
     *
     * @param string $pseudo
     * @return array[]
     */
    public static function getDataFromPseudo($pseudo)
    {
        $query = Database::getPdo()->prepare("SELECT * FROM user WHERE pseudo = ?"); //TODO: enlever ce vilain select *
        $query->execute(array($pseudo));
        $data = $query->fetch();
        return $data;
    }

    /** Récupérer toutes les infos sur un utilisateur à partir de son id
     *
     * @param int $id
     * @return array[]
     */
    public static function getDataFromId($id)
    {
        $query = Database::getPdo()->prepare("SELECT * FROM user WHERE id = ?"); //TODO: enlever ce vilain select *
        $query->bindParam(1, $id, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetch();
        return $data;
    }

    /**
     * Vérifie si un pseudo est disponible (=absent dans la table user)
     *
     * @param string $pseudo Le pseudo à vérifier
     * @return bool true s'il est disponible, false sinon
     */
    public static function isPseudoAvailable($pseudo)
    {
        $query = Database::getPdo()->prepare("SELECT id FROM user WHERE pseudo = ?");
        $query->execute(array($pseudo));
        return empty($query->fetchAll());
    }

    /**
     * Vérifie si un email est disponible (=absent dans la table user)
     *
     * @param string $email L'email à vérifier
     * @return bool true s'il est disponible, false sinon
     */
    public static function isEmailAvailable($email)
    {
        $query = Database::getPdo()->prepare("SELECT id FROM user WHERE email = ?");
        $query->execute(array($email));
        return empty($query->fetchAll());
    }

    /**
     * Ajoute un client à la base de données.
     *
     * Les données passées ici doivent avoir été vérifiées en amont
     *
     * @param string $pseudo
     * @param string $nom
     * @param string $prenom
     * @param string $password
     * @param string $type
     * @param string $email
     * @param string $adresse
     * @param string $code_postal
     * @param string $ville
     * @param string $presentation
     * @param float $solde
     * @param bool $valid
     */
    public static function addClient($pseudo, $nom, $prenom, $genre_humain, $date_naissance, $password, $type, $telephone, $email, $adresse, $code_postal, $ville,
                                     $presentation, $solde, $valid, $pic = "default.png")
    {
        $query = Database::getPdo()->prepare("
          INSERT INTO user (pseudo, nom, prenom,genre_humain, date_naissance, password, type, telephone, email, adresse, code_postal, ville, presentation,
                            date_creation, solde, valide, pic)
          VALUES (:pseudo, :nom, :prenom, :genre_humain, :date_naissance, :password, :type, :telephone, :email, :adresse, :code_postal, :ville, :presentation,
                  :date_creation, :solde, :valide, :pic)");
        $query->bindParam(":pseudo", $pseudo);
        $query->bindParam(":nom", $nom);
        $query->bindParam(":prenom", $prenom);
        $query->bindParam(":genre_humain", $genre_humain);
        $query->bindParam(":date_naissance", $date_naissance);
        $query->bindParam(":password", password_hash($password, PASSWORD_DEFAULT));
        $query->bindParam(":type", self::strTypeToIntType($type), PDO::PARAM_INT);
        $query->bindParam(":telephone", $telephone);
        $query->bindParam(":email", $email);
        $query->bindParam(":adresse", $adresse);
        $query->bindParam(":code_postal", $code_postal);
        $query->bindParam(":ville", $ville);
        $query->bindParam(":presentation", $presentation);
        $query->bindParam(":date_creation", date("Y-m-d H:i:s"));
        $query->bindParam(":solde", $solde);
        $valid_int = ($valid) ? 1 : 0;
        $query->bindParam(":valide", $valid_int, PDO::PARAM_INT);
        $query->bindParam(":pic", $pic);
        $query->execute();
    }

    /**
     * @param int $id
     */
    public static function validate($id)
    {
        $query = Database::getPdo()->prepare("UPDATE user SET valide = 1,date_creation=date_creation WHERE id = ?");
        $query->bindParam(1, $id, PDO::PARAM_INT);
        $query->execute();
    }

    /**
     * @param $id
     */
    public static function invalidate($id)
    {
        $query = Database::getPdo()->prepare("UPDATE user SET valide = 0,date_creation=date_creation WHERE id = ?");
        $query->bindParam(1, $id, PDO::PARAM_INT);
        $query->execute();
    }

    //TODO: utiliser des constantes statiques ici
    /**
     * Traduit un type de compte "textuel" en type de compte "entier"
     *
     * @param string $type Doit valoir "admin", "famille" ou "babysitter"
     * @return int|null Entier correspondant, null si mauvaise valeur d'entrée
     */
    private static function strTypeToIntType($type)
    {
        switch ($type) {
            case "admin":
                $res = 0;
                break;
            case "famille":
                $res = 1;
                break;
            case "babysitter":
                $res = 2;
                break;
            default:
                $res = null;
                break;
        }
        return $res;
    }

    /**
     * @param $n
     * @param $page
     * @param $sort_by
     * @return array
     */
    public static function listAll($n, $page, $sort_by)
    {
        $query = Database::getPdo()->prepare("SELECT * FROM user ORDER BY $sort_by DESC LIMIT ? OFFSET ?");
        $query->bindParam(1, $sort_by);
        $query->bindParam(1, $n, PDO::PARAM_INT);
        $offset = ($page - 1) * $n;
        $query->bindParam(2, $offset, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll();
        return $data;
    }

    /**
     * @param $id
     * @param $type
     * @return array
     */
    public static function getCommentsAndNotesForUser($id, $type)
    {
        list($self, $other, $self_suffix, $other_suffix) = self::selfAndOther($type);
        $query = Database::getPdo()->prepare(
            "SELECT comment_$other_suffix, note_$other_suffix FROM garde WHERE $self = ?"
        );
        $query->bindParam(1, $id, PDO::PARAM_INT);
        $data = $query->fetchAll();
        return $data;
    }

    /**
     * Récupère le taux d'annulation d'un client ainsi que le nombre de gardes permettant le calcul de ce taux.
     *
     * @param int $id
     * @param int $type facultatif mais requête plus performante si précisé.
     * @return array ["taux_annulation" et le "nombre de garde permettant le calcul du taux (dénominateur"].
     */
    public static function getTauxAnnulationForUser($id, $type = null)
    {
        $valide = Garde::DB_VALIDEE;
        $annulee = Garde::DB_ANNULEE;

        if (is_null($type)) {
            $query = Database::getPdo()->prepare(
                "SELECT
              SUM(annuleur = :id) AS n_annul,
              SUM(status IN ($valide, $annulee) AND :id IN (babysitter, famille)) AS n_gardes
            FROM garde
            WHERE (
              (annuleur = :id) OR
              (status IN ($valide, $annulee) AND :id IN (babysitter, famille))
            )"
            );
        } else {
            $user_type = ($type == User::TYPE_BABYSITTER) ? "babysitter" : "famille";
            $query = Database::getPdo()->prepare(
                "SELECT
              SUM(annuleur = :id) AS n_annul,
              SUM(status IN ($valide, $annulee) AND $user_type = :id) AS n_gardes
            FROM garde
            WHERE (
              (annuleur = :id) OR
              (status IN ($valide, $annulee) AND $user_type = :id)
            )"
            );
            $query->bindParam(":type", $type, PDO::PARAM_INT);
        }

        $query->bindParam(":id", $id, PDO::PARAM_INT);

        $query->execute();
        $data = $query->fetch();
        $n_annul = $data['n_annul'];
        $n_accept = $data['n_gardes'];
        $data_res = array(
            "taux_annulation" => ($n_accept != 0) ? $n_annul / $n_accept : 0,
            "n_toget_taux_ann" => $n_accept
        );

        return $data_res;
    }

    /**
     * @return array
     */
    public static function countByMonth()
    {
        $query = Database::getPdo()->prepare(
            "SELECT
              date_creation, type, COUNT(type) AS n
            FROM
              user
            GROUP BY YEAR(date_creation), MONTH(date_creation), type
            ORDER BY date_creation"
        );
        $query->execute();
        $data = $query->fetchAll();
        $dates = [];
        $babysitters = [];
        $familles = [];
        $admins = [];
        foreach ($data as $row) {
            $mois = strftime("%b %y", strtotime($row["date_creation"]));
            $dates[] = $mois;
            $type = $row["type"];
            $n = $row["n"];
            switch ($type) {
                case User::TYPE_ADMIN:
                    $admins[$mois] = $n;
                    break;
                case User::TYPE_FAMILLE:
                    $familles[$mois] = $n;
                    break;
                case User::TYPE_BABYSITTER:
                    $babysitters[$mois] = $n;
                    break;
            }
        }
        $dates = array_values(array_unique($dates));
        return [$dates, $babysitters, $familles, $admins];
    }

    /**
     * @return array
     */
    public static function count()
    {
        $query = Database::getPdo()->prepare(
            "SELECT
              valide, type, COUNT(type) AS n
            FROM
              user
            GROUP BY
              type, valide;"
        );
        $query->execute();
        $data = $query->fetchAll();
        $dataset = [];
        foreach ($data as $row) {
            $status = $row["valide"];
            $type = $row["type"];
            $n = $row["n"];
            switch ($status . $type) {
                case "00": // TODO: utiliser les constantes User::TYPE_*
                    $dataset["admins suspendus"] = $n;
                    break;
                case "10":
                    $dataset["admins"] = $n;
                    break;
                case "01":
                    $dataset["familles suspendues"] = $n;
                    break;
                case "11":
                    $dataset["familles"] = $n;
                    break;
                case "02":
                    $dataset["baby-sitters suspendus"] = $n;
                    break;
                case "12":
                    $dataset["baby-sitters"] = $n;
                    break;
            }
        }
        return $dataset;
    }

    public static function modify($id, $type, $nom, $prenom, $email, $adresse, $code_postal, $ville, $pres, $tel,
                                  $password = null, $multiplicateur = null, $date_naissance = null, $genre = null)
    {
        $query_values = "nom = :nom, prenom = :prenom, email = :email, adresse = :adresse, code_postal = :code_postal,
                       ville = :ville, presentation = :pres, telephone = :tel";
        if (!is_null($password)) {
            $query_values .= ", password = :pass";
        }
        if ($type == User::TYPE_FAMILLE) {
            $query = Database::getPdo()->prepare(
                "UPDATE user 
                SET $query_values
                WHERE id = :id"
            );
        } else if ($type == User::TYPE_BABYSITTER) {
            $query = Database::getPdo()->prepare(
                "UPDATE user 
                SET $query_values, multiplicateur = :mul, date_naissance = :date_naissance, genre_humain = :genre
                WHERE id = :id"
            );
            $query->bindParam("mul", $multiplicateur);
            $query->bindParam("date_naissance", $date_naissance);
            $query->bindParam("genre", $genre);
        }
        if (!is_null($password)) {
            $query->bindParam("pass", password_hash($password, PASSWORD_DEFAULT));
        }
        $query->bindParam("nom", $nom);
        $query->bindParam("prenom", $prenom);
        $query->bindParam("email", $email);
        $query->bindParam("adresse", $adresse);
        $query->bindParam("code_postal", $code_postal);
        $query->bindParam("ville", $ville);
        $query->bindParam("pres", $pres);
        $query->bindParam("tel", $tel);
        $query->bindParam("id", $id, PDO::PARAM_INT);
        $query->execute();
    }

    /**
     * @param $type
     * @return array
     */
    private static function selfAndOther($type)
    {
        $self = ($type == "famille") ? "famille" : $type;
        $other = ($type == "famille") ? "babysitter" : $type;
        $self_suffix = ($type == "famille") ? "f" : "bs";
        $other_suffix = ($type == "famille") ? " bs" : "f";
        return [$self, $other, $self_suffix, $other_suffix];
    }
}