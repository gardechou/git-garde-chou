<?php
/**
 * Définition de la classe GardeService
 *
 * @package Services
 * @tag famille
 * @tag babysitter
 */

/** Gère les interactions avec la BDD liées aux gardes */
class GardeService
{
    public static function modifStatus($id, $status)
    {
        $query = Database::getPdo()->prepare("UPDATE garde SET status = $status WHERE id = $id");
        $query->execute();
    }

    public static function autoriserById($garde_id)
    {
        $query = Database::getPdo()->prepare("UPDATE garde SET status = 1 WHERE id = :garde");
        $query->bindParam(":garde", $garde_id, PDO::PARAM_INT);
        $query->execute();
    }

    public static function existsGardeAccepteeBetween($famille, $babysitter)
    {
        $query = Database::getPdo()->prepare(
            "SELECT id
            FROM garde
            WHERE famille = :f AND babysitter = :bs AND garde.status IN (:ac, :de) AND garde.fin > NOW()");
        $query->bindParam("f", $famille, PDO::PARAM_INT);
        $query->bindParam("bs", $babysitter, PDO::PARAM_INT);
        $status = Garde::DB_ACCEPTEE;
        $query->bindParam("ac", $status, PDO::PARAM_INT);
        $demande = Garde::DB_DEMANDEE;
        $query->bindParam("de", $demande, PDO::PARAM_INT);
        $query->execute();
        return (!empty($query->fetchAll()));
    }

    /**
     * @param $id
     * @param $n
     * @param $page
     * @return array
     */
    public static function getAllForActor($id, $n, $page)
    {
        $query = Database::getPdo()->prepare(
            "SELECT
              garde.*,
            user1.pseudo AS babysitter_pseudo,
            user2.pseudo AS famille_pseudo
            FROM garde
              LEFT JOIN user user1 ON garde.babysitter = user1.id
              LEFT JOIN user user2 ON garde.famille = user2.id
            WHERE garde.famille = ? OR garde.babysitter = ?
            ORDER BY debut DESC LIMIT ? OFFSET ?"
        );
        $query->bindParam(1, $id, PDO::PARAM_INT);
        $query->bindParam(2, $id, PDO::PARAM_INT);
        $query->bindParam(3, $n, PDO::PARAM_INT);
        $offset = ($page - 1) * $n;
        $query->bindParam(4, $offset, PDO::PARAM_INT);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * @param $famille_id
     * @param $babysitter_id
     * @param $date_demande
     * @param $debut
     * @param $fin
     * @param $prix
     * @param $commission
     * @param $nb_enfants
     * @param $lim_annul
     * @param $lim_comment
     * @param $lim_invalid
     * @param $status_id
     */
    public static function newDemande($famille_id, $babysitter_id, $date_demande,
                                      $debut, $fin, $prix, $commission, $nb_enfants, $lim_annul,
                                      $lim_comment, $lim_invalid, $status_id)
    {
        $query = Database::getPdo()->prepare(
            "INSERT INTO garde (famille, babysitter, date_demande, debut, fin, status, prix, commission, nb_enfants,
                                lim_annulation, lim_comment, lim_invalid)
             VALUES (:famille, :babysitter, :date_demande, :debut, :fin, :status_id, :prix, :commission, :nb_enfants,
                     :lim_annulation, :lim_comment, :lim_invalid)");
        $query->bindParam(":famille", $famille_id);
        $query->bindParam(":babysitter", $babysitter_id);
        $query->bindParam(":date_demande", $date_demande);
        $query->bindParam(":debut", $debut);
        $query->bindParam(":fin", $fin);
        $query->bindParam(":prix", $prix);
        $query->bindParam(":commission", $commission);
        $query->bindParam(":nb_enfants", $nb_enfants);
        $query->bindParam(":lim_annulation", $lim_annul);
        $query->bindParam(":lim_comment", $lim_comment);
        $query->bindParam(":lim_invalid", $lim_invalid);
        $query->bindParam(":status_id", $status_id);
        $query->execute();
    }

    /**
     * @param $id
     * @return array
     */
    public static function getActiveFromFamilleId($id)
    {
        $query = Database::getPdo()->prepare(
            "SELECT garde.*, user.pseudo AS babysitter_pseudo
             FROM garde JOIN user ON garde.babysitter = user.id
             WHERE famille = :famille AND 
             (lim_comment > NOW() OR lim_invalid > NOW() OR garde.lim_comment > NOW()) AND
             status NOT IN (5, 6, 7)
             ORDER BY debut;"
        );
        $query->bindParam(":famille", $id, PDO::PARAM_INT);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * @param $id
     * @return array
     */
    public static function getActiveFromBabysitterId($id)
    {
        $query = Database::getPdo()->prepare(
            "SELECT garde.*, user.pseudo AS famille_pseudo
             FROM garde JOIN user ON garde.famille = user.id
             WHERE babysitter = :babysitter AND 
             (lim_comment > NOW() OR lim_invalid > NOW() OR garde.lim_annulation > NOW()) AND
             garde.status NOT IN (5, 6, 7)
             ORDER BY debut"
        );
        $query->bindParam(":babysitter", $id, PDO::PARAM_INT);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getDataFromId($id)
    {
        $query = Database::getPdo()->prepare("SELECT * FROM garde WHERE id = ?");
        $query->bindParam(1, $id, PDO::PARAM_INT);
        $query->execute();
        return $query->fetch();
    }

    /**
     * @param $user_id
     * @param $garde_id
     * @return bool
     */
    public static function isActor($user_id, $garde_id)
    {
        $query = Database::getPdo()->prepare("SELECT famille, babysitter FROM garde WHERE id = ?");
        $query->bindParam(1, $garde_id, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetch();
        return ($user_id == $data["famille"] or $user_id == $data["babysitter"]);
    }

    /**
     * @param $garde_id
     * @param $actor_id
     */
    public static function annulerById($garde_id, $actor_id)
    {
        $query = Database::getPdo()->prepare("UPDATE garde SET status = 6, annuleur = :annuleur WHERE id = :garde");
        $query->bindParam(":garde", $garde_id, PDO::PARAM_INT);
        $query->bindParam(":annuleur", $actor_id, PDO::PARAM_INT);
        $query->execute();
    }

    /**
     * @param $garde_id
     */
    public static function avorterById($garde_id)
    {
        $query = Database::getPdo()->prepare("UPDATE garde SET status = 7 WHERE id = :garde");
        $query->bindParam(":garde", $garde_id, PDO::PARAM_INT);
        $query->execute();
    }

    /**
     * @param $garde_id
     */
    public static function refuserById($garde_id)
    {
        $query = Database::getPdo()->prepare("UPDATE garde SET status = 5 WHERE id = :garde");
        $query->bindParam(":garde", $garde_id, PDO::PARAM_INT);
        $query->execute();
    }

    /**
     * @param $garde_id
     */
    public static function accepterById($garde_id)
    {
        $query = Database::getPdo()->prepare("UPDATE garde SET status = 2 WHERE id = :garde");
        $query->bindParam(":garde", $garde_id, PDO::PARAM_INT);
        $query->execute();
    }

    /**
     * @param $garde_id
     */
    public static function validerById($garde_id)
    {
        $query = Database::getPdo()->prepare("UPDATE garde SET status = 3 WHERE id = :garde");
        $query->bindParam(":garde", $garde_id, PDO::PARAM_INT);
        $query->execute();
    }

    /**
     * @param $garde_id
     */
    public static function invaliderById($garde_id)
    {
        $query = Database::getPdo()->prepare("UPDATE garde SET status = 4 WHERE id = :garde");
        $query->bindParam(":garde", $garde_id, PDO::PARAM_INT);
        $query->execute();
    }


    public static function getBenefs()
    {
        $query = Database::getPdo()->prepare(
            "SELECT
            lim_invalid AS date, commission
            FROM garde WHERE status = 3 GROUP BY YEAR(lim_invalid), MONTH(lim_invalid)");
        $query->execute();
        $data = $query->fetchAll();
        $stats = [];
        foreach ($data as $row) {
            $stats[strftime("%b %y", strtotime($row["date"]))] = $row['commission'];
        }
        return $stats;
    }

    public static function listAll($n, $page, $sort_by)
    {
        $query = Database::getPdo()->prepare(
            "SELECT garde.*, babysitter.pseudo AS babysitter_pseudo, famille.pseudo AS famille_pseudo
             FROM garde
             JOIN user babysitter ON garde.babysitter = babysitter.id
             JOIN user famille ON garde.famille = famille.id
             ORDER BY $sort_by DESC LIMIT ? OFFSET ?");
        $query->bindParam(1, $n, PDO::PARAM_INT);
        $offset = ($page - 1) * $n;
        $query->bindParam(2, $offset, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll();
        return $data;
    }

    public static function listAttenteAdmin()
    {
        $attente_admin = Garde::DB_ATTENTE;
        $query = Database::getPdo()->prepare(
            "SELECT garde.*, babysitter.pseudo AS babysitter_pseudo, famille.pseudo AS famille_pseudo
             FROM garde
             JOIN user babysitter ON garde.babysitter = babysitter.id
             JOIN user famille ON garde.famille = famille.id
             WHERE status = $attente_admin
             ORDER BY garde.debut DESC");
        $query->bindParam(1, $n, $attente_admin);
        $query->execute();
        $data = $query->fetchAll();
        return $data;
    }
}
