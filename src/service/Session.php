<?php
/**
 * Définition de la classe Session
 *
 * @package Services
 */

/**
 * Cette classe centralise les interactions avec la variable superglobale $_SESSION
 *
 * Elle permet:
 * - d'éviter de ré-interroger la BDD en permanence.
 * - d'unifier la manière dont on interroge l'état de la session
 *
 */
class Session
{
    /**
     * Fait en sorte que l'utilisateur soit connecté et stocke les informations qui seront réutilisées tout au long de
     * sa session dans $_SESSION
     *
     * A appeler après avoir été correctement authentifié.
     *
     * @param string $pseudo
     */
    public static function setCurrentUserFromPseudo($pseudo)
    {
        // TODO: optimiser la requête ici aux infos strictement nécessaires.
        $data = UserService::getDataFromPseudo($pseudo);
        $_SESSION['pseudo'] = $data['pseudo'];
        $_SESSION['solde'] = $data['solde'];
        $_SESSION['id'] = $data['id'];
        $_SESSION['nom'] = $data['nom'];
        $_SESSION['prenom'] = $data['prenom']; //TODO: virer ce type en string et refactor tout le projet en conséquence
        switch ($data['type']) {
            case '0':
                $_SESSION['type'] = 'admin';
                break;
            case '1':
                $_SESSION['type'] = 'famille';
                $_SESSION['code_postal'] = $data['code_postal'];
                break;
            case '2':
                $_SESSION['type'] = 'babysitter';
                break;
        }
        $_SESSION['type_int'] = $data['type'];
        $_SESSION['logged'] = true;
    }

    public static function getIntType()
    {
        return $_SESSION["type_int"];
    }

    /** @return $string Le type d'utilisateur connecté: "admin", "famille" ou "babysitter" */
    public static function getType()
    {
        return $_SESSION['type'];
    }

    /**
     * L'utilisateur est-il connecté ou non?
     *
     * @return bool
     */
    public static function isLoggedIn()
    {
        return (isset($_SESSION['logged']) and $_SESSION['logged'] == true);
    }

    /**
     * @return bool
     */
    public static function isAdmin()
    {
        return (self::isLoggedIn() and self::getType() == "admin");
    }

    /**
     * @return bool
     */
    public static function isFamille()
    {
        return (self::isLoggedIn() and self::getType() == "famille");
    }

    /**
     * @return bool
     */
    public static function isBabysitter()
    {
        return (self::isLoggedIn() and self::getType() == "babysitter");
    }

    /** @return string Le pseudo de l'utilisateur connecté. */
    public static function getCurrentUserPseudo()
    {
        $res = null;
        if (self::isLoggedIn()) {
            $res = $_SESSION['pseudo'];
        }
        return $res;
    }
    /** @return int L'id de l'utilisateur connecté. */
    public static function getCurrentUserId() //TODO : Remplacer tout les $_SESSION["id"] par vette méthode
    {
        $res = null;
        if (self::isLoggedIn()) {
            $res = $_SESSION['id'];
        }
        return $res;
    }

    /** Détruit la session en cours. */
    public static function destroy()
    {
        session_destroy();
        $_SESSION = null;
    }

    public static function updateSolde()
    {
        $_SESSION['solde'] = UserService::getDataFromId($_SESSION['id'])['solde'];
    }
}