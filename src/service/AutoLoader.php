<?php
/**
 * Définition de la classe Autoloader
 */

/**
 * Définition des règles de chargement automatiques de classes.
 *
 * - Si une classe finit par Service ou est dans la liste des services spéciaux,
 * c'est à dire "Database", "Page", "Session", "Settings" ou "FormChecker",
 * sa définition se trouve dans "../service/".
 * - Si une classe finit par Controller, elle se trouve dans "../controller/".
 * Si elle concerne un type spécifique d'utilisateur défini, par exemple AdminController,
 * on ne peut la charger que si on est connecté en tant que ce type d'utilisateur.
 * Ceci est vérifié avec le service spécial Session
 * - Dans les autres cas, la classe doit se trouver dans "../model/".
 *
 * @package Services
 */
class AutoLoader
{
    /**
     * Vérifie si une string finit par une autre string
     *
     * @param $haystack string
     * @param $needle string
     * @return bool
     */
    static function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    /**
     * Vérifie si une string commence par une autre string
     *
     * @param $haystack string
     * @param $needle string
     * @return bool
     */
    static function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    /**
     * La fonction à appeler pour que le chargement automatique des classes fonctionne.
     */
    public static function register()
    {
        spl_autoload_register(function ($class) {
            $special_services = ["Database", "Page", "Session", "Settings", "FormChecker", "Upload"];
            if (in_array($class, $special_services) or self::endsWith($class, "Service")) {
                include "../service/" . $class . ".php";
            } elseif (self::endsWith($class, "Controller")) {
                $users = ["Admin", "Famille", "Babysitter"];
                $ok = true;
                foreach ($users as $user) {
                    if (self::startsWith($class, $user)) {
                        $check_function = "is" . $user;
                        if (!Session::$check_function()) {
                            $ok = false;
                        }
                    }
                }
                $logged_user_controllers = ["SoldeController", "GardeController", "CommentairesController"];
                if (in_array($class, $logged_user_controllers) and !Session::isLoggedIn()) {
                    $ok = false;
                }

                $babysitter_restricted_controllers = ["DispoController"];
                if (in_array($class, $babysitter_restricted_controllers) and !Session::isBabysitter()) {
                    $ok = false;
                }

                if ($ok) {
                    include "../controller/" . $class . ".php";
                } else {
                    DefaultController::homeAction(); // on renvoie vers l'accueil par défaut
                }
            } else {
                include "../model/" . $class . ".php";
            }
        }
        );
    }
}