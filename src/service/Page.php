<?php
/**
 * Définition de la classe Page
 *
 * @package Services
 */

/**
 * Gestion du rendu final de la page.
 *
 * C'est avec cette classe qu'on peut définir:
 * - le titre de la page
 * - les CSS utilisées
 * - les variables qui seront utilisées pour le rendu
 * - éventuellement, le message d'erreur à afficher
 *
 * La méthode Page::render() est appelée à la fin de ../web/index.php pour réellement afficher la page.
 *
 */
class Page
{
    /**
     * @var string[] |null $body_URI L'URI du corps de la page, relativement à ../view/
     * @var string[] $errors Les messages d'erreur ou avertissements affichés sur la page
     * @var string[] $infos les messages d'information affichés sur la page
     */
    private static $body_URIs = array();
    /**
     * @var array
     */
    private static $errors = array();
    /**
     * @var array
     */
    private static $infos = array();

    private static $warnings = array();

    /** @var string $title Le titre de la page qui sera affichée dans le navigateur de l'utilisateur. */
    static $title = "Garde chou";

    /** @var string[] La liste des feuilles de style utilisées. On définit ici celles communes à toutes les vues. */
    private static $css = array(
        URI_PREFIX . "/bootstrap/css/bootstrap.min.css",
        URI_PREFIX . "/bootstrap/css/bootstrap-theme.min.css",
        URI_PREFIX . "/style.css");

    /** @var mixed[] On centralise dans la tableau $vars l'ensemble des données qui seront utilisées par la vue. */
    private static $vars = array();

    /**
     * @var string
     */
    private static $navbarActive = "home";

    /**
     * Obtenir les messages d'informations
     * @return string
     */
    public static function getInfomsg()
    {
        return join("<br/>", self::$infos);
    }

    /**
     * Obtenir les messages d'informations
     * @return string
     */
    public static function getWarnings()
    {
        return join("<br/>", self::$warnings);
    }

    /**
     * Ajoute un message d'information
     * @param string $infomsg
     */
    public static function addInfo($infomsg)
    {
        self::$infos[] = $infomsg;
    }

    /**
     * Ajoute un message d'alerte
     * @param string $msg
     */
    public static function addWarning($msg)
    {
        self::$warnings[] = $msg;
    }

    /** Ajouter une variable que la vue aura besoin de consulter.
     * @param string $name
     * @param mixed $value
     */
    public static function add($name, $value)
    {
        self::$vars[$name] = $value;
    }

    /**
     * Retourner une variable que la vue aura besoin de consulter.
     *
     * Le contenu est transformé en simple string grace à htmlentities.
     *
     * @param string $name La clé de la variable à récupérer
     * @return string
     */
    public static function get($name)
    {
        if (isset(self::$vars[$name])) {
            return self::$vars[$name];
        } else {
            return null;
        }
    }

    /**
     * La variable de page a-t-elle été définie précédemment?
     *
     * @param string $name
     * @return bool
     */
    public static function isDefined($name)
    {
        return isset(self::$vars[$name]);
    }

    /**
     * Obtenir la listes des feuilles de style utilisées pour générer le <head> du document html
     * Destiné à être appelé par ../view/html_header.php.
     *
     * @return string[]
     *
     */
    public static function getCSS()
    {
        return self::$css;
    }

    /**
     * Ajouter une CSS à la page HTML qui sera générée par la fonction render.
     *
     * @param string $css_uri L'URI du CSS à utiliser pour la page
     */
    public static function addCSS($css_uri)
    {
        self::$css[] = $css_uri;
    }

    /**
     * Ajouter un message d'erreur à la page qui sera affiché.
     *
     * @param string $error Le message d'erreur à afficher
     */
    public static function addError($error)
    {
        self::$errors[] = $error;
    }

    /**
     * Définit le titre de la page.
     *
     * @param string $title Le titre de la page HTML qui sera affichée dans le navigateur de l'utilisateur.
     */
    public static function setTitle($title)
    {
        self::$title = $title;
    }

    /**
     * Ajoute un suffixe au titre actuel
     *
     * @param string $title_suffix Le suffixe à ajouter au titre
     * @param string $sep Le séparateur
     */
    public static function addToTitle($title_suffix, $sep = " - ")
    {
        self::$title = self::$title . $sep . $title_suffix;
    }

    /**
     * Obtenir le titre de la page (utilisée par ../view/html_header.php)
     *
     * @return string
     */
    public static function getTitle()
    {
        return self::$title;
    }

    /**
     * définir l'URI de la view du corps de la page, situé dans le répertoire ../view/
     *
     * @param string $view
     */
    public static function addView($view)
    {
        self::$body_URIs[] = "../view/" . $view;
    }

    /**
     * Obtenir le message d'erreur (utilisé par ../view/error.php)
     *
     * @return string
     */
    public static function getErrormsg()
    {
        return join("<br/>\n<strong>Problème&nbsp;! </strong>", self::$errors);
    }

    /** fonction appelée pour générer la page. */
    public static function render()
    {
        extract(self::$vars);
        self::header();
        // afficher la partie "message d'erreur" si un message d'erreur a été défini.
        if (!empty(self::$errors)) {
            self::error();
        }
        // afficher la partie "message d'info" si un message d'erreur a été défini.
        if (!empty(self::$infos)) {
            self::info();
        }
        if (!empty(self::$warnings)) {
            self::warning();
        }

        // on affiche le corps de la page
        foreach (self::$body_URIs as $URI) {
            include $URI;
        }
        self::footer();
    }

    /** génère l'en-tête de la page */
    private static function header()
    {
        include "../view/html_header.php";
        include "../view/header.php";
    }

    /** génère le pied de page */
    private static function footer()
    {
        include "../view/footer.php";
    }

    /** génère le message d'erreur */
    private static function error()
    {
        include "../view/error.php";
    }

    /** génére le message d'information */
    private static function info()
    {
        include "../view/info.php";
    }

    /** génére le message d'alerte */
    private static function warning()
    {
        include "../view/warning.php";
    }

    /**
     * @param $item
     */
    public static function echoActiveNavbar($item)
    {
        if ($item == self::$navbarActive) {
            echo 'class="active"';
        }
    }

    /**
     * @param string $navbarActive
     */
    public static function setNavbar($navbarActive)
    {
        self::$navbarActive = $navbarActive;
    }

    /**
     * @param $array
     */
    public static function addArray($array)
    {
        foreach ($array as $key => $value) {
            self::add($key, $value);
        }
    }

    /**
     *
     */
    private static function error_and_info_in_session()
    {
        if (!empty(self::$errors)) {
            $_SESSION['errors'] = self::$errors;
        }
        if (!empty(self::$infos)) {
            $_SESSION['infos'] = self::$infos;
        }
    }

    /**
     *
     */
    public static function goBack()
    {
        self::error_and_info_in_session();
        if (isset($_SERVER['HTTP_REFERER'])) {
            header("Location:" . $_SERVER['HTTP_REFERER']);
            exit();
        } else {
            self::goHome();
        }
    }

    /**
     *
     */
    public static function goHome()
    {
        self::error_and_info_in_session();
        header("Location:" . URI_PREFIX);
        exit();
    }

    public static function go($uri) {
        self::error_and_info_in_session();
        header("Location:" . URI_PREFIX . $uri);
        exit();
    }

    /**
     * @param array $errors
     */
    public static function setErrors($errors)
    {
        self::$errors = $errors;
    }

    /**
     * @param array $infos
     */
    public static function setInfos($infos)
    {
        self::$infos = $infos;
    }

    /**
     * @param array $infos
     */
    public static function addInfos($infos)
    {
        self::$infos = array_merge(self::$infos, $infos);
    }

    /**
     * @param array $errors
     */
    public static function addErrors($errors)
    {
        self::$errors = array_merge(self::$errors, $errors);
    }
}