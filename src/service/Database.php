<?php
/**
 * Définition de la classe Database
 *
 * @package Services
 */

/**
 * Ce service s'assure que la connection à la base de données ne se fait qu'une seule fois par execution de index.php
 *
 * Pour récupérer le {@link http://php.net/manual/en/class.pdo.php PDO}, on utilise sa méthode statique getPdo().
 */
class Database
{
    /** @var Database|null L'instance du singleton */
    private static $instance = null;
    /** @var PDO L'objet PDO accessible par getPdo() */
    private $pdo;

    /**
     * Fonction principale de cette classe.
     *
     * C'est en récupérant l'object {@link http://php.net/manual/en/class.pdo.php PDO} que l'on communique avec la BDD.
     *
     * @return PDO
     */
    public static function getPdo()
    {
        $instance = self::getInstance();
        return $instance->pdo;
    }

    /**
     * Retourne l'unique instance de Database, en la créant si nécessaire.
     *
     * @return Database
     */
    private static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /** On crée l'objet de communication avec la DB à la création de l'instance. */
    private function __construct()
    {
        $this->pdo = new PDO(
            "mysql:host=". DB_HOST . ";dbname=" . DB_NAME . ";charset=utf8",
            DB_USER, DB_PASSWORD
        );
        // Mode par défaut de retour des valeurs par PDO = structured array.
        $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    }

    /**
     * Convertit un timestamp en string que MySQL comprend
     *
     * @param int
     * @return string
     */
    public static function timestampToSQLString($timestamp) {
        return date("Y-m-d H:i:s", $timestamp);
    }
}