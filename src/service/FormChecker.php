<?php
/**
 * Définition de la classe FormChecker
 *
 * @package Services
 */

/**
 * Permet de vérifier que les données saisies par l'utilisateur dans un formulaire sont conformes à ce qui est attendu.
 *
 * Permet de générer les messages d'erreurs appropriés en utilisant Page::setErrorMsg
 * Permet de pré-remplir le formulaire avec les champs qui étaient valides en cas d'erreur sur un champ.
 *
 */
class FormChecker
{
    /** @var string[] La liste des noms des champs comme trouvés dans POST */
    private $fields = array();
    /** @var string[] La liste des noms complets (en français correct) des champs */
    private $names = array();
    /** @var string[] La liste des contraintes sous forme d'expression régulières */
    private $constraints = array();
    /** @var int[] La listes des longueurs maximales des champs */
    private $max_lengths = array();
    /** @var int[] La listes des longueurs minimales des champs */
    private $min_lengths = array();
    /** @var bool[] Une liste de bool indiquant si on doit pré-remplir le champ avant la valeur précédente. */
    private $pre_fills = array();
    /** @var bool[] Une liste de bool indiquant si le champ est optionnel. */
    private $optionals = array();
    /** @var (string|null)[] La valeur des champs */
    private $values = array();
    /** @var (string|null)[] Un message d'erreur spécifique pour chaque contenu */
    private $errormsgs = array();
    /** @var int Le nombre de champs */
    private $n = 0;
    /** @var array() */
    private $method;
    private $errors = array();
    private $valid_fields = array();

    /**
     * @return array
     */
    public function getValidFields()
    {
        return $this->valid_fields;
    }
    
    public function __construct($method)
    {
        $this->method = $method;
    }


    /**
     * Ajoute un champ à évaluer et se contraintes.
     *
     * @param string $field_name Le nom POST du champ.
     * @param string $full_name Le nom "français" du champ.
     * @param string|null $regexp Le type de validation du contenu du champ: "date", "email", "time", "float" ou une regexp. null ou pas de valeur accepte n'importe quelle valeur.
     * @param int $max La taille maximale du champ.
     * @param int $min La taille minimale du champ.
     * @param bool $pre_fill Doit-on pré-remplir si le champ s'il était correct?
     * @param string $errormsg Un message d'erreur spécifique si le champ n'est pas valide
     * @param bool $optional Le champ est-il facultatif?
     */
    public function addField($field_name, $full_name, $regexp = "/.*/", $min = 0, $max = INF, $errormsg = null, $pre_fill = true,
                             $optional = false, $clean_post = false)
    {
        $this->fields[] = $field_name;
        $this->names[] = $full_name;
        if ($regexp) {
            $this->constraints[] = $regexp;
        } else {
            $this->constraints[] = "/.*/";
        }
        $this->max_lengths[] = $max;
        $this->min_lengths[] = $min;
        $this->pre_fills[] = $pre_fill;
        $this->errormsgs[] = $errormsg;
        $this->optionals[] = $optional;
        if (isset($this->method[$field_name])) {
            $this->values[] = $this->method[$field_name];
        } else {
            $this->values[] = null;
        }
        $this->n++;
    }

    /**
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Vérifie la conformité des valeurs passées par POST.
     *
     * Renvoie true si tout est conforme.
     * Renvoie false sinon et met à jour Page avec les valeurs pré-remplies des champs pour que l'utilisateur
     * n'ait pas à tout retaper.
     *
     * Les messages d'erreurs peuvent être récupérés via getErrors()
     *
     * @return bool
     */
    public function parse() //TODO: ne pas ajouter les champs valides directement depuis ici (no side effects!)
    {
        $success = true;
        $missing_fields = $this->getMissingPostFields();
        if (!empty($missing_fields)) {
            if (count($missing_fields) == 1) {
                $this->errors[] = "Veuillez remplir le champ: " . $missing_fields[0];
            } else {
                $frenchList = self::getFrenchList($missing_fields);
                $this->errors[] = "Veuillez remplir les champs: " . $frenchList;
            }
            $success = false;
        }
        $invalid_length_fields = $this->getIdWithInvalidLength();
        if (!empty($invalid_length_fields)) {
            foreach ($invalid_length_fields as $i) {
                array_push($this->errors, "Le champ " . $this->names[$i] . " doit avoir une longueur comprise entre " .
                    $this->min_lengths[$i] . " et " . $this->max_lengths[$i]);
            }
            $success = false;
        }
        $invalid_fields = $this->getIdWithInvalidContentAndUpdateFieldIsNecessary();
        if (!empty($invalid_fields)) {
            foreach ($invalid_fields as $i) {
                if ($this->errormsgs[$i]) {
                    $this->errors[] = $this->errormsgs[$i];
                } else {
                    array_push($this->errors,
                        htmlspecialchars($this->values[$i]) . " n'est pas une valeur valide de " . $this->names[$i]);
                }
            }
            $success = false;
        }

        if (!$success) {
            // Remettre les valeurs qui étaient correctes avec Page::add pour ne pas avoir à les re-remplir
            $valid_fields = array_diff(range(0, $this->n - 1), $invalid_length_fields, $invalid_fields);
            foreach ($valid_fields as $i) {
                $name = $this->fields[$i];
                if (isset($this->method[$i])) {
                    $this->valid_fields[$name] = $this->method[$i];
                }
            }
        }
        return $success;
    }

    /**
     * Vérifie que les valeurs entrées par l'utilisateur ont la bonne longueur.
     *
     * @return int[]
     */
    private function getIdWithInvalidLength()
    {
        $res = array();
        for ($i = 0; $i < $this->n; $i++) {
            if (!$this->values[$i]) {
                continue;
            }
            $length = strlen($this->values[$i]);
            if ($length > $this->max_lengths[$i] or $length < $this->min_lengths[$i]) {
                $res[] = $i;
            }
        }
        return $res;
    }

    /**
     * Vérifie que les valeurs entrées par l'utilisateur correspondent à la vérification choisie à l'ajout du champ.
     *
     * @return int[]
     */
    private function getIdWithInvalidContentAndUpdateFieldIsNecessary()
    {
        $res = array();
        for ($i = 0; $i < $this->n; $i++) {
            if (!$this->values[$i]) {
                continue;
            }
            switch ($this->constraints[$i]) {
                case "email":
                    $valid = filter_var($this->values[$i], FILTER_VALIDATE_EMAIL);
                    break;
                case "date":
                    $date = explode("-", $this->values[$i]);
                    if (count($date) == 3) {
                        $valid = checkdate($date[1], $date[2], $date[0]);
                    } else {
                        $valid = false;
                    }
                    break;
                case "time":
                    $valid = preg_match("/(2[0-3]|[01][0-9]):([0-5][0-9])/", $this->values[$i]);
                    break;
                case "zipcode":
                    $valid = preg_match("/^\d{5}$/", $this->values[$i]);
                    break;
                case "confirm":
                    $valid = ($this->values[$i] == $this->values[$i - 1]);
                    break;
                case "float":
                    // On convertit la virgule en point si nécessaire
                    $this->values[$i] = str_replace(",", ".", $this->values[$i]);
                    $valid = preg_match("/^\d*\.*\d*$/", $this->values[$i]);
                    break;
                default:
                    $valid = preg_match($this->constraints[$i], $this->values[$i]);
                    break;
            }
            if (!$valid) {
                $res[] = $i;
            }
        }
        return $res;
    }

    /**
     * Vérifie que tous les champs nécessaires sont bien présents dans le POST.
     *
     * Retourne la liste des champs non remplis.
     *
     * @return string[] La liste des nom des champs manquants.
     */
    private function getMissingPostFields()
    {
        $res = array();
        for ($i = 0; $i < $this->n; $i++) {
            if (!strlen($this->values[$i]) and !$this->optionals[$i]) {
                $res[] = $this->names[$i];
            }
        }
        return $res;
    }

    /**
     * Convertit un array en liste lisible en français.
     *
     * Exemple: array("a","b","c") => "a, b et c."
     *
     * @param string[] $list La liste de string à convertir.
     * @return string La liste lisible en français.
     */
    public static function getFrenchList($list)
    {
        $n = count($list);
        $res = $list[0];
        foreach (array_slice($list, 1, $n - 2) as $el) {
            $res .= ", " . $el;
        }
        if ($n > 1) {
            $res .= " et " . $list[$n - 1];
        }
        return $res;
    }

    public function getStructuredArray()
    {
        $res = array();
        for ($i = 0; $i < $this->n; $i++) {
            $res[$this->fields[$i]] = $this->values[$i];
        }
        return $res;
    }

    public function parseAndGoBackIfError() {
        $success = $this->parse();
        if (!$success) {
            Page::addError($this->errors);
            Page::goBack();
        }
    }
}