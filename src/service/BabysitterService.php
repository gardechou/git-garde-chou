<?php
/**
 * Définition de la classe BabysitterService
 *
 * @package Services
 */

/**
 * Le service qui gère les interactions avec la base de donnée en relation avec les babysitters.
 */
class BabysitterService
{
    /**
     * Interroge la BDD sur les babysitters disponibles pour un code postal donné.
     *
     * @param string $code
     * @return array[] Un array d'arrays structurés représentant avec les champs de la BDD comme clé
     * et la valeur comme valeur.
     */
    public static function getDataFromDispoGeo($code)
    {
        $query = Database::getPdo()->prepare(
            "SELECT pseudo, presentation, pic
            FROM dispo_geo JOIN user ON dispo_geo.babysitter = user.id
            WHERE dispo_geo.code_postal = ? AND valide = 1");
        $query->execute(array($code));
        $data = $query->fetchAll();
        return $data;
    }

    /**
     * Détermine si un baby-sitter a un compte suspendu/pas encore validé
     *
     * @param $pseudo
     * @return bool
     */
    public static function isValide($pseudo)
    {
        $query = Database::getPdo()->prepare("SELECT valide FROM user WHERE pseudo = ?");
        $query->execute([$pseudo]);
        return ($query->fetch()['valide'] == "1");
    }

    /**
     * Interroge la BDD sur les babysitters disponibles sur un code postal et des horaires donnés.
     *
     * @param string $code_postal
     * @param string $debut Le début du travail au format "YYYY-MM-DD HH:MM"
     * @param string $fin La fin du travail au format "YYYY-MM-DD HH:MM"
     * @return array[] Un array d'arrays structurés représentant avec les champs de la BDD comme clé
     *                 et la valeur comme valeur.
     */
    public static function getDataFromDispo($code_postal, $debut, $fin)
    {
        $query = Database::getPdo()->prepare("
            SELECT taux_horaire, pseudo, multiplicateur, presentation, date_creation, user.id AS id
            FROM dispo JOIN dispo_geo ON dispo.babysitter = dispo_geo.babysitter
                       JOIN user ON dispo_geo.babysitter = user.id
            WHERE dispo_geo.code_postal = ? AND ? >= debut AND ? <= fin AND valide = 1");
        $query->bindParam(1, $code_postal);
        $query->bindParam(2, $debut);
        $query->bindParam(3, $fin);
        $query->execute();
        $data = $query->fetchAll();
        return $data;
    }

    /**
     * Interroge la BDD sur les comptes baby-sitters pas validés par l'adrmin.
     *
     * @return array[] Un array d'arrays structurés
     */
    public static function getInvalides()
    {
        $query = Database::getPdo()->prepare("SELECT * FROM user WHERE valide = 0");
        $query->execute();
        $data = $query->fetchAll();
        return $data;
    }
}