<?php
/**
 * Définition de EmailService
 *
 * @package Services
 */

/**
 * Centralise la procédure pour envoyer des emails
 */
class EmailService
{
    /**
     * Envoyer au babysitter le mail pour l'informer qu'il a reçu une demande de garde.
     *
     * @param string $babysitter_pseudo
     * @param string $babysitter_email
     * @param string $famille_id
     * @param string $date Date de début de la garde
     * @param string $montant Le prix que le babysitter va recevoir pour la garde
     * @param string $famille
     */
    public static function demande($babysitter_pseudo, $babysitter_email, $famille_id, $date, $montant, $famille)
    {
        $subject = "Garde-chou: nouvelle demande";
        $date = strftime(DATE_FORMAT, $date);
        $body =
            "Bonjour $babysitter_pseudo," . PHP_EOL . PHP_EOL .
            "Vous avez reçu une nouvelle demande de garde pour le $date pour un montant de $montant euros." . PHP_EOL .
            "Elle émane de $famille: " . DOMAIN . URI_PREFIX . "/user/profile?id=$famille_id" . PHP_EOL .
            "Rendez-vous sur " . DOMAIN . URI_PREFIX . " pour l'accepter." . PHP_EOL . PHP_EOL .
            "-- " . PHP_EOL . "L'équipe garde-chou" . PHP_EOL;
        self::sendMail($babysitter_email, $subject, $body);
    }

    /**
     * La fonction d'envoi de mail à proprement parler
     *
     * @param $email
     * @param $subject
     * @param $body
     */
    private static function sendMail($email, $subject, $body)
    {
        $headers = "From: \"Garde-chou\" <" . GARDECHOU_EMAIL . ">\r\n";
        $headers .= "Reply-To: \"Garde-chou\" <". GARDECHOU_EMAIL . ">\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/plain; charset=UTF-8\r\n";
        mail($email, $subject, $body, $headers);
    }
}