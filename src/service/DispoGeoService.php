<?php
/**
 * Définition de la classe DispoGeoService
 *
 * @package Services
 * @tag Baby-sitter
 */

/**
 * Ce service gère les interactions avec la BDD en rapport avec les dispos geo des babysitters
 */
class DispoGeoService
{
    /**
     * Retourne la liste des disponibilités géographiques (=codes postaux) d'un babysitter donné
     *
     * @param int $id L'id du babysitter dans la BDD (table user)
     * @return array La liste des codes postaux
     */
    public static function getFromBabysitterId($id)
    {
        $query = Database::getPdo()->prepare("SELECT code_postal FROM dispo_geo WHERE babysitter = ? ORDER BY code_postal");
        $query->bindParam(1, $id, PDO::PARAM_INT);
        $query->execute();
        $data = $query->fetchAll();
        $dispos_geo = array();
        foreach ($data as $row) {
            $dispos_geo[] = $row["code_postal"];
        }
        return $dispos_geo;
    }

    /**
     * @param int $babysitter
     * @param string $code_postal
     */
    public static function add($babysitter, $code_postal)
    {
        $query = Database::getPdo()->prepare("INSERT INTO dispo_geo (babysitter, code_postal) VALUES (?, ?)");
        $query->bindParam(1, $babysitter, PDO::PARAM_INT);
        $query->bindParam(2, $code_postal);
        $query->execute();
    }

    /**
     * @param int $babysitter
     * @param string $code_postal
     */
    public static function remove($babysitter, $code_postal)
    {
        $query = Database::getPdo()->prepare("DELETE FROM dispo_geo WHERE babysitter = ? AND code_postal = ?");
        $query->bindParam(1, $babysitter, PDO::PARAM_INT);
        $query->bindParam(2, $code_postal);
        $query->execute();
    }

    /**
     * @param int $babysitter
     * @param string $code_postal
     * @return bool
     */
    public static function isPresent($babysitter, $code_postal)
    {
        $query = Database::getPdo()->prepare("SELECT code_postal FROM dispo_geo WHERE babysitter = ? AND code_postal = ?");
        $query->bindParam(1, $babysitter, PDO::PARAM_INT);
        $query->bindParam(2, $code_postal);
        $query->execute();
        return !empty($query->fetch());
    }
}