<?php
/**
 * Définition de la classe Upload
 *
 * @package Services
 */

/**
 * Le service permettant d'uploader des fichiers sur le site.
 */
class Upload
{
    /**
     * Envoyer une image sur le site par POST.
     *
     * @param string $filename le nom du fichier qui a été uploadé tel que défini dans $_FILES
     * @return null|string renvoie null si l'upload
     */
    static public function picture($filename)
    {
        $target_dir = "../web/images/userpics/";
        $file = $_FILES[$filename];
        $uploadOk = 1;
        $imageFileType = pathinfo($file["name"], PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if (isset($_POST["submit"])) {
            $check = getimagesize($file["tmp_name"]);
            if ($check !== false) {
                $uploadOk = 1;
            } else {
                Page::addError("Le fichier n'est pas une image.");
                $uploadOk = 0;
            }
        }
        // Randomiser le nom de fichier jusqu'à ce que le nom de fichier ne soit pas déjà présent
        do {
            $random_name = str_shuffle(md5($file["name"])) . $file["name"];
            $target_file = $target_dir . $random_name;
        } while (file_exists($target_file));

        // Check file size
        if ($file["size"] > 500000) {
            Page::addError("L'image est trop grande.");
            $uploadOk = 0;
        }
        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif"
        ) {
            Page::addError("Seuls les fichiers JPG, JPEG, PNG & GIF sont autorisés.");
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            Page::addError("Un problème est survenu pendant l'envoi de l'image.");
        // if everything is ok, try to upload file
        } else {
            if (!move_uploaded_file($file["tmp_name"], $target_file)) {
                Page::addError("Un problème est survenu pendant l'envoi de l'image.");
            }
        }

        return $uploadOk ? $random_name : null;
    }
}