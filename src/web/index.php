<?php
/** Le programme principal vers où toutes les demandes sont redirigées.
 * L'action à invoquer est incluse dans l'URI sous la forme /contrôleur/action.
 * Des arguments peuvent être passés via HTTP POST.
 */
// Définir la "locale" (utile pour le formatage des dates et heures), avec un ptit hack pour que ça marche aussi
// sous windows...
if (!setlocale(LC_ALL, 'fr_FR.UTF8')) {
    setlocale(LC_ALL, 'french');
}

// Paramètre à régler selon l'URI "réelle" du site
include "../config.php";

// Règles d'auto-chargement des différentes classes
include "../service/AutoLoader.php";
AutoLoader::register();

// Démarre la session php, une fois pour toutes.
session_start();

// Le format des dates affichées, toujours avec un ptit hack spécialement parce que visiblement strftime est buggé
// sous xampp/windows...
define('DATEONLY_FORMAT', "%a " . ((strtoupper(substr(PHP_OS, 0, 3)) == 'WIN') ? '%#d' : '%e') . " %b %Y");
define('TIME_FORMAT', "%H:%M");
define('DATE_FORMAT', DATEONLY_FORMAT . " à " . TIME_FORMAT);

// On récupère le controlleur et l'action demandés.
$uri = $_SERVER['REQUEST_URI'];
// On vire le préfix s'il y apparaît.
if (strpos($uri, URI_PREFIX) === 0) {
    $uri = substr($uri, strlen(URI_PREFIX));
}
// On vire éventuellement le contenu du GET
$isGetHere = strpos($uri, "?");
if ($isGetHere) {
    $uri = substr($uri, 0, $isGetHere);
}

// On nettoie un peu l'URI en enlevant les "/" finaux et initiaux.
$uri = trim($uri, '/');
if (strlen($uri) === 0) {
    DefaultController::homeAction();
} else {
    // On sépare les noms du contrôleur et de l'action en 2 variables distinctes.
    $data = explode('/', $uri);
    if (count($data) === 2) {
        // On sépare action et contrôleur.
        list($controllerName, $actionName) = $data;
        // On vérifie que le contrôleur est défini.
        $controllerName = ucfirst($controllerName) . 'Controller';
        $actionName = $actionName . 'Action';
        if (!class_exists($controllerName)) {
            Page::addError("Accès interdit.");
        } else {
            // On vérifie que l'action est bien définie dans le contrôleur.
            if (!method_exists(new $controllerName(), $actionName)) {
                Page::addError("Action non définie.");
            } else {
                // Le contrôleur réalise les actions nécessaires et met à jour les infos nécessaires au
                // rendu de la page HTML en utilisant la classe Page.
                $controllerName::$actionName();
            }
        }
    } else {
        // URI mal formée.
        Page::addError("La page demandée n'existe pas.");
    }
}

// Si $_SESSION contient errors ou infos, on les affiche et on le retire de $_SESSION
if (isset($_SESSION['errors'])) {
    Page::addErrors($_SESSION['errors']);
    $_SESSION['errors'] = null;
}
if (isset($_SESSION['infos'])) {
    Page::addInfos($_SESSION['infos']);
    $_SESSION['infos'] = null;
}

if (Session::isLoggedIn()) {
    Session::updateSolde();
}

// On envoie le HTML.
Page::render();
