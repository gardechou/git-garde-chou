<?php
/**
 * Définition du modèle métier client.
 *
 * @package Modèles
 */

/**
 * Le modèle représentant un client du site. Famille et Babysitter en sont dérivées.
 */
class Client extends User
{
    /**
     * @var string $adresse
     * @var string $code_postal
     * @var string $ville
     * @var string $presentation
     * @var float $solde
     * @var string $nom
     * @var string $prenom
     * @var string $telephone
     * @var Comment[] $mes_commentaires
     * @var Comment[] $commentaires_sur_moi
     * @var string $profile_pic
     * @var float $note_moyenne
     * @var int $nombre_notes
     */
    protected $adresse,
        $code_postal,
        $ville,
        $presentation,
        $solde,
        $nom,
        $prenom,
        $telephone,
        $note_moyenne,
        $nombre_notes,
        $taux_annulation,
        $n_toget_taux_ann,
        $mes_commentaires,
        $commentaires_sur_moi,
        $profile_pic;


    /**
     * Permet de récupérer pour un Client , le taux d'annulation, ainsi que le nombre d'élements ayant permis de calculer ce taux.
     */
    public function computeTauxAnnulationAndNombreTogetTauxAnnulation() {
        $data =  UserService::getTauxAnnulationForUser($this->id,$this->type);
        $this->taux_annulation = is_null($data["taux_annulation"]) ? 0 : $data["taux_annulation"];
        $this->n_toget_taux_ann = $data["n_toget_taux_ann"];

    }

    /**
     * Met en forme l'affichage du taux d'annulation.
     * @return string
     */
    public function getFormattedTauxAnnulation()
    {
        if ($this->getNombreToGetTauxAnnulation() == 0) {
            $res = "<em>Pas encore de gardes</em>";
        } else {
            $res = $this->taux_annulation . " %  (" . $this->getNombreToGetTauxAnnulation() . " gardes)";
        }
        return $res;
    }

    /**
     * Permet d'obtenir le taux d'annulation.
     * @return mixed
     */
    public function getTauxAnnulation()
    {
        if (is_null($this->taux_annulation)) {
            $this->computeTauxAnnulationAndNombreTogetTauxAnnulation();
        }
        return $this->taux_annulation;
    }

    /**
     * Permet d'obtenir le nombre d'élements ayant permis de calculer le taux d'annulation.
     * @return mixed
     */
    public function getNombreToGetTauxAnnulation()

    {
        if (is_null($this->taux_annulation)) {
            $this->computeTauxAnnulationAndNombreTogetTauxAnnulation();
        }
        return $this->n_toget_taux_ann;
    }


    /**
     * Permet de récupérer pour un Client , la note moyenne, ainsi que le nombre d'élements ayant permis de calculer cette note.
     */
    public function computeNoteMoyenneAndNombreNotes()
    {
        $data = CommentService::getNoteMoyenneFromId($this->id, $this->type);
        $this->note_moyenne = $data["note_moyenne"];
        $this->nombre_notes = $data["nombre_notes"];
    }

    /**
     * Met en forme l'affichage de la note moyenne.
     */
    public function getFormattedNote()
    {
        if ($this->getNombreNotes() == 0) {
            $res = "<em>Pas encore de notes</em>";
        } else {
            $res = round($this->note_moyenne, 1) . " / 5  (" . $this->getNombreNotes() . " notes)";
        }
        return $res;
    }

    /**
     * Permet d'obtenir la note moyenne.
     * @return float
     */
    public function getNoteMoyenne()
    {
        if (is_null($this->note_moyenne)) {
            $this->computeNoteMoyenneAndNombreNotes();
        }
        return $this->note_moyenne;
    }

    /**
     * Permet d'obtenir le nombre d'élements ayant permis de calculer la note moyenne.
     * @return int
     */
    public function getNombreNotes()
    {
        if (is_null($this->note_moyenne)) {
            $this->computeNoteMoyenneAndNombreNotes();
        }
        return $this->nombre_notes;
    }


    /**
     * Permet d'obtenir la photo de profil utilisé par le Client.
     * @return mixed
     */
    public function getProfilePic()
    {
        return URI_PREFIX . "/images/userpics/" . $this->profile_pic;
    }

    /**
     * Établit une image de profil.
     * @param mixed $profile_pic
     */
    public function setProfilePic($profile_pic)
    {
        $this->profile_pic = $profile_pic;
    }

    /**
     * Permet d'obtenir les commentaires postés par le Client.
     * @return Comment[]
     */
    public function getMesCommentaires()
    {
        return $this->mes_commentaires;
    }

    /**
     * Établit les commentaires postés par le Client.
     * @param mixed $mes_commentaires
     */
    public function setMesCommentaires($mes_commentaires)
    {
        $this->mes_commentaires = $mes_commentaires;
    }

    /**
     * Permet d'obtenir les commentaires reçus pour un Client.
     * @return Comment[]
     */
    public function getCommentairesSurMoi()
    {
        return $this->commentaires_sur_moi;
    }

    /**
     * Établit les commentaires reçus pour un Client.
     * @param mixed $commentaires_sur_moi
     */
    public function setCommentairesSurMoi($commentaires_sur_moi)
    {
        $this->commentaires_sur_moi = $commentaires_sur_moi;
    }

    /**
     * Permet d'obtenir le nom du Client.
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Établit le nom du Client.
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * Permet d'obtenir le prénom du Client.
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Établit le prénom du Client.
     * @param string $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * Permet d'obtenir le solde courant du Client.
     * @return float
     */
    public function getSolde()
    {
        return $this->solde;
    }

    /**
     * Établit le solde courant du Client.
     * @param float $solde
     */
    public function setSolde($solde)
    {
        $this->solde = $solde;
    }

    /**
     * Permet d'obtenir l'adresse du Client.
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Établit l'adresse du Client.
     * @param string $adresse
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }

    /**
     * Permet d'obtenir le code postal du Client.
     * @return string
     */
    public function getCodePostal()
    {
        return $this->code_postal;
    }

    /**
     * Établit le code postal du Client.
     * @param string $code_postal
     */
    public function setCodePostal($code_postal)
    {
        $this->code_postal = $code_postal;
    }

    /**
     * Permet d'obtenir la ville du Client.
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Établit la ville du Client
     * @param string $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    /**
     * Permet d'obtenir la présentation du Client.
     * @return string
     */
    public function getPresentation()
    {
        return $this->presentation;
    }

    /**
     * Établit la présentation du Client.
     * @param string $presentation
     */
    public function setPresentation($presentation)
    {
        $this->presentation = $presentation;
    }

    /**
     * Met à jour les attributs du client à partir de la BDD
     *
     * À partir d'un associative array correspondant à une "ligne" SQL telle que retournée
     * par pdo->fetch(), met à jour les attributs spécifiques du babysitter.
     *
     * Appelle la fonction homonyme de la classe parente avant pour les attributs communs à tous les utilisateurs.
     *
     * @param array $row Un associative array similaire à celui retourné par pdo->fetch()
     */
    protected function populateFromRow($row)
    {
        parent::populateFromRow($row);
        $this->setAdresse($row['adresse']);
        $this->setCodePostal($row['code_postal']);
        $this->setPresentation($row['presentation']);
        $this->setNom($row['nom']);
        $this->setPrenom($row['prenom']);
        $this->setSolde($row['solde']);
        $this->setVille($row['ville']);
        $this->setTelephone($row['telephone']);
        $this->profile_pic = $row['pic'];
    }

    /**
     * Permet d'obtenir le numéro de téléphone du Client.
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Établit le numéro de téléphone du Client.
     * @param mixed $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     *
     */
    public function populateAllComments()
    {
        $all_comments = CommentService::getAllCommentsForUser($this->id);
        $this->updateComments($all_comments);
    }

    /**
     *
     */
    public function populatePublishedComments()
    {
        $all_comments = CommentService::getPublishedCommentsForUser($this->id);
        $this->updateComments($all_comments);
    }

    /**
     * @param $all_comments
     */
    private function updateComments($all_comments)
    {
        $sur_moi = [];
        $par_moi = [];
        foreach ($all_comments as $who => $comments) {
            foreach ($comments as $comment) {
                $comm = new Comment();
                $comm->setContent($comment["contenu"]);
                $comm->setDate($comment["date"]);
                $comm->setNote($comment["note"]);
                $comm->setGardeId($comment["garde"]);
                $comm->setLimitModif($comment["limite"]);
                if ($who == "par_moi") {
                    $comm->setByPseudo($this->pseudo);
                    $comm->setById($this->id);
                    $comm->setOnId($comment["sur_id"]);
                    $comm->setOnPseudo($comment["sur"]);
                    $par_moi[] = $comm;
                } else {
                    $comm->setByPseudo($comment["par"]);
                    $comm->setById($comment["par_id"]);
                    $comm->setOnId($this->id);
                    $comm->setOnPseudo($this->pseudo);
                    $comm->setByPic($comment["pic"]);
                    $sur_moi[] = $comm;
                }
            }
        }
        $this->mes_commentaires = $par_moi;
        $this->commentaires_sur_moi = $sur_moi;
    }


}