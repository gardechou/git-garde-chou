<?php
/**
 * Définition de la classe métier Utilisateur
 *
 * @package Modèles
 * @tag Famille
 * @tag Baby-sitter
 * @tag Administrateur
 */
/**
 * Le modèle dont dérivent tous les classes d'utilisateurs du site.
 */
class User
{
    const TYPE_ADMIN = 0;
    const TYPE_FAMILLE = 1;
    const TYPE_BABYSITTER = 2;

    const TYPE_DICT = [
        0 => "administrateur",
        1 => "famille",
        2 => "baby-sitter"];

    /**
     * @var string $pseudo
     * @var string $email
     * @var int $id
     * @var DateTime $date_creation
     */
    protected $pseudo, $email, $id, $date_creation, $valid, $type;

    public function getStringType()
    {
        return self::TYPE_DICT[$this->type];
    }

    public function isCurrentUser()
    {
        return ($this->id == $_SESSION['id']);
    }

    /**
     * Génère une instance de User avec ses attributs
     *
     * À partir de la BDD.
     *
     * @param int $id
     * @return User|Famille|Babysitter
     */
    public static function genFromId($id)
    {
        $row = UserService::getDataFromId($id);
        switch ($row['type'])
        {
            case User::TYPE_ADMIN:
                $user = new User;
                $user->populateFromRow($row);
                $user->setType(User::TYPE_ADMIN);
                break;
            case User::TYPE_FAMILLE:
                $user = new Famille;
                $user->populateFromRow($row);
                $user->populatePublishedComments();
                break;
            case User::TYPE_BABYSITTER:
                $user = new Babysitter;
                $user->populateFromRow($row);
                $user->populatePublishedComments();
                break;
        }
        return $user;
    }

    public function setValid($valid)
    {
        $this->valid = $valid;
    }

    public function isValid()
    {
        return $this->valid;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * @param int $date_creation
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * @param $pseudo string
     */
    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param $email string
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    public static function genAll($n, $page, $sort_by)
    {
        $data = UserService::listAll($n, $page, $sort_by);
        $users = [];
        foreach ($data as $row) {
            switch ($row['type']) {
                case self::TYPE_FAMILLE:
                    $user = new Famille();
                    break;
                case self::TYPE_BABYSITTER:
                    $user = new Babysitter();
                    break;
                default:
                    $user = new User();
                    $user->setType(User::TYPE_ADMIN);
            }
            $users[] = $user;
            $user->populateFromRow($row);
        }
        return $users;
    }

    protected function populateFromRow($row)
    {
        $this->setDateCreation(strtotime($row['date_creation']));
        $this->setId($row['id']);
        $this->setEmail($row['email']);
        $this->setPseudo($row['pseudo']);
        $valid = ($row['valide'] == 1) ? True : False;
        $this->setValid($valid);
    }

    public function summary()
    {
        return $this->pseudo . ", inscrit le " . strftime(DATE_FORMAT, $this->date_creation);
    }

    public function getNom()
    {
        return "Non défini";
    }

    public function getPrenom()
    {
        return "Non défini";
    }

    public function isInBusinessWith($user_id)
    {
        if ($this->type == User::TYPE_BABYSITTER) {
            $babysitter = $this->id;
            $famille = $user_id;
        } else {
            $famille = $this->id;
            $babysitter = $user_id;
        }
        return GardeService::existsGardeAccepteeBetween($famille, $babysitter);
    }
}