<?php
/**
 * Définition du modèle métier Baby-sitter.
 *
 * @package Modèles
 */

/**
 * Le modèle représentant un babysitter du site.
 *
 * @tag Baby-sitter
 */
class Babysitter extends Client
{

    const GENRE_MASCULIN = 1;
    const GENRE_FEMININ = 2;
    const GENRE_STR = [1=>"Masculin", 2=>"Féminin"];

    /**
     * @var int $genre_humain
     * @var int $date_naissance
     * @var float|null $multiplicateur
     * @var float $taux_horaire
     */
    private $genre_humain, $date_naissance, $multiplicateur, $taux_horaire;

    /**
     *
     * @return mixed
     */
    public function getGenreHumain()
    {
        return $this->genre_humain;
    }

    /**
     * @return mixed
     */
    public function getGenreAsString()
    {
        return self::GENRE_STR[$this->genre_humain];
    }

    /**
     * @param mixed $genre_humain
     */
    public function setGenreHumain($genre_humain)
    {
        $this->genre_humain = $genre_humain;
    }

    /**
     * Retourne la date de naissance du babysitter.
     * @return mixed
     */
    public function getDateNaissance()
    {
        return $this->date_naissance;
    }

    /**
     * Retourne l'age du babysitter.
     * @return string
     */
    public function getAge()
    {
        $now = new DateTime('now');
        $naissance = new DateTime($this->date_naissance);
        $age = $naissance->diff($now);
        return $age->format('%y ans');
    }

    /**
     * @param mixed $date_naissance
     */
    public function setDateNaissance($date_naissance)
    {
        $this->date_naissance = $date_naissance;
    }

    /**
     * Babysitter constructor.
     *
     * Définit le type "numérique" du client selon les conventions de la BDD
     */
    public function __construct()
    {
        $this->type = User::TYPE_BABYSITTER;
    }


    /**
     * Retourne des instances de Babysitter correspondant aux baby-sitters travaillant dans un code postal donné.
     *
     * @param string $code Le code postal
     * @return Babysitter[]
     */
    static public function genFromDispoGeo($code)
    {
        $data = BabysitterService::getDataFromDispoGeo($code);
        $n = count($data);
        $babysitters = new SplFixedArray($n);
        for ($i = 0; $i < $n; $i++) {
            $row = $data[$i];
            $babysitter = new self;
            $babysitter->setPseudo($row['pseudo']);
            $babysitter->setPresentation($row['presentation']);
            $babysitter->setProfilePic($row['pic']);
            $babysitters[$i] = $babysitter;
        }
        return $babysitters;
    }

    /**
     * Retourne des instances de Babysitter correspondant aux baby-sitters travaillant dans un code postal donné,
     * pour une période précise.
     *
     * @param string $code_postal Le code postal
     * @param string $debut_timestamp Le début du travail au format "YYYY-MM-DD HH:MM"
     * @param string $fin_timestamp La fin du travail au format "YYYY-MM-DD HH:MM"
     * @return Babysitter[]
     */
    static public function genFromDispo($code_postal, $debut_timestamp, $fin_timestamp)
    {
        $data = BabysitterService::getDataFromDispo($code_postal, $debut_timestamp, $fin_timestamp);
        $n = count($data);
        $babysitters = new SplFixedArray($n);
        for ($i = 0; $i < $n; $i++) {
            $row = $data[$i];
            $babysitter = new self;
            $babysitter->setPseudo($row['pseudo']);
            $babysitter->setPresentation($row['presentation']);
            $babysitter->setMultiplicateur($row['multiplicateur']);
            $babysitter->setTauxHoraire($row['taux_horaire']);
            $babysitter->setDateCreation(strtotime($row['date_creation']));
            $babysitter->setId($row['id']);
            $babysitters[$i] = $babysitter;
        }
        return $babysitters;
    }

    /**
     * Retourne des instances de Babysitter correspondants aux baby-sitters non encore validés.
     *
     * @return Babysitter[]
     */
    static public function genInvalides()
    {
        $data = BabysitterService::getInvalides();
        $n = count($data);
        $babysitters = new SplFixedArray($n);
        for ($i = 0; $i < $n; $i++) {
            $row = $data[$i];
            $babysitter = new self;
            $babysitter->setPseudo($row['pseudo']);
            $babysitter->setPresentation($row['presentation']);
            $babysitter->setNom($row['nom']);
            $babysitter->setDateCreation(strtotime($row['date_creation']));
            $babysitter->setPrenom($row['prenom']);
            $babysitter->setId($row['id']);
            $babysitters[$i] = $babysitter;
        }
        return $babysitters->toArray();
    }

    /**
     * @return float
     */
    public function getTauxHoraire()
    {
        return $this->taux_horaire;
    }

    /**
     * @param float $taux_horaire
     */
    public function setTauxHoraire($taux_horaire)
    {
        $this->taux_horaire = $taux_horaire;
    }

    /**
     * @return float
     */
    public function getMultiplicateur()
    {
        return $this->multiplicateur;
    }

    /**
     * @param $multiplicateur float
     */
    public function setMultiplicateur($multiplicateur)
    {
        $this->multiplicateur = $multiplicateur;
    }

    /**
     * Met à jour les attributs du babysitter à partir de la BDD
     *
     * À partir d'un associative array correspondant à une "ligne" SQL telle que retournée
     * par pdo->fetch(), met à jour les attributs spécifiques du babysitter.
     *
     * Appelle la fonction homonyme de la classe parente avant pour les attributs communs à tous les clients.
     *
     * @param array $row Un associative array similaire à celui retourné par pdo->fetch()
     */
    public function populateFromRow($row)
    {
        parent::populateFromRow($row);
        $this->multiplicateur = $row['multiplicateur'];
        $this->date_naissance = $row['date_naissance'];
        $this->genre_humain = $row['genre_humain'];
    }
}