<?php
/**
 * Définition de la classe métier commentaires
 *
 * @package Modèles
 */

/**
 * Le modèle représentant un commentaire posté sur le site.
 *
 * Une note est associée au commentaire, et le contenu texte du commentaire peut être vide.
 */
class Comment
{
    /** @var int Le type d'utilisateur ayant posté la garde (User::TYPE_FAMILLE ou User::TYPE_BABYSITTER) */
    private $by_type;
    /** @var int L'id de la garde auquel se réfère le commentaire dans la BDD */
    private $garde_id;
    /** @var int Le timestamp de fin de la garde, qui sert de date de commentaire */
    private $date;
    /** @var int L'id de l'utilisateur qui a posté le commentaire */
    private $by_id;
    /** @var string Le pseudo de l'utilisateur qui a posté le commentaire */
    private $by_pseudo;
    /** @var int L'id de l'utilisateur concerné par le commentaire */
    private $on_id;
    /** @var string Le pseudo de l'utilisateur concerné par le commentaide */
    private $on_pseudo;
    /** @var string Le contenu du commentaire */
    private $content;
    /** @var int La note associée au commentaire (entre 1 et 5) */
    private $note;
    /** @var string Le nom de fichier de l'image de l'auteur du commentaire */
    private $by_pic;
    /** @var int Le timestamp limite de modification d'un commentaire */
    private $limit_modif;

    /**
     * Générer des instances de Comment à partir de la BDD
     *
     * @param int $page Le numéro de la "page" à afficher
     * @param int $n Le nombre d'éléments à retourner
     * @param string $sort_by Par quel champ de la BDD trier les commentaires
     * @return Comment[] Une liste d'instances de commentaires.
     */
    public static function genAll($n, $page, $sort_by)
    {
        $data = CommentService::getAll($n, $page, $sort_by);
        $comments = [];
        foreach ($data as $row) {
            $comment = new Comment();
            $comment->setByPseudo($row["by"]);
            $comment->setById($row["by_id"]);
            $comment->setOnPseudo($row["on"]);
            $comment->setOnId($row["on_id"]);
            $comment->setGardeId($row["garde"]);
            $comment->setNote($row["note"]);
            $comment->setContent($row["contenu"]);
            $comment->setDate($row["date"]);
            $comment->setByType($row["by_type"]);
            $comments[] = $comment;
        }
        return $comments;
    }

    /**
     * Le commentaire a-t-il un contenu texte?
     *
     * @return bool
     */
    public function hasContent()
    {
        return (!empty($this->content));
    }

    /**
     * Le commentaire est-il déjà publié? === NOT Le commentaire est-il modifiable?
     *
     * @return bool
     */
    public function isPublished()
    {
        return $this->limit_modif < time();
    }

    /**
     * Permet d'obtenr la limite temporelle pour modifier un commentaire.
     * @return int
     */
    public function getLimitModif()
    {
        return $this->limit_modif;
    }

    /**
     * Établit la limite temporelle pour modifier un commentaire.
     * @param int $limit_modif
     */
    public function setLimitModif($limit_modif)
    {
        $this->limit_modif = $limit_modif;
    }

    /**
     * Permet d'obtenir l'image de profil de l'utilisateur qui a posté le commentaire.
     * @return string
     */
    public function getByPic()
    {
        return URI_PREFIX . "/images/userpics/" . $this->by_pic;
    }

    /**
     * Établit l'image de profil de l'utilisateur qui a posté le commentaire.
     * @param string $by_pic
     */
    public function setByPic($by_pic)
    {
        $this->by_pic = $by_pic;
    }

    /**
     * Permet d'obtenir la note associée au commentaire.
     * @return int
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Établit la note associée au commentaire.
     * @param int $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * Permet d'obtenir le contenu d'un commentaire.
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Établit le contenu d'un commentaire.
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Permet d'obtenir la date du commentaire.
     * @return int
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Établit la date du commentaire.
     * @param int $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Permet d'obtenir l'id de l'utilisateur qui a posté le commentaire.
     * @return int
     */
    public function getById()
    {
        return $this->by_id;
    }

    /**
     * Établit l'id de l'utilisateur qui a posté le commentaire.
     * @param int $by_id
     */
    public function setById($by_id)
    {
        $this->by_id = $by_id;
    }

    /**
     * Permet d'obtenir le pseudo de l'utilisateur qui a posté le commentaire.
     * @return string
     */
    public function getByPseudo()
    {
        return $this->by_pseudo;
    }

    /**
     * Établit le pseudo de l'utilisateur qui a posté le commentaire.
     * @param string $by_pseudo
     */
    public function setByPseudo($by_pseudo)
    {
        $this->by_pseudo = $by_pseudo;
    }

    /**
     * Permet d'obtenir le pseudo de l'utilisateur recevant le commentaire.
     * @return int
     */
    public function getOnId()
    {
        return $this->on_id;
    }

    /**
     * Établit le pseudo de l'utilisateur recevant le commentaire.
     * @param int $on_id
     */
    public function setOnId($on_id)
    {
        $this->on_id = $on_id;
    }

    /**
     * Permet d'obtenir le pseudo de l'utilisateur recevant le commentaire.
     * @return string
     */
    public function getOnPseudo()
    {
        return $this->on_pseudo;
    }

    /**
     * Établit le pseudo de l'utilisateur recevant le commentaire.
     * @param string $on_pseudo
     */
    public function setOnPseudo($on_pseudo)
    {
        $this->on_pseudo = $on_pseudo;
    }

    /**
     * Permet d'obtenir l'id de la garde associé au commentaire.
     * @return int
     */
    public function getGardeId()
    {
        return $this->garde_id;
    }

    /**
     * Établit l'id de la garde associé au commentaire.
     * @param int $garde_id
     */
    public function setGardeId($garde_id)
    {
        $this->garde_id = $garde_id;
    }

    /**
     * Permet d'obtenir le "type" de l'utilisateur qui a posté le commentaire.
     * @return int
     */
    public function getByType()
    {
        return $this->by_type;
    }

    /**
     * Établit le "type" de l'utilisateur qui a posté le commentaire.
     * @param int $by_type
     */
    public function setByType($by_type)
    {
        $this->by_type = $by_type;
    }
}