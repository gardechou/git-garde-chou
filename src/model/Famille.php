<?php
/**
 * Définition du modèle métier disponibilité.
 *
 * @package Modèles
 */

/**
 * Le modèle représentant une famille du site.
 *
 */
class Famille extends Client
{
    /**
     * Famille constructor.
     */
    public function __construct()
    {
        $this->type = User::TYPE_FAMILLE;
    }

}