<?php
/**
 * Définition du modèle métier disponibilité.
 *
 * @package Modèles
 */

/**
 * Le modèle représentant une disponibilité temporelle de babysitter.
 */
class Dispo
{
    /**
     * @var int $debut
     * @var int $fin
     * @var float $taux_horaire
     * @var int $id
     * @var int $babysitter
     */
    private $debut, $fin, $taux_horaire, $id, $babysitter;

    /**
     * Y a-t-il une intersection entre (x1, y1) et (x2, y2) ?
     *
     * @param int $x1 positif
     * @param int $x2 positif
     * @param int $y1 positif
     * @param int $y2 positif
     * @return bool
     */
    public static function overlap($x1, $x2, $y1, $y2)
    {
        return $x1 < $y2 and $x2 > $y1;
    }

    /**
     * L'instance de dispo est est-elle en conflit temporel avec une liste de dispos?
     *
     * @param Dispo[] La liste de dispos vs laquelle tester la dispo
     * @return bool
     */
    public function hasConflictWith($dispos)
    {
        if (empty($dispos)) {
            return false;
        }

        $conflict = false;
        $i = 0;
        while (!$conflict and $i < count($dispos)) {
            $dispo = $dispos[$i];
            if ($this->overlap($this->debut, $this->fin, $dispo->getDebut(), $dispo->getFin())) {
                $conflict = true;
            }
            $i++;
        }
        return $conflict;
    }

    /**
     * Retourne une liste de dispos pour un babysitter donné
     *
     * Génère des instances de dispos à partir de la BDD.
     *
     * @param int $id L'id du babysitter (table user)
     * @param int $n Le nombre max de dispos à retourner
     * @param int $page Le numéro de la page à afficher
     * @return Dispo[]
     */
    public static function genFromBabysitterId($id, $n = INF, $page = 0)
    {
        $data = DispoService::getFromBabysitterId($id, $n, $page);
        $dispos = [];
        foreach ($data as $row) {
            $dispo = new Dispo();
            $dispo->populateFromRow($row);
            $dispos[] = $dispo;
        }
        return $dispos;
    }

    /**
     * Met à jour les attribut de l'instance de dispo
     *
     * @param array $row Un associative array de type pdo->fetch()
     */
    public function populateFromRow($row)
    {
        $this->id = $row['id'];
        $this->debut = strtotime($row['debut']);
        $this->fin = strtotime($row['fin']);
        $this->babysitter = $row['babysitter'];
        $this->taux_horaire = $row['taux_horaire'];
    }

    /**
     * Permet d'obtenir le début de la disponibilité temporelle.
     * @return int
     */
    public function getDebut()
    {
        return $this->debut;
    }

    /**
     * Établit le début de la disponibilité temporelle.
     * @param int $debut
     */
    public function setDebut($debut)
    {
        $this->debut = $debut;
    }

    /**
     * Permet d'obtenir la fin d'une disponibilité temporelle.
     * @return int
     */
    public function getFin()
    {
        return $this->fin;
    }

    /**
     * Établit la fin d'une disponibilité temporelle.
     * @param int $fin
     */
    public function setFin($fin)
    {
        $this->fin = $fin;
    }

    /**
     * Permet d'obtenir le taux horraire associé à la disponibilité temporelle.
     * @return float
     */
    public function getTauxHoraire()
    {
        return $this->taux_horaire;
    }

    /**
     * Établit le taux horraire associé à la disponibilité.
     * @param float $taux_horaire
     */
    public function setTauxHoraire($taux_horaire)
    {
        $this->taux_horaire = $taux_horaire;
    }

    /**
     * Permet d'obtenir l'id de l'utilisateur associé à la disponibilité.
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Établit l'id de l'utilisateur associé à la disponibilité.
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int L'ID du babysitter concerné par la disponibilité.
     */
    public function getBabysitter()
    {
        return $this->babysitter;
    }

    /**
     * Établit l'id du babysitter concerné par la disponibilité.
     * @param int $babysitter
     */
    public function setBabysitter($babysitter)
    {
        $this->babysitter = $babysitter;
    }
}