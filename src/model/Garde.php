<?php
/**
 * Définition de la classe métier Garde
 *
 * @package Modèles
 * @tag Famille
 * @tag Baby-sitter
 */

/**
 * Le modèle représentant une garde
 */
class Garde //TODO: écrire les docblocks
{
    const DB_ATTENTE = 0;
    const DB_DEMANDEE = 1;
    const DB_ACCEPTEE = 2;
    const DB_VALIDEE = 3;
    const DB_INVALIDEE = 4;
    const DB_REFUSEE = 5;
    const DB_ANNULEE = 6;
    const DB_AVORTEE = 7;
    const DB_INTERDITE = 8;

    const STATUS2STR = array(
        0 => "en attente d'autorisation par l'administrateur",
        1 => "demandée par la famille",
        2 => "acceptée par le baby-sitter",
        3 => "validée par la famille",
        4 => "invalidée par la famille",
        5 => "refusée par le baby-sitter",
        6 => "annulée",
        7 => "avortée par la famille",
        8 => "interdite par l'administrateur"
    );

    /** @var int */
    private $id;
    /** @var Famille */
    private $famille;
    /** @var int */
    private $famille_id;
    /** @var string */
    private $famille_pseudo;
    /** @var Babysitter */
    private $babysitter;
    /** @var int */
    private $babysitter_id;
    /** @var string */
    private $babysitter_pseudo;
    /** @var int */
    private $date_demande;
    /** @var int */
    private $debut;
    /** @var int */
    private $fin;
    /** @var string */
    private $db_status;
    /** @var float */
    private $prix;
    /** @var float */
    private $commission;
    /** @var int */
    private $nb_enfants;
    /** @var int */
    private $limite_annulation;
    /** @var int */
    private $limite_commentaire;
    /** @var int */
    private $limite_invalidation;
    /**
     * @var
     */
    private $note_famille;
    /**
     * @var
     */
    private $note_babysitter;
    /**
     * @var
     */
    private $commentaire_famille;
    /**
     * @var
     */
    private $commentaire_babysitter;

    public function getDuree()
    {
        return $this->fin - $this->debut;
    }

    public function getFormattedDuree()
    {
        $duree = $this->getDuree();
        $heures = floor($duree / 3600);
        $res = "$heures h";
        $minutes = floor(($duree % 3600) / 60);
        if ($minutes) {
            $res = $res . " $minutes min";
        }
        return $res;
    }

    public function echoSelectedNote($note)
    {
        $type = Session::getType();
        $n = "note_$type";
        if ($this->$n == null and $note == 3 or $this->$n == $note)
        {
            echo 'selected="selected"';
        }
    }

    public function getCurUserComment()
    {
        $type = Session::getType();
        $n = "commentaire_$type";
        return $this->$n;
    }

    /**
     * @return mixed
     */
    public function getNoteFamille()
    {
        return $this->note_famille;
    }

    /**
     * @param mixed $note_famille
     */
    public function setNoteFamille($note_famille)
    {
        $this->note_famille = $note_famille;
    }

    /**
     *
     */
    public function setPseudos()
    {
        $this->babysitter_pseudo = UserService::getDataFromId($this->babysitter_id)["pseudo"];
        $this->famille_pseudo = UserService::getDataFromId($this->famille_id)["pseudo"];
    }

    /**
     * @return mixed
     */
    public function getNoteBabysitter()
    {
        return $this->note_babysitter;
    }

    /**
     * @param mixed $note_babysitter
     */
    public function setNoteBabysitter($note_babysitter)
    {
        $this->note_babysitter = $note_babysitter;
    }

    /**
     * @return mixed
     */
    public function getCommentaireFamille()
    {
        return $this->commentaire_famille;
    }

    /**
     * @param mixed $commentaire_famille
     */
    public function setCommentaireFamille($commentaire_famille)
    {
        $this->commentaire_famille = $commentaire_famille;
    }

    /**
     * @return mixed
     */
    public function getCommentaireBabysitter()
    {
        return $this->commentaire_babysitter;
    }

    /**
     * @param mixed $commentaire_babysitter
     */
    public function setCommentaireBabysitter($commentaire_babysitter)
    {
        $this->commentaire_babysitter = $commentaire_babysitter;
    }

    /**
     * @param $id
     * @return Garde
     */
    public static function genFromId($id)
    {
        $row = GardeService::getDataFromId($id);
        $garde = new self;
        $garde->populateFromRow($row);
        return $garde;
    }

    /**
     * @return int
     */
    public function getLimiteAnnulation()
    {
        return $this->limite_annulation;
    }

    /**
     * @param int $limite_annulation
     */
    public function setLimiteAnnulation($limite_annulation)
    {
        $this->limite_annulation = $limite_annulation;
    }

    /**
     * @return int
     */
    public function getLimiteCommentaire()
    {
        return $this->limite_commentaire;
    }

    /**
     * @param int $limite_commentaire
     */
    public function setLimiteCommentaire($limite_commentaire)
    {
        $this->limite_commentaire = $limite_commentaire;
    }

    /**
     * @return int
     */
    public function getLimiteInvalidation()
    {
        return $this->limite_invalidation;
    }

    /**
     * @param int $limite_invalidation
     */
    public function setLimiteInvalidation($limite_invalidation)
    {
        $this->limite_invalidation = $limite_invalidation;
    }

    /**
     * @param $id int L'id de la famille
     *
     * @return Garde[]
     */
    public static function genActiveFromFamilleId($id)
    {
        $data = GardeService::getActiveFromFamilleId($id);
        $n = count($data);
        $gardes = new SplFixedArray($n);
        for ($i = 0; $i < $n; $i++) {
            $row = $data[$i];
            $garde = new Garde();
            $garde->setBabysitterPseudo($row['babysitter_pseudo']);
            $garde->populateFromRow($row);
            $gardes[$i] = $garde;
        }
        return $gardes;
    }

    /**
     * @param $id
     * @param $n
     * @param $page
     * @return array
     */
    public static function genForActor($id, $n, $page)
    {
        $data = GardeService::getAllForActor($id, $n, $page);
        $gardes = [];
        foreach ($data as $row) {
            $garde = new Garde();
            $garde->setFamillePseudo($row['famille_pseudo']);
            $garde->setBabysitterPseudo($row['babysitter_pseudo']);
            $garde->populateFromRow($row);
            $gardes[] = $garde;
        }
        return $gardes;
    }

    /**
     * @param $id
     * @return Garde[]
     */
    public static function genActiveFromBabysitterId($id)
    {
        $data = GardeService::getActiveFromBabysitterId($id);
        $n = count($data);
        $gardes = new SplFixedArray($n);
        for ($i = 0; $i < $n; $i++) {
            $row = $data[$i];
            $garde = new Garde();
            $garde->setFamillePseudo($row['famille_pseudo']);
            $garde->populateFromRow($row);
            $gardes[$i] = $garde;
        }
        return $gardes;
    }

    /**
     * @param $row
     */
    private function populateFromRow($row)
    {
        $this->setFamilleId($row['famille']);
        $this->setBabysitterId($row['babysitter']);
        $this->setId($row['id']);
        $this->setDateDemande(strtotime($row['date_demande']));
        $this->setDebut(strtotime($row['debut']));
        $this->setFin(strtotime($row['fin']));
        $this->setDbStatus($row['status']);
        $this->setPrix($row['prix']);
        $this->setCommission($row['commission']);
        $this->setNbEnfants($row['nb_enfants']);
        $this->setLimiteAnnulation(strtotime($row['lim_annulation']));
        $this->setLimiteCommentaire(strtotime($row['lim_comment']));
        $this->setLimiteInvalidation(strtotime($row['lim_invalid']));
        $this->setCommentaireFamille($row['comment_f']);
        $this->setCommentaireBabysitter($row['comment_bs']);
        $this->setNoteFamille($row['note_f']);
        $this->setNoteBabysitter($row['note_bs']);
    }

    /**
     * @return mixed
     */
    public function getStatusString()
    {
        $now = time();
        if ($this->db_status == self::DB_ACCEPTEE) {
            if ($this->fin < $now) {
                return "en attente de validation par la famille";
            }
        }

        if ($this->debut < $now and $this->db_status == self::DB_DEMANDEE) {
            return "ignorée par le babysitter";
        } elseif ($this->debut < $now and $this->db_status == self::DB_ATTENTE) {
            return "ignorée par l'admiministrateur";
        } else {
            return self::STATUS2STR[$this->db_status];
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Famille
     */
    public function getFamille()
    {
        return $this->famille;
    }

    /**
     * @param Famille $famille
     */
    public function setFamille($famille)
    {
        $this->famille = $famille;
    }

    /**
     * @return int
     */
    public function getFamilleId()
    {
        return $this->famille_id;
    }

    /**
     * @param int $famille_id
     */
    public function setFamilleId($famille_id)
    {
        $this->famille_id = $famille_id;
    }

    /**
     * @return string
     */
    public function getFamillePseudo()
    {
        return $this->famille_pseudo;
    }

    /**
     * @param string $famille_pseudo
     */
    public function setFamillePseudo($famille_pseudo)
    {
        $this->famille_pseudo = $famille_pseudo;
    }

    /**
     * @return Babysitter
     */
    public function getBabysitter()
    {
        return $this->babysitter;
    }

    /**
     * @param Babysitter $babysitter
     */
    public function setBabysitter($babysitter)
    {
        $this->babysitter = $babysitter;
    }

    /**
     * @return int
     */
    public function getBabysitterId()
    {
        return $this->babysitter_id;
    }

    /**
     * @param int $babysitter_id
     */
    public function setBabysitterId($babysitter_id)
    {
        $this->babysitter_id = $babysitter_id;
    }

    /**
     * @return string
     */
    public function getBabysitterPseudo()
    {
        return $this->babysitter_pseudo;
    }

    /**
     * @param string $babysitter_pseudo
     */
    public function setBabysitterPseudo($babysitter_pseudo)
    {
        $this->babysitter_pseudo = $babysitter_pseudo;
    }

    /**
     * @return int
     */
    public function getDateDemande()
    {
        return $this->date_demande;
    }

    /**
     * @param int $date_demande
     */
    public function setDateDemande($date_demande)
    {
        $this->date_demande = $date_demande;
    }

    /**
     * @return int
     */
    public function getDebut()
    {
        return $this->debut;
    }

    /**
     * @param int $debut
     */
    public function setDebut($debut)
    {
        $this->debut = $debut;
    }

    /**
     * @return int
     */
    public function getFin()
    {
        return $this->fin;
    }

    /**
     * @param int $fin
     */
    public function setFin($fin)
    {
        $this->fin = $fin;
    }

    /**
     * @return string
     */
    public function getDbStatus()
    {
        return $this->db_status;
    }

    /**
     * @param string $db_status
     */
    public function setDbStatus($db_status)
    {
        $this->db_status = $db_status;
    }

    /**
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param float $prix
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    }

    /**
     * @return float
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * @param float $commission
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;
    }

    /**
     * @return int
     */
    public function getNbEnfants()
    {
        return $this->nb_enfants;
    }

    /**
     * @param int $nb_enfants
     */
    public function setNbEnfants($nb_enfants)
    {
        $this->nb_enfants = $nb_enfants;
    }

    /**
     * @return bool
     */
    public function isAnnulable()
    {
        return (
            !$this->isEnCours() and
            !$this->isFinie() and
            time() < $this->limite_annulation and
            $this->db_status == self::DB_ACCEPTEE
        );
    }

    public function isAvortable()
    {
        return (
            $this->debut > time() and $this->db_status == Garde::DB_DEMANDEE
        );
    }

    public function isAvortableBy($id)
    {
        return ($this->famille_id == $id and $this->isAvortable());
    }

    /**
     * @return bool
     */
    public function isFinie()
    {
        $now = time();
        return $now > $this->fin;
    }

    /**
     * @return bool
     */
    public function isEnCours()
    {
        $now = time();
        return $now > $this->debut and $now < $this->fin;
    }

    /**
     * @return bool
     */
    public function isValidable()
    {
        $now = time();
        return (
            $this->db_status == self::DB_ACCEPTEE and
            $this->isFinie() and
            $now < $this->limite_invalidation);
    }

    /**
     * @param $id
     * @return bool
     */
    public function isValidableBy($id)
    {
        return ($this->isValidable() and $this->famille_id == $id);
    }

    /**
     * @return bool
     */
    public function isCommentable()
    {
        return ($this->isFinie() and $this->isValidee() and $this->limite_commentaire > time());
    }

    /**
     * @param $user_id
     * @return bool
     */
    public function isCommentableBy($user_id)
    {
        return (
            $this->isCommentable() and
            in_array($user_id, [$this->babysitter_id, $this->famille_id])
        );
    }


    /**
     * @return bool
     */
    public function isValidee()
    {
        return ($this->db_status == self::DB_VALIDEE);
    }

    /**
     * @return float
     */
    public function getTotalPrice()
    {
        return $this->prix + $this->commission;
    }

    /**
     * @return bool
     */
    public function isAcceptee()
    {
        return $this->db_status == self::DB_ACCEPTEE;
    }

    /**
     * @return bool
     */
    public function isAcceptable()
    {
        return (
            !$this->isFinie() and
            !$this->isEnCours() and
            $this->db_status == self::DB_DEMANDEE
        );
    }

    /**
     * @param $id
     * @return bool
     */
    public function isAcceptableBy($id)
    {
        return ($this->isAcceptable() and $this->babysitter_id == $id);
    }

    /**
     * @param $id
     * @return bool
     */
    public function isAnnulableBy($id)
    {
        $res = false;
        if ($id == $this->babysitter_id) {
            $res = ($this->db_status != self::DB_DEMANDEE);
        } elseif ($id == $this->famille_id) {
            $res = true;
        }
        return ($res and $this->isAnnulable());
    }

    /**
     * @return bool
     */
    public function isRefusable()
    {
        return (
            $this->debut > time() and
            (
                $this->db_status == self::DB_DEMANDEE or
                $this->db_status == self::DB_ATTENTE
            )
        );
    }

    /**
     * @return bool
     */
    public function isAutorisable()
    {
        return (
            $this->debut > time() and
            $this->db_status == self::DB_ATTENTE
        );
    }

    /**
     * @param $id
     * @return bool
     */
    public function isRefusableBy($id)
    {
        return ($this->isRefusable() and $this->babysitter_id == $id);
    }

    /**
     * @return string
     */
    public function summary()
    {
        return "du <strong>" . strftime(DATE_FORMAT, $this->debut) . "</strong> avec " .
        (Session::isBabysitter() ? $this->famille_pseudo : $this->babysitter_pseudo);
    }

    public static function genAll($n, $page, $sort_by = "date_demande")
    {
        $data = GardeService::listAll($n, $page, $sort_by);
        $gardes = [];
        foreach ($data as $row) {
            $garde = new Garde();
            $garde->fullPopulateFromRow($row);
            $gardes[] = $garde;
        }
        return $gardes;
    }

    public static function genAttenteAdmin()
    {
        $data = GardeService::listAttenteAdmin();
        $gardes = [];
        foreach ($data as $row) {
            $garde = new Garde();
            $garde->fullPopulateFromRow($row);
            $gardes[] = $garde;
        }
        return $gardes;
    }

    public function fullPopulateFromRow($row)
    {
        $this->populateFromRow($row);
        $this->setBabysitterPseudo($row['babysitter_pseudo']);
        $this->setFamillePseudo($row['famille_pseudo']);
    }
}