<?php /**
 * @var
 */
?>
<section class="container">
    <div class="row">
        <div class="col-sm-12">
            <h4>Baby-sitters disponibles du
                <strong class="text-danger"><?= $debut ?></strong> au
                <strong class="text-danger"><?= $fin ?></strong> pour
                <strong class="text-danger"><?= htmlspecialchars($nb_enfants) ?></strong>&nbsp;:
            </h4>
            <p>
                <?php if ($limit_annulation < $date_demande_timestamp) { ?>
                    Vous ne pourrez pas annuler cette garde si elle acceptée (trop tard).
                <?php } else { ?>
                    Vous aurez jusqu'au <?= strftime("%A %e %B %Y %H:%M", $limit_annulation) ?> pour annuler cette
                    garde si le baby-sitter accepte.
                <?php } ?>
            </p>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Pseudonyme</th>
                        <th>Membre depuis</th>
                        <th>Prix</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($babysitters as $i => $babysitter) { ?>
                    <form action="<?= URI_PREFIX ?>/famille/envoyerdemande" method="post">
                        <input type="hidden" name="debut" value="<?= $debut_timestamp ?>"/>
                        <input type="hidden" name="fin" value="<?= $fin_timestamp ?>"/>
                        <input type="hidden" name="nb_enfants" value="<?= $nb_enfants ?>"/>
                        <input type="hidden" name="date_demande"
                               value="<?= $date_demande_timestamp ?>"/>
                        <input type="hidden" name="limit_annulation"
                               value="<?= $limit_annulation ?>">
                        <input type="hidden" name="limit_comment"
                               value="<?= $limit_comment ?>">
                        <input type="hidden" name="limit_invalidation"
                               value="<?= $limit_invalidation ?>">
                        <input type="hidden" name="prix" value="<?= $prices[$i] ?>"/>
                        <input type="hidden" name="commission" value="<?= $commissions[$i] ?>"/>
                        <input type="hidden" name="autovalid" value="<?= $autovalids[$i] ?>"/>
                        <tr>
                            <td><span class="glyphicon glyphicon-user"></span>
                                <?= htmlspecialchars($babysitter->getPseudo()) ?></td>
                            <td><?= strftime("%B %Y", $babysitter->getDateCreation()) ?></td>
                            <td><?= $prices[$i] ?>€</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div class="col-sm-8">
                                    Présentation&nbsp;: <?= htmlspecialchars($babysitter->getPresentation()) ?><br/>
                                    <strong>Prix final&nbsp;: <?= $total_prices[$i] ?>&nbsp;€</strong>
                                    (<?= $commissions[$i] ?>&nbsp;€ de commission)<br/>
                                    <button
                                        formaction="<?= URI_PREFIX ?>/famille/envoyerdemande?babysitter=<?= $babysitter->getId() ?>"
                                        class="btn btn-success">
                                        <span class="glyphicon glyphicon-bullhorn"></span>
                                        Faire une demande de garde
                                    </button>
                                    <a href="<?= URI_PREFIX ?>/user/profile?id=<?= $babysitter->getId() ?>"
                                       class="btn btn-primary btn-info" target="_blank">
                                        <span class="glyphicon glyphicon-user"></span>
                                        Voir le profil complet
                                        <span class="glyphicon glyphicon-new-window"></span>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </form>
                    <?php } ?>
                    </tbody>
                </table>
        </div>
    </div>
</section>
