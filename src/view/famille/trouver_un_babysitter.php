<section class="container">
    <div class="row">
        <div class="col-sm-12">
            <form method="get" action="<?= URI_PREFIX ?>/famille/demande" class="form-inline">
                <fieldset>
                    <legend>Trouver un baby-sitter</legend>
                    <div class="form-group">
                        <label for="date_debut">Le</label>
                        <input type="date" class="form-control" name="date_debut" id="date_debut"
                               value="<?= $date_debut ?>">
                        <p class="help-block">jour</p>
                    </div>
                    <div class="form-group">
                        <label for="heure_debut">à:</label>
                        <input type="time" class="form-control" name="heure_debut" id="heure_debut"
                               value="<?= $heure_debut ?>">
                        <p class="help-block">heure</p>
                    </div>
                    <div class="form-group">
                        <label for="heure_debut">pendant</label>
                        <input type="time" class="form-control" name="duree" id="duree"
                               value="<?= $duree ?>">
                        <p class="help-block">durée</p>
                    </div>
                    <div class="form-group">
                        <label for="nb_enfants">pour</label>
                        <input style="width:50px;" type="number" class="form-control" name="nb_enfants" id="nb_enfants"
                               value="<?= $nb_enfants ?>">
                        <p class="help-block">enfant(s)</p>
                    </div>
                    <button type="submit" class="btn btn-success">Trouver</button>
                </fieldset>
            </form>
        </div>
    </div>
</section>
