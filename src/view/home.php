<?php
/**
 * La corps de la page d'accueil visiteur
 *
 * @package Vues
 * @tag Visiteur
 */
echo ""; // ne fait rien mais est nécessaire pour la génération automatique de la documentation avec phpDocumentor
?>

<div class="jumbotron text-center">
    <div class="container">
        <h1>Bienvenue&nbsp;!</h1>
        <h2>Garde-chou vous permet de trouver un baby-sitter pour vos petits choux.</h2>
        <form class="form-inline" action="<?= URI_PREFIX ?>/default/listbabysitters" method="get">
            Tapez votre code postal pour voir les baby-sitters travaillant près de chez vous
            <input size="9" type="text" name="code_postal" class="form-control form-group" placeholder="Code postal"
                   required="">
            <button type="submit" class="btn btn-success form-group"><span class="glyphicon glyphicon-search"></span></button>
        </form>
    </div>
</div>
<div class="container">
        <div class="col-sm-12">
<!--            <h2><a href="--><?php //echo URI_PREFIX ?><!--/default/subscribe?type=famille">Inscrivez-vous</a></h2>-->
            <h4><a href="<?php echo URI_PREFIX ?>/default/subscribe?type=babysitter">
                    Vous souhaitez rejoindre nos baby-sitters&nbsp;? C'est par ici&nbsp;!
                </a>
            </h4>
        </div>
    </div>
</div>