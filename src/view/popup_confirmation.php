<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Êtes-vous sûr de vous&nbsp;?</h4>
            </div>
            <div class="modal-body">
                <!--                <p>Êtes-vous sûr de vouloir annuler cette garde?</p>-->
                <p class="summary"></p>
                <!--                <p class="debug-url"></p>-->
            </div>
            <div class="modal-footer">
                <a class="btn btn-success btn-ok">Oui</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Non</button>
            </div>
        </div>
    </div>
</div>