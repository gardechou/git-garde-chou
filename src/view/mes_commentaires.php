<?php
/** @var Comment[] $comments */
?>
<div class="container">
    <legend>Les commentaires que j'ai laissés</legend>
    <?php foreach ($comments as $comment) { ?>
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    sur
                    <a href="<?= URI_PREFIX . "/user/profile?id=" . $comment->getOnId() ?>">
                        <strong><?= $comment->getOnPseudo() ?></strong></a><span
                            class="text-muted"> le <?= strftime(DATE_FORMAT, $comment->getDate()) ?></span>
                    <?php if (!$comment->isPublished()) { ?>
                        <a href="<?= URI_PREFIX . "/garde/commenter?id=" . $comment->getGardeId() ?>">
                            Modifier
                        </a>
                    <?php } ?>
                </div>
                <div class="panel-body">
                    <?= $comment->hasContent() ? $comment->getContent() : "<em>Pas de texte</em>" ?><br/>
                    Note&nbsp;: <?= $comment->getNote() ?>/5
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if (empty($comments)) { ?>
        Aucun commentaire
    <?php } ?>
</div>