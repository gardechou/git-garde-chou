<div class="container">
    <div class="alert alert-warning fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <?php echo Page::getWarnings() ?>
    </div>
</div>
