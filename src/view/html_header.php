<!DOCTYPE html>
<html>
<head>
    <title><?= Page::getTitle() ?></title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php foreach (Page::getCSS() as $css) { ?>
        <link rel="stylesheet" href="<?php echo $css ?>"/>
    <?php } ?>
    <?php //TODO: inclure les scripts dans la classe Page, comme pour les CSS?>
</head>
<body>
