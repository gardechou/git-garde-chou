<?php /**
 * @var Garde $garde
 * @var string $legende La légende
 */?>
<div class="container">
    <div class="col-sm-12">
        <form class="form-horizontal" action="<?php echo URI_PREFIX ?>/<?=$control?>/<?=$action?>" method="post">
            <fieldset>
                <!-- Form Name -->
                <legend><?=$legende?></legend>

                <input type="hidden" name="garde_id" value="<?= $garde->getId() ?>"/>
                <input type="hidden" name="type_client" value="<?= $type_client  ?>"/>

                <!-- Select Basic -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="type">Déroulement de la garde</label>
                    <div class="col-md-4">
                        <select id="type" name="note" class="form-control">
                            <option value="1"<?= ($note == 1) ? ' selected="selected"' : '' ?>>
                                1 - Scandaleux! Jamais plus!
                            </option>
                            <option value="2"<?= ($note == 2) ? ' selected="selected"' : '' ?>>
                                2 - Mouaip
                            </option>
                            <option value="3"<?= ($note == 3) ? ' selected="selected"' : '' ?>>
                                3 - Rien à signaler
                            </option>
                            <option value="4"<?= ($note == 4) ? ' selected="selected"' : '' ?>>
                                4 - Pas mal
                            </option>
                            <option value="5"<?= ($note == 5) ? ' selected="selected"' : '' ?>>
                                5 - Je recommande chaudement
                            </option>
                        </select>
                    </div>
                </div>

                <!-- Textarea -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="presentation">Commentaire (optionnel)</label>
                    <div class="col-md-4">
                        <textarea class="form-control" id="presentation"
                                  name="commentaire"><?=$contenu ?></textarea>
                        <p class="help-block"><?=$help_comment?></p>
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="singlebutton"></label>
                    <div class="col-md-4">
                        <button type="submit" id="singlebutton" class="btn btn-primary">
                            Valider
                        </button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
