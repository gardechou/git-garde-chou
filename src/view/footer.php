<?php
/**
 * Le pied de page, commun à toutes les pages du site.
 */
?>
<div class="container">
    <hr class="featurette-divider">
    <footer>
        <p class="pull-right text-right">
            <small>
                <a href="https://bitbucket.org/gardechou/git-garde-chou">
                    Code source <span class="glyphicon glyphicon-file"></span></a><br/>
                <a href="https://bitbucket.org/gardechou/">
                    À propos... <span class="glyphicon glyphicon-info-sign"></span></a>
            </small>
        </p>
        <p class="pull-left">
            <small>
                <a href="https://bitbucket.org/gardechou/git-garde-chou/issues/new" target="_blank"><span class="glyphicon glyphicon-question-sign"></span>
                    Signaler un bug</a><br/>
                <a href="mailto:nicoco@gardechou.fr"><span class="glyphicon glyphicon-envelope"></span>
                    Contact</a>
            </small>
        </p>
        <p class="text-center">
            <small>
                <span class="glyphicon glyphicon-copyright-mark"></span><br/>
                Garde-chou <?= date("Y") ?><br/>
            </small>
        </p>
    </footer>
</div>
<!-- JQuery -->
<script src="<?php echo URI_PREFIX ?>/js/jquery.js"></script>
<!-- Bootstrap -->
<script src="<?php echo URI_PREFIX ?>/bootstrap/js/bootstrap.min.js"></script>
<script>
    // Les "tooltips"
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    // la popup de confirmation
    $('#confirm').on('show.bs.modal', function (e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        var button = $(e.relatedTarget);
        var summary = button.data('summary');
        $('.summary').html(summary);
    });
</script>
</body>
</html>
