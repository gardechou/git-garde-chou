<?php
include "popup_confirmation.php"
/** @var Garde[] $gardes
 * @var string $garde_title
 * @var int $page Le numéro de la page affichée
 * @var bool $nextpage Y a-t-il une page suivante?
 */
?>
<section class="container">
    <div class="row">
        <div class="col-sm-12">
            <legend><?= $gardes_title ?>&nbsp;</legend>
            <?php if (count($gardes)) { ?>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Durée</th>
                        <th>Prix</th>
                        <?php if (Session::isAdmin() or Session::isBabysitter()) { ?>
                            <th>Famille</th>
                        <?php } ?>
                        <?php if (Session::isAdmin() or Session::isFamille()) { ?>
                            <th>Baby-sitter</th>
                        <?php } ?>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($gardes as $garde) { ?>
                        <tr>
                            <td><?= strftime(DATE_FORMAT, $garde->getDebut()) ?></td>
                            <td><?= $garde->getFormattedDuree() ?></td>
                            <td><?= $garde->getTotalPrice() ?>€</td>
                            <?php if (Session::isAdmin() or Session::isBabysitter()) { ?>
                                <td>
                                    <a href="<?= URI_PREFIX ?>/user/profile?id=<?= $garde->getFamilleId() ?>">
                                        <?= $garde->getFamillePseudo() ?>
                                    </a>
                                </td>
                            <?php } ?>
                            <?php if (Session::isAdmin() or Session::isFamille()) { ?>
                                <td>
                                    <a href="<?= URI_PREFIX ?>/user/profile?id=<?= $garde->getBabysitterId() ?>">
                                        <?= $garde->getBabysitterPseudo() ?>
                                    </a>
                                </td>
                            <?php } ?>
                            <td><?= $garde->getStatusString() ?></td>
                            <td>
                                <?php if (Session::isAdmin()) { ?>
                                    <?php if ($garde->isAutorisable()) { ?>
                                        <button type="button" class="btn btn-xs btn-success"
                                                data-href="<?= URI_PREFIX ?>/admin/autorisergarde?id=<?= $garde->getId() ?>"
                                                data-toggle="modal" data-target="#confirm"
                                                data-summary="Voulez-vous vraiment autoriser la garde <?= $garde->summary() ?>&nbsp;?">
                                        <span data-toggle="tooltip" data-placement="top" title="Autoriser"
                                              class="glyphicon glyphicon-check"></span>
                                        </button>
                                        <button type="button" class="btn btn-xs btn-danger"
                                                data-href="<?= URI_PREFIX ?>/admin/interdiregarde?id=<?= $garde->getId() ?>"
                                                data-toggle="modal" data-target="#confirm"
                                                data-summary="Voulez-vous vraiment interdire la garde <?= $garde->summary() ?>&nbsp;?">
                                        <span data-toggle="tooltip" data-placement="top" title="Interdire"
                                              class="glyphicon glyphicon-remove-sign"></span>
                                        </button>
                                    <?php } ?>
                                <?php } ?>
                                <?php if ($garde->isAnnulableBy($_SESSION["id"])) { ?>
                                    <button type="button" class="btn btn-xs btn-danger"
                                            data-href="<?= URI_PREFIX ?>/garde/annuler?id=<?= $garde->getId() ?>"
                                            data-toggle="modal" data-target="#confirm"
                                            data-summary="Voulez-vous vraiment annuler la garde <?= $garde->summary() ?>&nbsp;?">
                                        <span data-toggle="tooltip" data-placement="top" title="Annuler"
                                              class="glyphicon glyphicon-trash"></span>
                                    </button>
                                <?php } ?>
                                <?php if ($garde->isAvortableBy($_SESSION["id"])) { ?>
                                    <button type="button" class="btn btn-xs btn-danger"
                                            data-href="<?= URI_PREFIX ?>/garde/avorter?id=<?= $garde->getId() ?>"
                                            data-toggle="modal" data-target="#confirm"
                                            data-summary="Voulez-vous vraiment avorter la demande de garde <?= $garde->summary() ?>&nbsp;?">
                                        <span data-toggle="tooltip" data-placement="top" title="Avorter"
                                              class="glyphicon glyphicon-trash"></span>
                                    </button>
                                <?php } ?>
                                <?php if ($garde->isAcceptableBy($_SESSION["id"])) { ?>
                                    <button class="btn btn-xs btn-success"
                                            data-href="<?= URI_PREFIX ?>/garde/accepter?id=<?= $garde->getId() ?>"
                                            data-toggle="modal" data-target="#confirm"
                                            data-summary="Voulez-vous vraiment accepter la garde <?= $garde->summary() ?>&nbsp;?">
                                        <span data-toggle="tooltip" data-placement="top" title="Accepter"
                                              class="glyphicon glyphicon-ok"></span>
                                    </button>
                                <?php } ?>
                                <?php if ($garde->isValidableBy($_SESSION["id"])) { ?>
                                    <button class="btn btn-xs btn-success"
                                            data-toggle="modal" data-target="#confirm"
                                            data-href="<?= URI_PREFIX ?>/garde/valider?id=<?= $garde->getId() ?>"
                                            data-summary="Voulez-vous vraiment valider la garde <?= $garde->summary() ?>&nbsp;?">
                                        <span data-toggle="tooltip" data-placement="top" title="Valider"
                                              class="glyphicon glyphicon-ok"></span>
                                    </button>
                                    <button class="btn btn-xs btn-danger"
                                            data-toggle="modal" data-target="#confirm"
                                            data-href="<?= URI_PREFIX ?>/garde/invalider?id=<?= $garde->getId() ?>"
                                            data-summary="Voulez-vous vraiment invalider la garde <?= $garde->summary() ?>&nbsp;?">
                                        <span data-toggle="tooltip" data-placement="top" title="Invalider"
                                              class="glyphicon glyphicon-remove-circle"></span>
                                    </button>
                                <?php } ?>
                                <?php if ($garde->isCommentableBy($_SESSION["id"])) { ?>
                                    <button class="btn btn-xs btn-primary"
                                            onclick="location.href='<?= URI_PREFIX ?>/garde/commenter?id=<?= $garde->getId() ?>'">
                                        <span data-toggle="tooltip" data-placement="top" title="Commenter"
                                              class="glyphicon glyphicon-comment"></span>
                                    </button>
                                <?php } ?>
                                <?php if ($garde->isRefusableBy($_SESSION["id"])) { ?>
                                    <button class="btn btn-xs btn-danger"
                                            onclick="location.href='<?= URI_PREFIX ?>/garde/refuser?id=<?= $garde->getId() ?>'"
                                            data-toggle="tooltip">
                                        <span data-toggle="tooltip" data-placement="top" title="Refuser"
                                              class="glyphicon glyphicon-remove"></span>
                                    </button>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            <?php } else { ?>
                <p>Aucune garde.</p>
            <?php } ?>
            <ul class="pager">
                <?php if ($page > 1) { ?>
                    <li class="previous"><a href="<?= URI_PREFIX . $nextpage_uri?>?page=<?= $page - 1 ?>">
                            Page précédente</a>
                    </li>
                <?php } ?>
                <?php if ($nextpage) { ?>
                    <li class="next"><a href="<?= URI_PREFIX . $nextpage_uri ?>?page=<?= $page + 1 ?>">
                            Page suivante</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</section>