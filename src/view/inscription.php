<div class="container">
    <div class="col-sm-12">
        <form class="form-horizontal" action="<?php echo URI_PREFIX ?>/default/subscribe" method="post"
              enctype="multipart/form-data">
            <fieldset>
                <!-- Form Name -->
                <input type="hidden" name="type" value="<?= $type ?>"/>
                <legend>
                    Inscription
                </legend>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="type">Vous voulez</label>
                    <div class="col-md-4">
                        <select class="form-control"
                                onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                            <?php if ($type == "famille") { ?>
                                <option value="#" selected="">faire garder vos enfants</option>
                                <option value="<?=URI_PREFIX?>/default/subscribe?type=babysitter">être baby-sitter</option>
                            <?php } else { ?>
                                <option value="<?=URI_PREFIX?>/default/subscribe?type=famille">faire garder vos enfants</option>
                                <option value="#" selected="">être baby-sitter</option>
                            <?php } ?>
                        </select>
                    </div>

                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="pseudonyme">Pseudonyme</label>
                    <div class="col-md-4">
                        <input id="pseudonyme" name="pseudonyme" type="text" placeholder="pseudonyme"
                               class="form-control input-md" required="" value="<?= $pseudonyme ?>">
                    </div>
                </div>

                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="password">Mot de passe</label>
                    <div class="col-md-4">
                        <input id="password" name="password" type="password" placeholder="mot de passe"
                               class="form-control input-md" required="">
                    </div>
                </div>

                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="confirm_password">Confirmez le mot de passe</label>
                    <div class="col-md-4">
                        <input id="password" name="confirm_password" type="password"
                               placeholder="confirmation du mot de passe"
                               class="form-control input-md" required="">
                        <p class="help-block">Vérifier la touche "MAJ" et "ver num" pour éviter les erreurs de
                            frappe.</p>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="telephone">Numéro de téléphone</label>
                    <div class="col-md-4">
                        <input id="telephone" name="telephone" type="text" placeholder="0XXXXXXXXX"
                               class="form-control input-md" required="" value="<?= $telephone ?>">
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="email">Adresse de courriel</label>
                    <div class="col-md-4">
                        <input id="pseudonyme" name="email" type="email" placeholder="vous@example.net"
                               class="form-control input-md" required="" value="<?= $email ?>">
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="prenom">Prénom</label>
                    <div class="col-md-4">
                        <input id="pseudonyme" name="prenom" type="text" placeholder="prenom"
                               class="form-control input-md" required="" value="<?= $prenom ?>">
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="nom">Nom</label>
                    <div class="col-md-4">
                        <input id="pseudonyme" name="nom" type="text" placeholder="nom"
                               class="form-control input-md" required="" value="<?= $nom ?>">
                    </div>
                </div>

                <?php if ($type == "babysitter") { ?>

                    <!--  Radiobox -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="genre_humain">Sexe</label>
                        <div class="col-md-4 checkbox">
                            <label><input id="genre_humain" name="genre_humain" type="radio"
                                          required="" value="1" <?= ($genre_humain == 1) ? 'checked="checked"' : ""?>> Masculin</label>
                            <label><input id="genre_humain" name="genre_humain" type="radio"
                                          required="" value="2" <?= ($genre_humain == 2) ? 'checked="checked"' : ""?>> Féminin</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="date_naissance">Date de naissance</label>
                        <div class="col-md-4">
                            <input id="date_naissance" name="date_naissance" type="date"
                                   class="form-control input-md" required="" value="<?= $date_naissance ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="multiplicateur">Multiplicateur</label>
                        <div class="col-md-4">
                            <input id="multiplicateur" name="multiplicateur" type="text"
                                   class="form-control input-md" required="" value="<?= $multiplicateur ?>">
                            <p class="help-block">
                                Cette valeur sera utilisée pour multiplier le prix d'une garde lorsqu'il y a plus d'un enfant.
                                Pour 2 enfants, le prix sera multiplié par la valeur.
                                Pour 3 enfants, le prix sera multiplié par la valeur au carré, etc.
                            </p>
                        </div>
                    </div>

                <?php } ?>


                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="adresse">Adresse</label>
                    <div class="col-md-4">
                        <input id="adresse" name="adresse" type="text" placeholder="adresse"
                               class="form-control input-md" required="" value="<?= $adresse ?>">
                    </div>
                </div>
                <?php //TODO : faire le formulaire dtae de naissance en trois block ? ?>
                <!--                <!-- Select Basic -->
                <!--                <div class="form-group">-->
                <!--                    <label class="col-md-4 control-label" for="selectbasic">Select Basic</label>-->
                <!--                    <div class="col-md-1">-->
                <!--                        <select id="selectbasic" name="selectbasic" class="form-control">-->
                <!--                            <option value="1">Option one</option>-->
                <!--                            <option value="2">Option two</option>-->
                <!--                        </select>-->
                <!--                    </div>-->
                <!--                    <div class="col-md-2">-->
                <!--                        <select id="selectbasic" name="selectbasic" class="form-control">-->
                <!--                            <option value="1">Option one</option>-->
                <!--                            <option value="2">Option two</option>-->
                <!--                        </select>-->
                <!--                    </div>-->
                <!--                    <div class="col-md-1">-->
                <!--                        <select id="selectbasic" name="selectbasic" class="form-control">-->
                <!--                            <option value="1">Option one</option>-->
                <!--                            <option value="2">Option two</option>-->
                <!--                        </select>-->
                <!--                    </div>-->
                <!--                </div>-->

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="code_postal">Code Postal</label>
                    <div class="col-md-4">
                        <input id="code_postal" name="code_postal" type="text" placeholder="code postal"
                               class="form-control input-md" required="" value="<?= $code_postal ?>">
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="ville">Ville</label>
                    <div class="col-md-4">
                        <input id="pseudonyme" name="ville" type="text" placeholder="ville"
                               class="form-control input-md" required="" value="<?= $ville ?>">
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="ville">Image de profil</label>
                    <div class="col-md-4">
                        <input id="profile_picture" name="profile_picture" type="file" class="form-control input-md">
                        <!--                        <p class="help-block"></p>-->
                    </div>
                </div>

                <!-- Textarea -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="presentation">Texte de présentation</label>
                    <div class="col-md-4">
                        <textarea class="form-control" id="presentation"
                                  name="presentation"><?= $presentation ?></textarea>
                        <p class="help-block">Vous pouvez ajouter un texte de présentation pour que les autres
                            utilisateurs aient une idée de qui vous êtes.</p>
                    </div>
                </div>

                <!-- Checkbox -->
                <div class="form-group">
                    <div class="col-md-4 col-md-offset-4 checkbox">
                        <label><input type="checkbox" name="validation" required="required">J'ai compris que ce site était un projet
                            étudiant.</label>
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="singlebutton"></label>
                    <div class="col-md-4">
                        <button type="submit" id="singlebutton" name="singlebutton" class="btn btn-primary">
                            Valider
                        </button>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>