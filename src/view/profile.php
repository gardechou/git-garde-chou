<?php
/**
 * Profil utilisateur
 */

/**
 * @var User|Babysitter|Famille $user
 * @var bool $view_comments
 */
?>
<div class="container">
    <div class="row">
        <div
                class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <?= $user->getPrenom() . " " . $user->getNom() ?> (<?= $user->getPseudo() ?>)
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-lg-3" align="center">
                            <img alt="User Pic"
                                 src="<?= $user->getProfilePic() ?>"
                                 class="img-circle img-responsive">
                        </div>
                        <div class=" col-md-9 col-lg-9 ">
                            <table class="table table-user-information">
                                <tbody>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-calendar"></span>
                                        Membre depuis&nbsp;:
                                    </td>
                                    <td><?= strftime(DATEONLY_FORMAT, $user->getDateCreation()) ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="glyphicon glyphicon-ban-circle"></span>
                                        Taux d'annulation&nbsp;:
                                    </td>
                                    <td><?= $user->getFormattedTauxAnnulation() ?>&nbsp</td>
                                </tr>

                                <?php if ($user->getType() == User::TYPE_BABYSITTER) { ?>
                                    <tr>
                                        <td>
                                            <span class="glyphicon glyphicon-dashboard"></span>
                                            Age&nbsp;:
                                        </td>
                                        <td><?= $user->getAge() ?></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="glyphicon glyphicon-adjust"></span>
                                            Genre&nbsp;:
                                        </td>
                                        <td>
                                            <?= $user->getGenreAsString() ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                <?php if (Session::isAdmin() or $user->getType() == User::TYPE_BABYSITTER or Session::isBabysitter()) { ?>
                                    <tr>
                                        <td>
                                            <span class="glyphicon glyphicon-star"></span>
                                            Note moyenne&nbsp;:
                                        </td>
                                        <td>
                                            <?= $user->getFormattedNote() ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                <?php if (Session::isAdmin() or $user->getId() == $_SESSION['id'] or $user->isInBusinessWith($_SESSION["id"])) { ?>
                                    <tr>

                                        <td>
                                            <span class="glyphicon glyphicon-map-marker"></span>
                                            Adresse&nbsp;:
                                        </td>
                                        <td>
                                            <?= $user->getAdresse() ?><br/>
                                            <?= $user->getCodePostal() ?>
                                            <?= $user->getVille() ?>
                                        </td>
                                    </tr>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="glyphicon glyphicon-earphone"></span>
                                            Numéro de téléphone&nbsp;:
                                        </td>
                                        <td>
                                            <?= $user->getTelephone() ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="glyphicon glyphicon-envelope"></span>
                                            Courriel&nbsp;:
                                        </td>
                                        <td>
                                            <a href="mailto:<?= $user->getEmail() ?>"><?= $user->getEmail() ?></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <?php if ($user->getId() == Session::getCurrentUserId()) { ?>
                            <a href="<?= URI_PREFIX ?>/user/modif"><button class="btn btn-info" type="button">Modifier</button></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($view_comments) { ?>
    <div class="container">
        <div class="row col-sm-12">
            <legend>Commentaires</legend>
            <?php foreach ($user->getCommentairesSurMoi() as $comment) { ?>
                <div class="col-sm-12">
                    <div class="col-sm-1">
                        <img class="img-responsive img-circle"
                             src="<?= $comment->getByPic() ?>">
                    </div><!-- /col-sm-1 -->
                    <div class="col-sm-11">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                par <strong><a
                                            href="<?= URI_PREFIX . "/user/profile?id=" . $comment->getById() ?>"><?= $comment->getByPseudo() ?></a></strong><span
                                        class="text-muted"> le <?= strftime(DATE_FORMAT, $comment->getDate()) ?></span>
                            </div>
                            <div class="panel-body">
                                <?= $comment->getContent() ?><br/>
                                Note:<?= $comment->getNote() ?>
                            </div><!-- /panel-body -->
                        </div><!-- /panel panel-default -->
                    </div>
                </div>
            <?php } ?>
            <?php if (empty($user->getCommentairesSurMoi())) { ?>
                Aucun commentaire
            <?php } ?>
        </div><!-- /row -->
    </div><!-- /container -->
<?php } ?>

