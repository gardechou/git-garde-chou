<?php
/**
 * @var $babysitters Babysitter[]
 */
?>
<section class="container">
    <div class="col-sm-12">
        <?php if (!count($babysitters)) { ?>
            <h3>C'est ballot&nbsp;!</h3>
            <hr/>
            <p>
                Aucun baby-sitter trouvé pour le code postal
                <strong class="text-danger"><?= $code_postal ?></strong>
            </p>
            Ce n'est pas une raison,
        <?php } else { ?>
            <h3>Baby-sitters disponibles pour le code postal
                <strong class="text-success"><?= $code_postal ?></strong>&nbsp;:
            </h3>
            <hr/>
            <?php foreach ($babysitters as $babysitter) { ?>
                <div class="well row">
                    <img src="<?= $babysitter->getProfilePic() ?>" class="img-circle" style="float:left" height="100px">
                    <h3>&nbsp;<?= $babysitter->getPseudo() ?></h3>
                    <h6>&nbsp;<?= $babysitter->getPresentation() ?></h6>
                </div>
            <?php } ?>
            <h3>Intéressé?</h3>
        <?php } ?>
        <p>
            <a href="<?= URI_PREFIX ?>/default/subscribe?code_postal=<?= $code_postal ?>"
               class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span>
                Inscrivez-vous!</a>
        </p>
    </div>
</section>
