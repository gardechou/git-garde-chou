<section class="container">
    <div class="row">
        <div class="col-sm-12">
            <legend>Zone de travail</legend>
            <?php foreach ($dispos_geo as $code_postal) { ?>
                <form class="floatingzip-input" method="post" action="<?= URI_PREFIX ?>/dispo/supprimer_geo">
                    <input type="hidden" name="code_postal" value="<?= $code_postal ?>"/>
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default">
                                <?= $code_postal ?>
                            </button>
                        </span>
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-danger">
                                <span class="glyphicon glyphicon-minus"></span>
                            </button>
                        </span>
                    </div>
                </form>
            <?php } ?>
            <form class="floatingzip-input" method="post" action="<?= URI_PREFIX ?>/dispo/ajouter_geo">
                <div class="input-group">
                    <input class="form-control" name="code_postal" type="text"/>
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-plus"></span>
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</section>
