<section class="container">
    <div class="row">
        <div class="col-sm-12">
            <legend>Liste des disponibilités</legend>
            <table class="table" style="table-layout: auto;">
                <thead>
                <tr>
                    <th>Début</th>
                    <th>Fin</th>
                    <th>Prix</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($dispos as $dispo) { ?>
                    <tr>
                        <td><?= strftime(DATE_FORMAT, $dispo->getDebut()) ?></td>
                        <td><?= strftime(DATE_FORMAT, $dispo->getFin()) ?></td>
                        <td><?= $dispo->getTauxHoraire() ?> €/h</td>
                        <td class="text-center">
                            <form method="post">
                                <input type="hidden" name="dispo_id" value="<?= $dispo->getId() ?>">
                                <button class="btn btn-xs btn-danger"
                                        formaction="<?= URI_PREFIX ?>/dispo/supprimer"
                                        data-toggle="tooltip" title="Supprimer la disponibilité">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </button>
                            </form>
                        </td>
                    </tr>

                <?php } ?>
                <?php if (!empty($dispos)) { ?>
                    <tr>
                        <td colspan="4">
                            <form action="<?= URI_PREFIX ?>/dispo/supprimer" method="post">
                                <input type="hidden" name="dispo_id" value="all">
                                <button class="btn btn-danger"
                                        formaction="<?= URI_PREFIX ?>/dispo/supprimer"
                                        data-toggle="tooltip" title="Tout supprimer">
                                    <span class="glyphicon glyphicon-trash"></span> Tout supprimer
                                </button>
                            </form>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <ul class="pager">
                <?php if ($page > 1) { ?>
                    <li class="previous"><a href="<?= URI_PREFIX ?>/dispo/lister?page=<?= $page - 1 ?>">
                            Page précédente</a>
                    </li>
                <?php } ?>
                <?php if ($nextpage) { ?>
                    <li class="next"><a href="<?= URI_PREFIX ?>/dispo/lister?page=<?= $page + 1 ?>">
                            Page suivante</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form action="<?= URI_PREFIX ?>/dispo/ajouter" method="post" class="form-inline">
                <legend>Ajouter une disponibilité ponctuelle</legend>
                <input type="hidden" name="type_dispo" value="ponctuelle"/>
                <div class="form-group-sm">
                    <label for="date_debut">Début</label>
                    <div class="input-group input-group-sm small-input" id="date_debut">
                        <input name="date_debut" type='date' class="form-control input-sm"/>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <div class="input-group input-group-sm small-input" id="time_debut">
                        <input name="time_debut" type='time' class="form-control input-sm"/>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                    </div>
                    <label for="date_debut">Fin</label>
                    <div class="input-group input-group-sm small-input" id="date_fin">
                        <input name="date_fin" type='date' class="form-control input-sm"/>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <div class="input-group input-group-sm small-input" id="time_fin">
                        <input name="time_fin" type='time' class="form-control input-sm"/>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                    </div>
                    <label for="prix">Prix</label>
                    <div class="input-group input-group-sm small-input" id="prix">
                        <input name="taux_horaire" class="form-control input-sm" type="number"/>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-euro">/h</span></span>
                    </div>
                    <button class="btn btn-success"
                            data-toggle="tooltip" title="Ajouter"><span class="glyphicon glyphicon-plus"></span>Ajouter
                    </button>
                </div>
            </form>
        </div>
    </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-sm-12">
            <form action="<?= URI_PREFIX ?>/dispo/ajouter" method="post" class="form-horizontal">
                <legend>Ajouter une disponibilité régulière</legend>
                <input type="hidden" name="type_dispo" value="reguliere"/>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="jour">Jour(s) de la semaine</label>
                    <div class="col-md-4">
                        <label class="checkbox-inline" for="jour-0">
                            <input type="checkbox" name="jour1" id="jour-0" value="monday">
                            Lundi
                        </label>
                        <label class="checkbox-inline" for="jour-1">
                            <input type="checkbox" name="jour2" id="jour-1" value="tuesday">
                            Mardi
                        </label>
                        <label class="checkbox-inline" for="jour-2">
                            <input type="checkbox" name="jour3" id="jour-2" value="wednesday">
                            Mercredi
                        </label>
                        <label class="checkbox-inline" for="jour-3">
                            <input type="checkbox" name="jour4" id="jour-3" value="thursday">
                            Jeudi
                        </label>
                        <label class="checkbox-inline" for="jour-4">
                            <input type="checkbox" name="jour5" id="jour-4" value="friday">
                            Vendredi
                        </label>
                        <label class="checkbox-inline" for="jour-5">
                            <input type="checkbox" name="jour6" id="jour-5" value="saturday">
                            Samedi
                        </label>
                        <label class="checkbox-inline" for="jour-6">
                            <input type="checkbox" name="jour7" id="jour-6" value="sunday">
                            Dimanche
                        </label>
                    </div>
                </div>
                <!-- Appended Input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="duree">Pendant</label>
                    <div class="col-md-4">
                        <div class="input-group">
                            <input id="duree" name="duree" class="form-control" type="text" required="">
                            <span class="input-group-addon">semaine(s)</span>
                        </div>
                    </div>
                </div>
                <!-- Appended Input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="duree">Prix horaire</label>
                    <div class="col-md-4">
                        <div class="input-group">
                            <input id="duree" name="taux_horaire" class="form-control" type="text" required="">
                            <span class="input-group-addon">€/heure</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="duree">De</label>
                    <div class="col-md-4">
                        <div class="input-group input-group-sm small-input" id="time_fin">
                            <input name="debut" type='time' class="form-control input-sm" required=""/>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="duree">À</label>
                    <div class="col-md-4">
                        <div class="input-group input-group-sm small-input" id="time_fin">
                            <input name="fin" type='time' class="form-control input-sm" required=""/>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="singlebutton"></label>
                    <div class="col-md-4">
                        <button class="btn btn-success"
                                data-toggle="tooltip" title="Ajouter"><span class="glyphicon glyphicon-plus"></span>Ajouter
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>