<?php
/**
 * Barre de navigation du site
 */
?>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Afficher le menu</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= URI_PREFIX ?>">Garde-chou</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <?php if (!Session::isLoggedIn()) { ?>
                <ul class="nav navbar-nav">
                    <li <?= Page::echoActiveNavbar("subscribe") ?>>
                        <a href="<?= URI_PREFIX ?>/default/subscribe">S'inscrire</a>
                    </li>
                </ul>
                <form id="signin" class="navbar-form navbar-right" role="form" method="post"
                      action="<?= URI_PREFIX ?>/user/login">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="pseudo" type="text" class="form-control input-sm" name="pseudo" value=""
                               size="10" placeholder="Pseudonyme">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="password" type="password" class="form-control input-sm" name="password" value=""
                               size="10" placeholder="Mot de passe">
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm"><span
                                class="glyphicon glyphicon-log-out"></span></button>
                </form>
            <?php } elseif (Session::isAdmin()) { ?>
                <ul class="nav navbar-nav">
                    <li <?= Page::echoActiveNavbar("home") ?>>
                        <a href="<?= URI_PREFIX ?>/admin/home">
                            <span class="glyphicon glyphicon-home"></span>
                        </a>
                    </li>
                    <li <?= Page::echoActiveNavbar("users") ?>>
                        <a href="<?= URI_PREFIX ?>/admin/users">
                            <span class="glyphicon glyphicon-user"></span>
                            Utilisateurs
                        </a>
                    </li>
                    <li <?= Page::echoActiveNavbar("comment") ?>>
                        <a href="<?= URI_PREFIX ?>/admin/comments">
                            <span class="glyphicon glyphicon-comment"></span>
                            Commentaires
                        </a>
                    </li>
                    <li <?= Page::echoActiveNavbar("garde") ?>>
                        <a href="<?= URI_PREFIX ?>/admin/gardes">
                            <span class="glyphicon glyphicon-baby-formula"></span>
                            Gardes
                        </a>
                    </li>
                    <li <?= Page::echoActiveNavbar("stats") ?>>
                        <a href="<?= URI_PREFIX ?>/admin/stats">
                            <span class="glyphicon glyphicon-stats"></span>
                            Statistiques
                        </a>
                    </li>
                    <li <?= Page::echoActiveNavbar("settings") ?>>
                        <a href="<?= URI_PREFIX ?>/admin/parametres">
                            <span class="glyphicon glyphicon-cog"></span>
                            Paramètres
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <span class="glyphicon glyphicon-user"></span>
                            <?= htmlspecialchars($_SESSION['prenom']) ?>
                            <?= htmlspecialchars($_SESSION['nom']) ?>
                            (<?= htmlspecialchars(Session::getCurrentUserPseudo()) ?>)
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="<?= URI_PREFIX ?>/user/logout">
                                    <span class="glyphicon glyphicon-log-out"></span>
                                    Se déconnecter
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            <?php } else { ?>
                <ul class="nav navbar-nav">
                    <li <?= Page::echoActiveNavbar("home") ?>>
                        <a href="<?= URI_PREFIX ?>/user/home"><span class="glyphicon glyphicon-home"></span></a>
                    </li>
                    <li <?= Page::echoActiveNavbar("gardes") ?>>
                        <a href="<?= URI_PREFIX ?>/garde/lister"><span class="glyphicon glyphicon-baby-formula"></span>
                            Gardes</a>
                    </li>
                    <li <?= Page::echoActiveNavbar("comment") ?>>
                        <a href="<?= URI_PREFIX ?>/commentaires/lister"><span
                                    class="glyphicon glyphicon-comment"></span>
                            Commentaires</a>
                    </li>
                    <?php if (Session::isBabysitter()) { ?>
                        <li <?= Page::echoActiveNavbar("dispo") ?>>
                            <a href="<?= URI_PREFIX ?>/dispo/lister"><span class="glyphicon glyphicon-calendar"></span>
                                Disponibilités</a>
                        </li>
                    <?php } ?>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <span class="glyphicon glyphicon-user"></span>
                            <?= htmlspecialchars($_SESSION['prenom']) ?>
                            <?= htmlspecialchars($_SESSION['nom']) ?>
                            (<?= htmlspecialchars(Session::getCurrentUserPseudo()) ?>)
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="<?= URI_PREFIX ?>/user/profile">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                        Mes infos persos
                                    </a>
                                </li>
                                <li class="divider"></li>
                            <li>
                                <a><span class="glyphicon glyphicon-piggy-bank"></span>
                                    Solde&nbsp;: <?= $_SESSION['solde'] ?>&nbsp;€</a>
                            </li>
                            <li>
                                <a href="<?= URI_PREFIX ?>/solde/add?montant=100">
                                    <span class="glyphicon glyphicon-credit-card"></span>
                                    Mettre des sous
                                </a>
                            </li>
                            <li>
                                <a href="<?= URI_PREFIX ?>/solde/cashout">
                                    <span class="glyphicon glyphicon-credit-card"></span>
                                    Retirer l'argent
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?= URI_PREFIX ?>/user/logout">
                                    <span class="glyphicon glyphicon-log-out"></span>
                                    Se déconnecter
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            <?php } ?>
        </div>
    </div>
</nav>
