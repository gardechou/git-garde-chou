<div class="container">
    <div class="alert alert-danger fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Problème&nbsp;! </strong>
        <?php echo Page::getErrormsg() ?>
    </div>
</div>
