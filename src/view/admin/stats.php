<?php
include "../view/popup_confirmation.php"
// TODO: utiliser POST au lieu de GET ici: http://stackoverflow.com/questions/8982295/confirm-delete-modal-dialog-with-twitter-bootstrap
?>
<script src="<?= URI_PREFIX ?>/js/Chart.min.js"></script>
<section class="container">
    <div class="row">
        <div class="col-sm-8">
            <legend>Évolution au cours du temps</legend>
            <canvas id="dateChart" width="200" height="100"></canvas>
            <script>
                var ctx = document.getElementById("dateChart").getContext('2d');
                var dateChart = new Chart(ctx, {
                    options: {
                        scales: {
                            yAxes: [
                                {
                                    type: "linear", "id": "y-axis-1", display: true, position: "left",
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'utilisateurs'
                                    }
                                },
                                {
                                    type: "linear", "id": "y-axis-2", display: true, position: "right",
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'euros'
                                    }
                                }
                            ]
                        }
                    },
                    type: 'line',
                    data: {
                        labels: <?=$dates?>,
                        datasets: <?=$dates_data?>
                    }
                });
            </script>
        </div>
<!--    </div>-->
<!--    <div class="row">-->
        <div class="col-sm-4">
            <legend>Utilisateurs</legend>
            <canvas id="myChart" width="200" height="200"></canvas>
            <script>
                var ctx = document.getElementById("myChart").getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: <?=$labels_types?>,
                        datasets: [{
                            backgroundColor: <?=$colors_types?>,
                            data: <?=$data_types?>
                        }]
                    }
                });
            </script>
        </div>
<!--        <div class="col-sm-6">-->
<!--            <legend>Gardes</legend>-->
            <?php // TODO: stats sur les gardes?>
<!--        </div>-->
    </div>
</section>
