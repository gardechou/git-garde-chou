<?php
/**
 * @var Comment[] $comments
 */
include "../view/popup_confirmation.php"
// TODO: utiliser POST au lieu de GET ici: http://stackoverflow.com/questions/8982295/confirm-delete-modal-dialog-with-twitter-bootstrap
?>
<section class="container">
    <div class="row">
        <div class="col-sm-12">
            <legend>Liste des commentaires</legend>
            <table class="table">
                <thead>
                <tr>
                    <th>Date de publication</th>
                    <th>Par</th>
                    <th>Sur</th>
                    <th>Note</th>
                    <th>Contenu</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($comments as $comment) { ?>
                    <tr>
                        <td><?= strftime(DATE_FORMAT, $comment->getDate()) ?></td>
                        <td>
                            <a href="<?= URI_PREFIX ?>/user/profile?id=<?= $comment->getById() ?>">
                                <?= $comment->getByPseudo() ?>
                            </a>
                        </td>
                        <td>
                            <a href="<?= URI_PREFIX ?>/user/profile?id=<?= $comment->getOnId() ?>">
                                <?= $comment->getOnPseudo() ?>
                            </a>
                        </td>
                        <td><?= $comment->getNote() ?></td>
                        <td><?= $comment->getContent() ?></td>
                        <td>
                            <a href="<?= URI_PREFIX ?>/admin/editercomm?garde_id=<?= $comment->getGardeId()?>&type_client=<?= $comment->getByType()?>"
                               data-toggle="tooltip" data-placement="top" title="Editer le commentaire">
                                <button type="button" class="btn btn-xs btn-primary">
                                    <span class="glyphicon glyphicon-comment"></span>
                                </button>
                            </a>
                            <button type="button" class="btn btn-xs btn-danger"
                                    data-href="<?= URI_PREFIX ?>/admin/suppcomm?garde_id=<?= $comment->getGardeId() ?>&type_client=<?= $comment->getByType() ?>"
                                    data-toggle="modal" data-target="#confirm"
                                    data-summary='Voulez-vous vraiment supprimer le commentaire suivant :  <br/>"<?= $comment->getContent() ?>"'>
                                        <span class="glyphicon glyphicon-comment"
                                              data-toggle="tooltip" data-placement="top"
                                              title="Supprimer le commentaire"></span>
                            </button>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <ul class="pager">
                <?php if ($page > 1) { ?>
                    <li class="previous"><a href="<?= URI_PREFIX ?>/admin/users?page=<?= $page - 1 ?>">
                            Page précédente</a>
                    </li>
                <?php } ?>
                <?php if ($nextpage) { ?>
                    <li class="next"><a href="<?= URI_PREFIX ?>/admin/users?page=<?= $page + 1 ?>">
                            Page suivante</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</section>
