<section class="container">
    <div class="row">
        <div class="col-sm-12">
            <p>Page d'accueil admin.</p>
            <?php foreach (Page::get('babysitters') as $i=>$babysitter) { ?>
                <h2><?php echo htmlspecialchars($babysitter->getPseudo()) ?>:</h2>
                <p>Description&nbsp;: <?php echo htmlspecialchars($babysitter->getPresentation()) ?></p>
                <p><a href="<?php echo URI_PREFIX ?>/user/valider/?id=<?php echo $babysitter->getId()?>">Valider</a></p>
            <?php } ?>
        </div>
    </div>
</section>
