<?php
/**
 * @var User[]|Famille[]|Babysitter[] $users
 */
include "../view/popup_confirmation.php"
// TODO: utiliser POST au lieu de GET ici: http://stackoverflow.com/questions/8982295/confirm-delete-modal-dialog-with-twitter-bootstrap
?>
<section class="container">
    <div class="row">
        <div class="col-sm-12">
            <legend><?=$users_title?></legend>
            <table class="table">
                <thead>
                <tr>
                    <th>Date d'inscription</th>
                    <th>Type de compte</th>
                    <th>Pseudo</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($users as $user) { ?>
                    <tr>
                        <td><?= strftime(DATE_FORMAT, $user->getDateCreation()) ?></td>
                        <td><?= $user->getStringType() ?></td>
                        <td><?= $user->getPseudo() ?></td>
                        <td><?= $user->getNom() ?></td>
                        <td><?= $user->getPrenom() ?></td>
                        <td>
                            <?php if ($user->isValid()) { ?>
                                    <button type="button" class="btn btn-xs btn-warning"
                                            data-href="<?= URI_PREFIX ?>/user/invalider?id=<?= $user->getId() ?>"
                                            data-toggle="modal" data-target="#confirm"
                                            data-summary="Voulez-vous vraiment suspendre l'utilisateur <?= $user->summary() ?>&nbsp;?">
                                        <span data-toggle="tooltip" data-placement="top"
                                              title="Suspendre le compte"
                                              class="glyphicon glyphicon-ban-circle"></span>
                                    </button>
                            <?php } else { ?>
                                    <button type="button" class="btn btn-xs btn-success"
                                            data-href="<?= URI_PREFIX ?>/user/valider?id=<?= $user->getId() ?>"
                                            data-toggle="modal" data-target="#confirm"
                                            data-summary="Voulez-vous vraiment valider l'utilisateur <?= $user->summary() ?>&nbsp;?">
                                        <span class="glyphicon glyphicon-ok"
                                              data-toggle="tooltip" data-placement="top" title="Valider le compte"></span>
                                    </button>
                            <?php } ?>
                            <!--
                                <button type="button" class="btn btn-xs btn-danger"
                                        data-href="<?= URI_PREFIX ?>/user/delete?id=<?= $user->getId() ?>"
                                        data-toggle="modal" data-target="#confirm"
                                        data-summary="Voulez-vous vraiment supprimer l'utilisateur <?= $user->summary() ?>&nbsp;?">
                                    <span data-toggle="tooltip" data-placement="top"
                                          title="Supprimer le compte" class="glyphicon glyphicon-trash"></span>
                                </button>
                            -->
                            <a href="<?= URI_PREFIX ?>/user/profile?id=<?= $user->getId() ?>"
                               data-toggle="tooltip" data-placement="top" title="Voir les détails du compte">
                                <button type="button" class="btn btn-xs btn-info">
                                    <span class="glyphicon glyphicon-user"></span>
                                </button>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <ul class="pager">
                <?php if ($page > 1) { ?>
                    <li class="previous"><a href="<?= URI_PREFIX ?>/admin/users?page=<?= $page - 1 ?>">
                            Page précédente</a>
                    </li>
                <?php } ?>
                <?php if ($nextpage) { ?>
                    <li class="next"><a href="<?= URI_PREFIX ?>/admin/users?page=<?= $page + 1 ?>">
                            Page suivante</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</section>
