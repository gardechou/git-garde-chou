<section class="container">
    <form class="form-horizontal" action="<?= URI_PREFIX ?>/admin/parametres" method="post">
        <fieldset>
            <!-- Form Name -->
            <legend><span class="glyphicon glyphicon-eur"></span> Prix</legend>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="prix_max">Prix maximum</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="prix_max" name="prix_max" class="form-control"
                               type="number"
                               value="<?= $prix_max ?>"
                               required="">
                        <span class="input-group-addon">€</span>
                    </div>
                    <p class="help-block">Au delà de ce montant, la garde doit être validée par un administrateur avant
                        de pouvoir être acceptée par un baby-sitter.</p>
                </div>
            </div>

            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="commission">Commission</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="commission" name="commission" class="form-control"
                               type="number" step="any"
                               value="<?= number_format((float)$commission * 100, 2, '.', '') ?>"
                               required="">
                        <span class="input-group-addon">%</span>
                    </div>
                    <p class="help-block">Ajoutée au prix des gardes</p>
                </div>
            </div>

            <legend><span class="glyphicon glyphicon-time"></span> Délais</legend>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="annulation">Annulation</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="annulation" name="annulation" class="form-control"
                               type="number"
                               value="<?= $annulation ?>"
                               required="">
                        <span class="input-group-addon">heures</span>
                    </div>
                    <p class="help-block">Au délà de ce délai avant le début de la garde, il n'est plus possible de
                        l'annuler, pour le baby-sitter ou la famille</p>
                </div>
            </div>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="validation">Validation</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="validation" name="validation" class="form-control"
                               type="number"
                               value="<?= $validation ?>"
                               required="">
                        <span class="input-group-addon">heures</span>
                    </div>
                    <p class="help-block">Au délà de ce délai après la fin de la garde, elle est automatiquement
                        validée</p>
                </div>
            </div>
            <!-- Appended Input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="commentaire">Commentaire</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <input id="commentaire" name="commentaire" class="form-control"
                               type="number"
                               value="<?= $commentaire ?>"
                               required="">
                        <span class="input-group-addon">heures</span>
                    </div>
                    <p class="help-block">Au délà de ce délai après la fin de la garde, il n'est plus possible de
                        commenter l'autre utilisateur</p>
                </div>
            </div>

            <hr/>

            <!-- Button (Double) -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="button1id">Que faire de mes modifications&nbsp;?</label>
                <div class="col-md-8">
                    <button type="submit" id="button1id" name="button1id" class="btn btn-success">Valider</button>
                    <button type="reset" id="button2id" name="button2id" class="btn btn-danger">Annuler</button>
                </div>
            </div>
        </fieldset>
    </form>
</section>
