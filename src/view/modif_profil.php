<div class="container">
    <div class="col-sm-12">
        <form class="form-horizontal" action="<?php echo URI_PREFIX ?>/user/modif" method="post"
              enctype="multipart/form-data">
            <fieldset>
                <!-- Form Name -->
                <legend>
                    Modifications de mes informations personnelles
                </legend>

                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="password">Mot de passe</label>
                    <div class="col-md-4">
                        <input id="password" name="password" type="password" placeholder="mot de passe"
                               class="form-control input-md">
                        <p class="help-block">Laisser vide pour ne pas modifier.</p>
                    </div>
                </div>

                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="confirm_password">Confirmez le mot de passe</label>
                    <div class="col-md-4">
                        <input id="password" name="confirm_password" type="password"
                               placeholder="confirmation du mot de passe"
                               class="form-control input-md">
                        <p class="help-block">Vérifier la touche "MAJ" et "ver num" pour éviter les erreurs de
                            frappe.</p>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="telephone">Numéro de téléphone</label>
                    <div class="col-md-4">
                        <input id="telephone" name="telephone" type="text" placeholder="0XXXXXXXXX"
                               class="form-control input-md" value="<?= $user->getTelephone() ?>">
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="email">Adresse de courriel</label>
                    <div class="col-md-4">
                        <input id="pseudonyme" name="email" type="email" placeholder="vous@example.net"
                               class="form-control input-md" value="<?= $user->getEmail() ?>">
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="prenom">Prénom</label>
                    <div class="col-md-4">
                        <input id="pseudonyme" name="prenom" type="text" placeholder="prenom"
                               class="form-control input-md" value="<?= $user->getPrenom() ?>">
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="nom">Nom</label>
                    <div class="col-md-4">
                        <input id="pseudonyme" name="nom" type="text" placeholder="nom"
                               class="form-control input-md" value="<?= $user->getNom() ?>">
                    </div>
                </div>

                <?php if (Session::isBabysitter()) { ?>

                    <!--  Radiobox -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="genre_humain">Sexe</label>
                        <div class="col-md-4 checkbox">
                            <label><input id="genre_humain" name="genre_humain" type="radio"
                                          value="1" <?= ($user->getGenreHumain() == 1) ? 'checked="checked"' : ""?>> Masculin</label>
                            <label><input id="genre_humain" name="genre_humain" type="radio"
                                          value="2" <?= ($user->getGenreHumain() == 2) ? 'checked="checked"' : ""?>> Féminin</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="date_naissance">Date de naissance</label>
                        <div class="col-md-4">
                            <input id="date_naissance" name="date_naissance" type="date"
                                   class="form-control input-md" value="<?= $user->getDateNaissance() ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="multiplicateur">Multiplicateur</label>
                        <div class="col-md-4">
                            <input id="multiplicateur" name="multiplicateur" type="text"
                                   class="form-control input-md" value="<?= $user->getMultiplicateur() ?>">
                            <p class="help-block">
                                Cette valeur sera utilisée pour multiplier le prix d'une garde lorsqu'il y a plus d'un enfant.
                                Pour 2 enfants, le prix sera multiplié par la valeur.
                                Pour 3 enfants, le prix sera multiplié par la valeur au carré, etc.
                            </p>
                        </div>
                    </div>

                <?php } ?>


                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="adresse">Adresse</label>
                    <div class="col-md-4">
                        <input id="adresse" name="adresse" type="text" placeholder="adresse"
                               class="form-control input-md" value="<?= $user->getAdresse() ?>">
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="code_postal">Code Postal</label>
                    <div class="col-md-4">
                        <input id="code_postal" name="code_postal" type="text" placeholder="code postal"
                               class="form-control input-md" value="<?= $user->getCodePostal() ?>">
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="ville">Ville</label>
                    <div class="col-md-4">
                        <input id="pseudonyme" name="ville" type="text" placeholder="ville"
                               class="form-control input-md" value="<?= $user->getVille() ?>">
                    </div>
                </div>

                <!-- Textarea -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="presentation">Texte de présentation</label>
                    <div class="col-md-4">
                        <textarea class="form-control" id="presentation"
                                  name="presentation"><?= $user->getPresentation() ?></textarea>
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="singlebutton"></label>
                    <div class="col-md-4">
                        <button type="submit" id="singlebutton" name="singlebutton" class="btn btn-primary btn-success">
                            Valider
                        </button>
                        <button type="reset" id="singlebutton" name="singlebutton" class="btn btn-primary btn-danger">
                            Annuler
                        </button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>