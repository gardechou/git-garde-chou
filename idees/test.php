<!DOCTYPE HTML>
<html>
<head>
    <title>Test de PHP</title>
    <link rel="stylesheet" type="text/css" href="test.css">
</head>
<body>
<h1><?php
    function square($x)
    {
        return $x * $x;
    }
    $x = 5;
    echo $x . "^2 = " . square($x);
    ?>
</h1>
</body>
</html>