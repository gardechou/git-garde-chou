<!DOCTYPE html>
<html>
<head>
    <title>Garde-chou</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
</head>
<body>
<header class="container">
    <div class="row">
        <h3 class="col-sm-6">Garde chou</h3>
        <form method="POST" action="<?php echo URI_PREFIX ?>/user/login">
            <p class="col-sm-2">
                <input class="form-control" type="text" name="username"/>
            </p>
            <p class="col-sm-2">
                <input class="form-control" type="password" name="password"/>
            </p>
            <p class="col-sm-2">
                <button type="submit" class="btn btn-default">Se connecter</button>
            </p>
        </form>
        <div class="row">
            <nav class="col-sm-12">
                <ul class="nav navbar-nav">
                    <li class="col-sm-3" class="active"><a href="#">Accueil</a></li>
                    <li class="col-sm-3"><a href="#">Inscription</a></li>
                    <li class="col-sm-3"><a href="#">Connexion</a></li>
                    <li class="col-sm-3"><a href="#">À propos</a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>